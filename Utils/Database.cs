﻿namespace Utils
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Data.Common;
    using System.Data.OleDb;
    using System.Data.SqlClient;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Runtime.Caching;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;

    using Devart.Data.MySql;

    /// <summary>
    /// Constants that represent database types.
    /// </summary>
    public enum DatabaseType
    {
        /// <summary>
        /// OLE DB (Access)
        /// </summary>
        OleDb = 0,

        /// <summary>
        /// MySQL
        /// </summary>
        MySql = 1,

        /// <summary>
        /// SQL Server
        /// </summary>
        SqlServer = 2
    }

    /// <summary>
    /// Database services.
    /// </summary>
    public static class Database
    {
        /// <summary>
        /// Builds a connection string.
        /// </summary>
        /// <param name="type">The database type.</param>
        /// <param name="old">Old connection string.</param>
        /// <param name="name">The database name.</param>
        /// <param name="user">The database username.</param>
        /// <param name="pass">The database password.</param>
        /// <param name="server">The database server.</param>
        /// <param name="port">The database port.</param>
        /// <param name="timeout">The connection timeout.</param>
        /// <returns>
        /// A connection string.
        /// </returns>
        public static string BuildConnectionString(DatabaseType type, string old, string name, string user, string pass, string server, int port, int timeout = 15)
        {
            if(!string.IsNullOrWhiteSpace(name))
            {
                switch(type)
                {
                    case DatabaseType.OleDb:
                    {
                        return new OleDbConnectionStringBuilder(old)
                        {
                            DataSource = name
                        }.ConnectionString;
                    }
                    case DatabaseType.MySql:
                    case DatabaseType.SqlServer:
                    {
                        if(port > 0 && !string.IsNullOrWhiteSpace(server) && !string.IsNullOrWhiteSpace(user) && !string.IsNullOrWhiteSpace(pass))
                        {
                            if(type == DatabaseType.MySql)
                            {
                                return new MySqlConnectionStringBuilder(old)
                                {
                                    Host = server,
                                    Port = port,
                                    Database = name,
                                    UserId = user,
                                    Password = pass,
                                    ConnectionTimeout = timeout
                                }.ConnectionString;
                            }
                            if(type == DatabaseType.SqlServer)
                            {
                                return new SqlConnectionStringBuilder(old)
                                {
                                    DataSource = string.Format(CultureInfo.InvariantCulture, "{0},{1}", server, port),
                                    InitialCatalog = name,
                                    UserID = user,
                                    Password = pass,
                                    ConnectTimeout = timeout
                                }.ConnectionString;
                            }
                        }
                        break;
                    }
                    default:
                        throw new InvalidEnumArgumentException("type", (int)Config.DatabaseType, typeof(DatabaseType));
                }
            }
            return null;
        }

        private static void IncrementQueriesCount()
        {
            HttpContext context = HttpContext.Current;
            if(context != null)
            {
                lock(context.Items.SyncRoot)
                {
                    context.Items["Database Queries"] = (context.Items["Database Queries"] as int? ?? 0) + 1;
                }
            }
        }

        /// <summary>
        /// Gets the number of executed (not cached) database queries for the current request.
        /// </summary>
        public static int QueriesCount
        {
            get
            {
                return HttpContext.Current.Items["Database Queries"] as int? ?? 0;
            }
        }

        /// <summary>
        /// Creates a database parameter.
        /// </summary>
        /// <remarks>
        /// Rounds a <c>decimal</c> or <c>double</c> value to the parameter scale. Replaces a <c>null</c> value with the <see cref="System.DBNull" /> value.
        /// </remarks>
        /// <param name="name">The parameter name.</param>
        /// <param name="type">The parameter type.</param>
        /// <param name="value">The parameter value.</param>
        /// <param name="size">The parameter size.</param>
        /// <param name="precision">The parameter precision.</param>
        /// <param name="scale">The parameter scale.</param>
        /// <param name="direction">The parameter direction.</param>
        /// <returns>
        /// A database parameter.
        /// </returns>
        public static DbParameter CreateParameter(string name, DbType type, object value = null, int? size = null, byte? precision = null, byte? scale = null, ParameterDirection direction = ParameterDirection.Input)
        {
            DbParameter parameter;
            switch(Config.DatabaseType)
            {
                case DatabaseType.OleDb:
                {
                    parameter = new OleDbParameter();
                    break;
                }
                case DatabaseType.MySql:
                {
                    parameter = new MySqlParameter();
                    break;
                }
                case DatabaseType.SqlServer:
                {
                    parameter = new SqlParameter();
                    break;
                }
                default:
                    throw new InvalidEnumArgumentException(null, (int)Config.DatabaseType, typeof(DatabaseType));
            }
            parameter.ParameterName = name;
            parameter.DbType = type;
            if(direction != ParameterDirection.Input)
                parameter.Direction = direction;
            if(size != null)
                parameter.Size = size.Value;
            if(precision != null)
                parameter.Precision = precision.Value;
            if(scale != null)
            {
                parameter.Scale = scale.Value;
                var m = value as decimal?;
                if(m != null)
                {
                    parameter.Value = Math.Round(m.Value, parameter.Scale);
                    return parameter;
                }
                var d = value as double?;
                if(d != null)
                {
                    parameter.Value = Math.Round(d.Value, parameter.Scale);
                    return parameter;
                }
            }
            parameter.Value = value ?? DBNull.Value;
            return parameter;
        }

        /// <summary>
        /// Creates a parameterized database command.
        /// </summary>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// A parameterized database command.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static DbCommand CreateCommand(string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, int? timeout = null)
        {
            DbCommand cmd;
            switch(Config.DatabaseType)
            {
                case DatabaseType.OleDb:
                {
                    cmd = new OleDbCommand(text, new OleDbConnection(Config.ConnectionString));
                    break;
                }
                case DatabaseType.MySql:
                {
                    cmd = new MySqlCommand(text, new MySqlConnection(Config.ConnectionString));
                    break;
                }
                case DatabaseType.SqlServer:
                {
                    cmd = new SqlCommand(text, new SqlConnection(Config.ConnectionString));
                    break;
                }
                default:
                    throw new InvalidEnumArgumentException(null, (int)Config.DatabaseType, typeof(DatabaseType));
            }
            cmd.CommandTimeout = timeout ?? (int)Config.TimeOut.TotalSeconds;
            cmd.CommandType = type;
            if(parameters != null)
                cmd.Parameters.AddRange(parameters);
            return cmd;
        }

        /// <summary>
        /// Opens a database connection.
        /// </summary>
        /// <remarks>
        /// Handles broken and closed connection states. Increments database queries count if connection is not already open.
        /// </remarks>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="connection">The database connection.</param>
        public static void ConnectionOpen(CancellationToken cancel, DbConnection connection)
        {
            if(connection.State == ConnectionState.Broken)
                connection.Close();
            if(connection.State == ConnectionState.Closed)
            {
                IncrementQueriesCount();
                cancel.ThrowIfCancellationRequested();
                connection.Open();
            }
        }

        /// <summary>
        /// Opens asynchronously a database connection.
        /// </summary>
        /// <remarks>
        /// Handles broken and closed connection states. Increments database queries count if connection is not already open.
        /// </remarks>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="connection">The database connection.</param>
        /// <returns>
        /// An await-able <see cref="System.Threading.Tasks.Task"/>.
        /// </returns>
        public static async Task ConnectionOpenAsync(CancellationToken cancel, DbConnection connection)
        {
            if(connection.State == ConnectionState.Broken)
                connection.Close();
            if(connection.State == ConnectionState.Closed)
            {
                IncrementQueriesCount();
                var my = connection as MySqlConnection;
                if(my != null)
                {
                    cancel.ThrowIfCancellationRequested();
                    await Task.Factory.FromAsync(my.BeginOpen, my.EndOpen, null).ConfigureAwait(false);
                }
                else
                    await connection.OpenAsync(cancel).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Executes a data manipulation command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// The number of rows affected.
        /// </returns>
        public static int ExecuteNonQuery(CancellationToken cancel, DbCommand cmd)
        {
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);

            ConnectionOpen(cancel, cmd.Connection);
            cancel.ThrowIfCancellationRequested();
            return cmd.ExecuteNonQuery();
        }

        /// <summary>
        /// Executes a data manipulation command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// The number of rows affected.
        /// </returns>
        public static int ExecuteNonQuery(CancellationToken cancel, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(text));

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            int i = ExecuteNonQuery(cancel, cmd);
                            cmd.Transaction.Commit();
                            return i;
                        }
                    }
                    return ExecuteNonQuery(cancel, cmd);
                }
            }
        }

        /// <summary>
        /// Executes asynchronously a data manipulation command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// The number of rows affected.
        /// </returns>
        public static async Task<int> ExecuteNonQueryAsync(CancellationToken cancel, DbCommand cmd)
        {
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);

            await ConnectionOpenAsync(cancel, cmd.Connection).ConfigureAwait(false);
            return await cmd.ExecuteNonQueryAsync(cancel).ConfigureAwait(false);
        }

        /// <summary>
        /// Executes asynchronously a data manipulation command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// The number of rows affected.
        /// </returns>
        public static async Task<int> ExecuteNonQueryAsync(CancellationToken cancel, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(text));

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            int i = await ExecuteNonQueryAsync(cancel, cmd).ConfigureAwait(false);
                            cmd.Transaction.Commit();
                            return i;
                        }
                    }
                    return await ExecuteNonQueryAsync(cancel, cmd).ConfigureAwait(false);
                }
            }
        }

        /// <summary>
        /// Executes a scalar valued command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// The first column of the first row in the first set.
        /// </returns>
        public static object ExecuteScalarValued(CancellationToken cancel, DbCommand cmd)
        {
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);

            ConnectionOpen(cancel, cmd.Connection);
            cancel.ThrowIfCancellationRequested();
            return cmd.ExecuteScalar();
        }

        /// <summary>
        /// Executes a scalar valued command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// The first column of the first row in the first set.
        /// </returns>
        public static object ExecuteScalarValued(CancellationToken cancel, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(text));

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            object o = ExecuteScalarValued(cancel, cmd);
                            cmd.Transaction.Commit();
                            return o;
                        }
                    }
                    return ExecuteScalarValued(cancel, cmd);
                }
            }
        }

        /// <summary>
        /// Executes asynchronously a scalar valued command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// The first column of the first row in the first set.
        /// </returns>
        public static async Task<object> ExecuteScalarValuedAsync(CancellationToken cancel, DbCommand cmd)
        {
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);

            await ConnectionOpenAsync(cancel, cmd.Connection).ConfigureAwait(false);
            return await cmd.ExecuteScalarAsync(cancel).ConfigureAwait(false);
        }

        /// <summary>
        /// Executes asynchronously a scalar valued command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// The first column of the first row in the first set.
        /// </returns>
        public static async Task<object> ExecuteScalarValuedAsync(CancellationToken cancel, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(text));

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            object o = ExecuteScalarValuedAsync(cancel, cmd).ConfigureAwait(false);
                            cmd.Transaction.Commit();
                            return o;
                        }
                    }
                    return await ExecuteScalarValuedAsync(cancel, cmd).ConfigureAwait(false);
                }
            }
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        private static T ExecuteSingleRow<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbDataReader reader) where T : class
        {
            cancel.ThrowIfCancellationRequested();
            if(reader.Read())
            {
                var dict = new Dictionary<string, int>(reader.VisibleFieldCount);
                for(int i = 0, j = reader.VisibleFieldCount; i < j; i++)
                    dict.Add(reader.GetName(i), i);
                return func(reader, dict);
            }
            return null;
        }

        private static async Task<T> ExecuteSingleRowAsync<T>(CancellationToken cancel, Func<DbDataReader, IReadOnlyDictionary<string, int>, T> func, DbDataReader reader) where T : class
        {
            if(await reader.ReadAsync(cancel).ConfigureAwait(false))
            {
                var dict = new Dictionary<string, int>(reader.VisibleFieldCount);
                for(int i = 0, j = reader.VisibleFieldCount; i < j; i++)
                    dict.Add(reader.GetName(i), i);
                return func(reader, dict);
            }
            return null;
        }

        /// <summary>
        /// Executes a single row data reader command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// The first row in the first set.
        /// </returns>
        public static T ExecuteSingleRow<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbCommand cmd) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);

            ConnectionOpen(cancel, cmd.Connection);
            cancel.ThrowIfCancellationRequested();
            using(DbDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleResult | CommandBehavior.SingleRow))
                return ExecuteSingleRow(cancel, func, reader);
        }

        /// <summary>
        /// Executes a single row data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// The first row in the first set.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static T ExecuteSingleRow<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(text));

            using(var cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            T item = ExecuteSingleRow(cancel, func, cmd);
                            cmd.Transaction.Commit();
                            return item;
                        }
                    }
                    return ExecuteSingleRow(cancel, func, cmd);
                }
            }
        }

        /// <summary>
        /// Executes asynchronously a single row data reader command.
        /// </summary>
        /// <remarks>
        /// Each row is read whole, all at once - instead of just one column at a time. When checking for <see cref="System.DBNull" /> value, the same column is not read twice.
        /// </remarks>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// The first row in the first set.
        /// </returns>
        public static async Task<T> ExecuteSingleRowAsync<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbCommand cmd) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);

            await ConnectionOpenAsync(cancel, cmd.Connection).ConfigureAwait(false);
            var my = cmd as MySqlCommand;
            if(my != null)
            {
                cancel.ThrowIfCancellationRequested();
                using(MySqlDataReader reader = await Task.Factory.FromAsync<Func<AsyncCallback, object, CommandBehavior, IAsyncResult>, CommandBehavior, MySqlDataReader>((arg1, arg2, cb, obj) => arg1(cb, obj, arg2), my.EndExecuteReader, my.BeginExecuteReader, CommandBehavior.SingleResult | CommandBehavior.SingleRow, null).ConfigureAwait(false))
                    return await ExecuteSingleRowAsync(cancel, func, reader).ConfigureAwait(false);
            }
            using(DbDataReader reader = await cmd.ExecuteReaderAsync(CommandBehavior.SingleResult | CommandBehavior.SingleRow, cancel).ConfigureAwait(false))
                return await ExecuteSingleRowAsync(cancel, func, reader).ConfigureAwait(false);
        }

        /// <summary>
        /// Executes asynchronously a single row data reader command.
        /// </summary>
        /// <remarks>
        /// Each row is read whole, all at once - instead of just one column at a time. When checking for <see cref="System.DBNull" /> value, the same column is not read twice.
        /// </remarks>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// The first row in the first set.
        /// </returns>
        public static async Task<T> ExecuteSingleRowAsync<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(text));

            using(var cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            T item = await ExecuteSingleRowAsync(cancel, func, cmd).ConfigureAwait(false);
                            cmd.Transaction.Commit();
                            return item;
                        }
                    }
                    return await ExecuteSingleRowAsync(cancel, func, cmd).ConfigureAwait(false);
                }
            }
        }

        // ReSharper disable once SuggestBaseTypeForParameter
        private static IReadOnlyList<T> ExecuteSingleSet<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbDataReader reader)
        {
            var list = new List<T>(0);
            cancel.ThrowIfCancellationRequested();
            if(reader.Read())
            {
                list.Capacity = Config.PageSize;
                var dict = new Dictionary<string, int>(reader.VisibleFieldCount);
                for(int i = 0, j = reader.VisibleFieldCount; i < j; i++)
                    dict.Add(reader.GetName(i), i);
                do
                {
                    list.Add(func(reader, dict));
                    cancel.ThrowIfCancellationRequested();
                }
                while(reader.Read());
            }
            return list;
        }

        private static async Task<IReadOnlyList<T>> ExecuteSingleSetAsync<T>(CancellationToken cancel, Func<DbDataReader, IReadOnlyDictionary<string, int>, T> func, DbDataReader reader)
        {
            var list = new List<T>(0);
            if(await reader.ReadAsync(cancel).ConfigureAwait(false))
            {
                list.Capacity = Config.PageSize;
                var dict = new Dictionary<string, int>(reader.VisibleFieldCount);
                for(int i = 0, j = reader.VisibleFieldCount; i < j; i++)
                    dict.Add(reader.GetName(i), i);
                do
                {
                    list.Add(func(reader, dict));
                }
                while(await reader.ReadAsync(cancel).ConfigureAwait(false));
            }
            return list;
        }

        /// <summary>
        /// Executes a single set data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// The first set.
        /// </returns>
        public static IReadOnlyList<T> ExecuteSingleSet<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbCommand cmd)
        {
            Contract.Requires(func != null);
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);

            ConnectionOpen(cancel, cmd.Connection);
            cancel.ThrowIfCancellationRequested();
            using(DbDataReader reader = cmd.ExecuteReader(CommandBehavior.SingleResult))
                return ExecuteSingleSet(cancel, func, reader);
        }

        /// <summary>
        /// Executes a single set data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// The first set.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        public static IReadOnlyList<T> ExecuteSingleSet<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null)
        {
            Contract.Requires(func != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(text));

            using(var cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            IReadOnlyList<T> list = ExecuteSingleSet(cancel, func, cmd);
                            cmd.Transaction.Commit();
                            return list;
                        }
                    }
                    return ExecuteSingleSet(cancel, func, cmd);
                }
            }
        }

        /// <summary>
        /// Executes asynchronously a single set data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// The first set.
        /// </returns>
        public static async Task<IReadOnlyList<T>> ExecuteSingleSetAsync<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbCommand cmd)
        {
            Contract.Requires(func != null);
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);

            await ConnectionOpenAsync(cancel, cmd.Connection).ConfigureAwait(false);
            var my = cmd as MySqlCommand;
            if(my != null)
            {
                cancel.ThrowIfCancellationRequested();
                using(MySqlDataReader reader = await Task.Factory.FromAsync<Func<AsyncCallback, object, CommandBehavior, IAsyncResult>, CommandBehavior, MySqlDataReader>((arg1, arg2, cb, obj) => arg1(cb, obj, arg2), my.EndExecuteReader, my.BeginExecuteReader, CommandBehavior.SingleResult, null).ConfigureAwait(false))
                    return await ExecuteSingleSetAsync(cancel, func, reader).ConfigureAwait(false);
            }
            using(DbDataReader reader = await cmd.ExecuteReaderAsync(CommandBehavior.SingleResult, cancel).ConfigureAwait(false))
                return await ExecuteSingleSetAsync(cancel, func, reader).ConfigureAwait(false);
        }

        /// <summary>
        /// Executes asynchronously a single set data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <returns>
        /// The first set.
        /// </returns>
        public static async Task<IReadOnlyList<T>> ExecuteSingleSetAsync<T>(CancellationToken cancel, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null)
        {
            Contract.Requires(func != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(text));

            using(var cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            IReadOnlyList<T> list = await ExecuteSingleSetAsync(cancel, func, cmd).ConfigureAwait(false);
                            cmd.Transaction.Commit();
                            return list;
                        }
                    }
                    return await ExecuteSingleSetAsync(cancel, func, cmd).ConfigureAwait(false);
                }
            }
        }
        
        /// <summary>
        /// Compiles a database command.
        /// </summary>
        /// <param name="cmd">The database command.</param>
        /// <returns>
        /// A compiled database command.
        /// </returns>
        public static string CompileCommand(DbCommand cmd)
        {
            var parameters = new DbParameter[cmd.Parameters.Count];
            cmd.Parameters.CopyTo(parameters, 0);
            return CompileCommand(cmd.CommandText, parameters, cmd.CommandType, cmd.Transaction);
        }

        /// <summary>
        /// Compiles a database command.
        /// </summary>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <returns>
        /// A compiled database command.
        /// </returns>
        public static string CompileCommand(string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, DbTransaction transaction = null)
        {
            var sb = new StringBuilder(text);
            switch(type)
            {
                case CommandType.Text:
                {
                    if(parameters != null && parameters.Length > 0)
                    {
                        Array.Sort(parameters, (p1, p2) => p2.ParameterName.Length.CompareTo(p1.ParameterName.Length));
                        foreach(DbParameter p in parameters)
                            sb.Replace(p.ParameterName, Filter.EscapeString(string.Format(CultureInfo.InvariantCulture, "{0}", p.Value)));
                    }
                    break;
                }
                case CommandType.StoredProcedure:
                {
                    sb.Insert(0, "EXEC ");
                    if(parameters != null && parameters.Length > 0)
                    {
                        Array.Sort(parameters, (p1, p2) => string.Compare(p1.ParameterName, p2.ParameterName, StringComparison.Ordinal));
                        foreach(DbParameter p in parameters)
                            sb.AppendFormat(CultureInfo.InvariantCulture, " {0} = {1},", p.ParameterName, Filter.EscapeString(string.Format(CultureInfo.InvariantCulture, "{0}", p.Value)));
                        sb.Remove(sb.Length - 1, 1);
                    }
                    break;
                }
                case CommandType.TableDirect:
                {
                    sb.Insert(0, "SELECT * FROM ");
                    break;
                }
                default:
                    throw new InvalidEnumArgumentException("type", (int)Config.DatabaseType, typeof(DatabaseType));
            }
            if(transaction != null)
            {
                sb.Insert(0, "BEGIN TRANSACTION ");
                sb.Append(" COMMIT");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Gets a cached scalar valued command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="cmd">The database command.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first column of the first row in the first set.
        /// </returns>
        public static object ExecuteScalarValuedCache(CancellationToken cancel, SemaphoreSlim sync, DbCommand cmd, bool update = false, CacheItemPolicy policy = null)
        {
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);
            Contract.Requires(sync != null);

            string key = CompileCommand(cmd);
            MemoryCache cache = MemoryCache.Default;
            object result = !update ? cache[key] : null;
            if(result == null)
            {
                sync.Wait(cancel);
                try
                {
                    if(update)
                        cache.Remove(key);
                    else
                        result = cache.Get(key);
                    if(result == null)
                    {
                        result = ExecuteScalarValued(cancel, cmd);
                        cache.Add(
                            key,
                            result,
                            policy ?? new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            });
                    }
                }
                finally
                {
                    sync.Release();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a cached scalar valued command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first column of the first row in the first set.
        /// </returns>
        public static object ExecuteScalarValuedCache(CancellationToken cancel, SemaphoreSlim sync, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null, bool update = false, CacheItemPolicy policy = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(text));
            Contract.Requires(sync != null);

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            object o = ExecuteScalarValuedCache(cancel, sync, cmd, update, policy);
                            cmd.Transaction.Commit();
                            return o;
                        }
                    }
                    return ExecuteScalarValuedCache(cancel, sync, cmd, update, policy);
                }
            }
        }

        /// <summary>
        /// Gets asynchronously a cached scalar valued command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="cmd">The database command.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first column of the first row in the first set.
        /// </returns>
        public static async Task<object> ExecuteScalarValuedCacheAsync(CancellationToken cancel, SemaphoreSlim sync, DbCommand cmd, bool update = false, CacheItemPolicy policy = null)
        {
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);
            Contract.Requires(sync != null);

            string key = CompileCommand(cmd);
            MemoryCache cache = MemoryCache.Default;
            object result = !update ? cache[key] : null;
            if(result == null)
            {
                await sync.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    if(update)
                        cache.Remove(key);
                    else
                        result = cache.Get(key);
                    if(result == null)
                    {
                        result = await ExecuteScalarValuedAsync(cancel, cmd).ConfigureAwait(false);
                        cache.Add(
                            key,
                            result,
                            policy ?? new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            });
                    }
                }
                finally
                {
                    sync.Release();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets asynchronously a cached scalar valued command.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first column of the first row in the first set.
        /// </returns>
        public static async Task<object> ExecuteScalarValuedCacheAsync(CancellationToken cancel, SemaphoreSlim sync, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null, bool update = false, CacheItemPolicy policy = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(text));
            Contract.Requires(sync != null);

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            object o = await ExecuteScalarValuedCacheAsync(cancel, sync, cmd, update, policy).ConfigureAwait(false);
                            cmd.Transaction.Commit();
                            return o;
                        }
                    }
                    return await ExecuteScalarValuedCacheAsync(cancel, sync, cmd, update, policy).ConfigureAwait(false);
                }
            }
        }

        /// <summary>
        /// Gets a cached single row data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="cmd">The database command.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first row in the first set.
        /// </returns>
        public static T ExecuteSingleRowCache<T>(CancellationToken cancel, SemaphoreSlim sync, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbCommand cmd, bool update = false, CacheItemPolicy policy = null) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);
            Contract.Requires(sync != null);

            string key = CompileCommand(cmd);
            MemoryCache cache = MemoryCache.Default;
            T result = !update ? cache[key] as T : null;
            if(result == null)
            {
                sync.Wait(cancel);
                try
                {
                    if(update)
                        cache.Remove(key);
                    else
                        result = cache.Get(key) as T;
                    if(result == null)
                    {
                        result = ExecuteSingleRow(cancel, func, cmd);
                        cache.Add(
                            key,
                            result,
                            policy ?? new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            });
                    }
                }
                finally
                {
                    sync.Release();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a cached single row data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first row in the first set.
        /// </returns>
        public static T ExecuteSingleRowCache<T>(CancellationToken cancel, SemaphoreSlim sync, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null, bool update = false, CacheItemPolicy policy = null) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(text));
            Contract.Requires(sync != null);

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            T item = ExecuteSingleRowCache(cancel, sync, func, cmd, update, policy);
                            cmd.Transaction.Commit();
                            return item;
                        }
                    }
                    return ExecuteSingleRowCache(cancel, sync, func, cmd, update, policy);
                }
            }
        }
        
        /// <summary>
        /// Gets asynchronously a cached single row data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="cmd">The database command.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first row in the first set.
        /// </returns>
        public static async Task<T> ExecuteSingleRowCacheAsync<T>(CancellationToken cancel, SemaphoreSlim sync, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbCommand cmd, bool update = false, CacheItemPolicy policy = null) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);
            Contract.Requires(sync != null);

            string key = CompileCommand(cmd);
            MemoryCache cache = MemoryCache.Default;
            T result = !update ? cache[key] as T : null;
            if(result == null)
            {
                await sync.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    if(update)
                        cache.Remove(key);
                    else
                        result = cache.Get(key) as T;
                    if(result == null)
                    {
                        result = await ExecuteSingleRowAsync(cancel, func, cmd).ConfigureAwait(false);
                        cache.Add(
                            key,
                            result,
                            policy ?? new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            });
                    }
                }
                finally
                {
                    sync.Release();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets asynchronously a cached single row data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first row in the first set.
        /// </returns>
        public static async Task<T> ExecuteSingleRowCacheAsync<T>(CancellationToken cancel, SemaphoreSlim sync, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null, bool update = false, CacheItemPolicy policy = null) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(text));
            Contract.Requires(sync != null);

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            T item = await ExecuteSingleRowCacheAsync(cancel, sync, func, cmd, update, policy).ConfigureAwait(false);
                            cmd.Transaction.Commit();
                            return item;
                        }
                    }
                    return await ExecuteSingleRowCacheAsync(cancel, sync, func, cmd, update, policy).ConfigureAwait(false);
                }
            }
        }

        /// <summary>
        /// Gets a cached single set data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="cmd">The database command.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first set.
        /// </returns>
        public static IReadOnlyList<T> ExecuteSingleSetCache<T>(CancellationToken cancel, SemaphoreSlim sync, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbCommand cmd, bool update = false, CacheItemPolicy policy = null)
        {
            Contract.Requires(func != null);
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);
            Contract.Requires(sync != null);

            string key = CompileCommand(cmd);
            MemoryCache cache = MemoryCache.Default;
            IReadOnlyList<T> result = !update ? cache[key] as IReadOnlyList<T> : null;
            if(result == null)
            {
                sync.Wait(cancel);
                try
                {
                    if(update)
                        cache.Remove(key);
                    else
                        result = cache.Get(key) as IReadOnlyList<T>;
                    if(result == null)
                    {
                        result = ExecuteSingleSet(cancel, func, cmd);
                        cache.Add(
                            key,
                            result,
                            policy ?? new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            });
                    }
                }
                finally
                {
                    sync.Release();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a cached single set data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first set.
        /// </returns>
        public static IReadOnlyList<T> ExecuteSingleSetCache<T>(CancellationToken cancel, SemaphoreSlim sync, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null, bool update = false, CacheItemPolicy policy = null)
        {
            Contract.Requires(func != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(text));
            Contract.Requires(sync != null);

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            IReadOnlyList<T> list = ExecuteSingleSetCache(cancel, sync, func, cmd, update, policy);
                            cmd.Transaction.Commit();
                            return list;
                        }
                    }
                    return ExecuteSingleSetCache(cancel, sync, func, cmd, update, policy);
                }
            }
        }

        /// <summary>
        /// Gets asynchronously a cached single set data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="cmd">The database command.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first set.
        /// </returns>
        public static async Task<IReadOnlyList<T>> ExecuteSingleSetCacheAsync<T>(CancellationToken cancel, SemaphoreSlim sync, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, DbCommand cmd, bool update = false, CacheItemPolicy policy = null)
        {
            Contract.Requires(func != null);
            Contract.Requires(cmd != null);
            Contract.Requires(cmd.Connection != null);
            Contract.Requires(sync != null);

            string key = CompileCommand(cmd);
            MemoryCache cache = MemoryCache.Default;
            IReadOnlyList<T> result = !update ? cache[key] as IReadOnlyList<T> : null;
            if(result == null)
            {
                await sync.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    if(update)
                        cache.Remove(key);
                    else
                        result = cache.Get(key) as IReadOnlyList<T>;
                    if(result == null)
                    {
                        result = await ExecuteSingleSetAsync(cancel, func, cmd).ConfigureAwait(false);
                        cache.Add(
                            key,
                            result,
                            policy ?? new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            });
                    }
                }
                finally
                {
                    sync.Release();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets asynchronously a cached single set data reader command.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="text">The command text.</param>
        /// <param name="parameters">The command parameters.</param>
        /// <param name="type">The command type.</param>
        /// <param name="transaction">The command transaction.</param>
        /// <param name="timeout">The command timeout.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// The cached first set.
        /// </returns>
        public static async Task<IReadOnlyList<T>> ExecuteSingleSetCacheAsync<T>(CancellationToken cancel, SemaphoreSlim sync, Func<IDataRecord, IReadOnlyDictionary<string, int>, T> func, string text, DbParameter[] parameters = null, CommandType type = CommandType.Text, bool transaction = false, int? timeout = null, bool update = false, CacheItemPolicy policy = null)
        {
            Contract.Requires(func != null);
            Contract.Requires(!string.IsNullOrWhiteSpace(text));
            Contract.Requires(sync != null);

            using(DbCommand cmd = CreateCommand(text, parameters, type, timeout))
            {
                using(cmd.Connection)
                {
                    if(transaction)
                    {
                        ConnectionOpen(cancel, cmd.Connection);
                        using(cmd.Transaction = cmd.Connection.BeginTransaction())
                        {
                            IReadOnlyList<T> list = await ExecuteSingleSetCacheAsync(cancel, sync, func, cmd, update, policy).ConfigureAwait(false);
                            cmd.Transaction.Commit();
                            return list;
                        }
                    }
                    return await ExecuteSingleSetCacheAsync(cancel, sync, func, cmd, update, policy).ConfigureAwait(false);
                }
            }
        }

        private static string FormatException(OleDbException ole)
        {
            var sb = new StringBuilder();
            foreach(OleDbError err in ole.Errors)
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("---------------");
                sb.AppendLine();
                sb.AppendLine("Error Message: " + err.Message);
                sb.AppendLine();
                sb.AppendLine("Native Error: " + err.NativeError.ToString(CultureInfo.InvariantCulture));
                sb.AppendLine();
                sb.AppendLine("State Code: " + err.SQLState.ToString(CultureInfo.InvariantCulture));
                sb.AppendLine();
                sb.Append("Source Name: " + err.Source);
            }
            return sb.ToString();
        }

        private static string FormatException(MySqlException my)
        {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("Error Code: " + my.Code.ToString(CultureInfo.InvariantCulture));
            sb.AppendLine();
            sb.Append("State Code: " + my.SqlState.ToString(CultureInfo.InvariantCulture));
            return sb.ToString();
        }

        private static string FormatException(SqlException sql)
        {
            var sb = new StringBuilder();
            foreach(SqlError err in sql.Errors)
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("---------------");
                sb.AppendLine();
                sb.AppendLine("Error Message: " + err.Message.ToString(CultureInfo.InvariantCulture));
                sb.AppendLine();
                sb.AppendLine("Error Number: " + err.Number.ToString(CultureInfo.InvariantCulture));
                sb.AppendLine();
                sb.AppendLine("State Code: " + err.State.ToString(CultureInfo.InvariantCulture));
                if(err.LineNumber > 0)
                {
                    sb.AppendLine();
                    sb.AppendLine("Line Number: " + err.LineNumber.ToString(CultureInfo.InvariantCulture));
                }
                if(err.Class > 0)
                {
                    sb.AppendLine();
                    sb.AppendLine("Severity Level: " + err.Class.ToString(CultureInfo.InvariantCulture));
                }
                if(!string.IsNullOrWhiteSpace(err.Procedure))
                {
                    sb.AppendLine();
                    sb.AppendLine("Stored Procedure: " + err.Procedure);
                }
                sb.AppendLine();
                sb.AppendLine("Server Name: " + err.Server);
                sb.AppendLine();
                sb.Append("Source Name: " + err.Source);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Formats a database exception for readability.
        /// </summary>
        /// <param name="ex">The database exception.</param>
        /// <returns>
        /// The formatted exception.
        /// </returns>
        public static string FormatException(Exception ex)
        {
            Contract.Requires(ex != null);

            if(ex is DbException)
            {
                var ole = ex as OleDbException;
                if(ole != null)
                    return FormatException(ole);
                var my = ex as MySqlException;
                if(my != null)
                    return FormatException(my);
                var sql = ex as SqlException;
                if(sql != null)
                    return FormatException(sql);
            }
            return null;
        }

        /// <summary>
        /// Detects if a command timeout occurred.
        /// </summary>
        /// <param name="ex">The database exception.</param>
        /// <returns>
        /// <c>true</c> if timeout; otherwise, <c>false</c>.
        /// </returns>
        public static bool TimeOut(Exception ex)
        {
            Contract.Requires(ex != null);

            if(ex is DbException)
            {
                if(ex is OleDbException)
                    return ex.HResult == -2147217871;
                if(ex is MySqlException)
                    return ex.HResult == -2147467259;
                if(ex is SqlException)
                    return ex.HResult == -2146232060;
            }
            return false;
        }
    }
}
