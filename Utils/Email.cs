﻿namespace Utils
{
    using System;
    using System.Collections.ObjectModel;
    using System.Globalization;
    using System.Net;
    using System.Net.Mail;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mail;

    using Utils.Types;

    /// <summary>
    /// Email message.
    /// </summary>
    public sealed class EmailMessage
    {
        /// <summary>
        /// Gets or sets the From header.
        /// </summary>
        public string From { get; set; }

        /// <summary>
        /// Gets or sets the To header.
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// Gets or sets the Cc (Carbon Copy) header.
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// Gets or sets the Bcc (Blind Carbon Copy) header.
        /// </summary>
        public string Bcc { get; set; }

        /// <summary>
        /// Gets or sets the Subject header.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the Content-Type header is "text/html" or "text/plain".
        /// </summary>
        public bool Html { get; set; }

        private readonly Collection<string> _attachments = new Collection<string>();

        /// <summary>
        /// Gets the attached files.
        /// </summary>
        public Collection<string> Attachments
        {
            get
            {
                return _attachments;
            }
        }
    }

    /// <summary>
    /// Email services.
    /// </summary>
    public static class Email
    {
        #pragma warning disable 618
        private static System.Web.Mail.MailMessage WebMailMessage(EmailMessage email)
        #pragma warning restore 618
        {
            #pragma warning disable 618
            var mm = new System.Web.Mail.MailMessage
            {
                BodyEncoding = Encoding.UTF8,
                Priority = System.Web.Mail.MailPriority.High
            };
            #pragma warning restore 618
            mm.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"] = Config.SmtpServer;
            mm.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"] = Config.SmtpPort;
            mm.Fields["http://schemas.microsoft.com/cdo/configuration/sendusing"] = 2;
            mm.Fields["http://schemas.microsoft.com/cdo/configuration/smtpusessl"] = true;
            if(Config.SmtpAuth)
            {
                mm.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = 1;
                mm.Fields["http://schemas.microsoft.com/cdo/configuration/sendusername"] = Config.SmtpEmail;
                mm.Fields["http://schemas.microsoft.com/cdo/configuration/sendpassword"] = Config.SmtpPass;
            }
            else
                mm.Fields["http://schemas.microsoft.com/cdo/configuration/smtpauthenticate"] = 0;
            mm.Headers.Add("Sender", string.Format(CultureInfo.InvariantCulture, "\"{0}\" <{1}>", Config.WebsiteName, Config.SmtpEmail));
            if(!string.IsNullOrWhiteSpace(email.From))
            {
                mm.From = email.From;
                mm.Headers.Add("Reply-To", email.From);
                mm.Headers.Add("Return-Path", email.From);
            }
            else
            {
                mm.From = (string)mm.Headers["Sender"];
                mm.Headers.Add("Reply-To", mm.From);
                mm.Headers.Add("Return-Path", mm.From);
            }
            if(!string.IsNullOrWhiteSpace(email.To))
                mm.To = email.To;
            if(!string.IsNullOrWhiteSpace(email.Cc))
                mm.Cc = email.Cc;
            if(!string.IsNullOrWhiteSpace(email.Bcc))
                mm.Bcc = email.Bcc;
            mm.Subject = email.Subject;
            mm.Body = email.Body;
            #pragma warning disable 618
            mm.BodyFormat = email.Html ? MailFormat.Html : MailFormat.Text;
            foreach(string file in email.Attachments)
                mm.Attachments.Add(new MailAttachment(file, MailEncoding.Base64));
            #pragma warning restore 618
            return mm;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static System.Net.Mail.MailMessage NetMailMessage(EmailMessage email)
        {
            var mm = new System.Net.Mail.MailMessage
            {
                BodyEncoding = Encoding.UTF8,
                Priority = System.Net.Mail.MailPriority.High,
                Sender = new MailAddress(Config.SmtpEmail, Config.WebsiteName, Encoding.UTF8),
                SubjectEncoding = Encoding.UTF8
            };
            if(!string.IsNullOrWhiteSpace(email.From))
            {
                mm.From = new MailAddress(email.From);
                mm.ReplyToList.Add(email.From);
                mm.Headers.Add("Return-Path", email.From);
            }
            else
            {
                mm.From = mm.Sender;
                mm.ReplyToList.Add(mm.Sender);
                mm.Headers.Add("Return-Path", mm.Sender.ToString());
            }
            if(!string.IsNullOrWhiteSpace(email.To))
                mm.To.Add(email.To);
            if(!string.IsNullOrWhiteSpace(email.Cc))
                mm.CC.Add(email.Cc);
            if(!string.IsNullOrWhiteSpace(email.Bcc))
                mm.Bcc.Add(email.Bcc);
            mm.Subject = email.Subject;
            mm.Body = email.Body;
            mm.IsBodyHtml = email.Html;
            foreach(string file in email.Attachments)
                mm.Attachments.Add(new Attachment(file));
            return mm;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static SmtpClient SmtpClient()
        {
            return new SmtpClient(Config.SmtpServer, Config.SmtpPort)
            {
                Credentials = Config.SmtpAuth ? new NetworkCredential(Config.SmtpEmail, Config.SmtpPass) : null,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = Config.SmtpSsl,
                Timeout = (int)Config.TimeOut.TotalMilliseconds,
                UseDefaultCredentials = false
            };
        }

        private static void MailError(EmailMessage email)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Sender: " + Config.SmtpEmail);
            if(!string.IsNullOrWhiteSpace(email.From))
                sb.AppendLine("From: " + email.From);
            if(!string.IsNullOrWhiteSpace(email.To))
                sb.AppendLine("To: " + email.To);
            if(!string.IsNullOrWhiteSpace(email.Cc))
                sb.AppendLine("CC: " + email.Cc);
            if(!string.IsNullOrWhiteSpace(email.Bcc))
                sb.AppendLine("BCC: " + email.Bcc);
            if(email.Attachments.Count > 0)
                sb.AppendLine(string.Format(CultureInfo.InvariantCulture, "Attachments: \"{0}\"", string.Join("\", \"", email.Attachments)));
            sb.AppendLine("Subject: " + email.Subject);
            sb.AppendLine();
            sb.Append(email.Body);
            RegisteredTask.Start(Logger.LogErrorAsync(sb.ToString(), Http.CurrentRequest));
        }

        private static void MailException(EmailMessage email, Exception ex)
        {
            ex = ex.GetBaseException();
            ex.Data.Add("Sender", Config.SmtpEmail);
            if(!string.IsNullOrWhiteSpace(email.From))
                ex.Data.Add("From", email.From);
            if(!string.IsNullOrWhiteSpace(email.To))
                ex.Data.Add("To", email.To);
            if(!string.IsNullOrWhiteSpace(email.Cc))
                ex.Data.Add("CC", email.Cc);
            if(!string.IsNullOrWhiteSpace(email.Bcc))
                ex.Data.Add("BCC", email.Bcc);
            if(email.Attachments.Count > 0)
                ex.Data.Add("Attachments", string.Format(CultureInfo.InvariantCulture, "\"{0}\"", string.Join("\", \"", email.Attachments)));
            ex.Data.Add("Subject", email.Subject);
            ex.Data.Add("Body", string.Format(CultureInfo.InvariantCulture, "{0}{1}{0}{2}{0}{1}", Environment.NewLine, "---------------", email.Body));
            RegisteredTask.Start(Logger.LogExceptionAsync(false, ex, Http.CurrentRequest));
        }

        /// <summary>
        /// Sends an email message.
        /// </summary>
        /// <remarks>
        /// Supports implicit and explicit SSL authentication. On exception, or optionally always, the whole message is saved in a log file.
        /// </remarks>
        /// <param name="email">The email message to send.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public static bool Send(EmailMessage email, CancellationToken cancel)
        {
            if(!string.IsNullOrWhiteSpace(Config.SmtpServer))
            {
                try
                {
                    if(Config.SmtpSsl && Config.SmtpImplicit)
                    {
                        #pragma warning disable 618
                        System.Web.Mail.MailMessage mm = WebMailMessage(email);
                        cancel.ThrowIfCancellationRequested();
                        SmtpMail.Send(mm);
                        #pragma warning restore 618
                    }
                    else
                    {
                        using(var mm = NetMailMessage(email))
                        {
                            using(var smtp = SmtpClient())
                            {
                                cancel.ThrowIfCancellationRequested();
                                smtp.Send(mm);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    MailException(email, ex);
                    return false;
                }
            }
            else
                MailError(email);
            return true;
        }

        /// <summary>
        /// Sends asynchronously an email message.
        /// </summary>
        /// <remarks>
        /// Supports implicit and explicit SSL authentication. On exception, or optionally always, the whole message is saved in a log file.
        /// </remarks>
        /// <param name="email">The email message to send.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        public static async Task<bool> SendAsync(EmailMessage email, CancellationToken cancel)
        {
            if(!string.IsNullOrWhiteSpace(Config.SmtpServer))
            {
                try
                {
                    if(Config.SmtpSsl && Config.SmtpImplicit)
                    {
                        #pragma warning disable 618
                        System.Web.Mail.MailMessage mm = WebMailMessage(email);
                        await Helper.TaskRun(s => SmtpMail.Send((System.Web.Mail.MailMessage)s), mm, cancel).ConfigureAwait(false);
                        #pragma warning restore 618
                    }
                    else
                    {
                        using(var mm = NetMailMessage(email))
                        {
                            using(var smtp = SmtpClient())
                            {
                                cancel.ThrowIfCancellationRequested();
                                await smtp.SendMailAsync(mm).ConfigureAwait(false);
                            }
                        }
                    }
                }
                catch(Exception ex)
                {
                    MailException(email, ex);
                    return false;
                }
            }
            else
                MailError(email);
            return true;
        }
    } 
}
