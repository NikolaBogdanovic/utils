﻿namespace Utils
{
    using System;
    using System.Collections.Specialized;
    using System.ComponentModel;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Numerics;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;

    /// <summary>
    /// Constants that represent HTTP request methods.
    /// </summary>
    public enum RequestMethod
    {
        /// <summary>
        /// GET
        /// </summary>
        GET,

        /// <summary>
        /// HEAD
        /// </summary>
        HEAD,

        /// <summary>
        /// POST
        /// </summary>
        POST,

        /// <summary>
        /// PUT
        /// </summary>
        PUT,

        /// <summary>
        /// DELETE
        /// </summary>
        DELETE,

        /// <summary>
        /// TRACE
        /// </summary>
        TRACE,

        /// <summary>
        /// CONNECT
        /// </summary>
        CONNECT,

        /// <summary>
        /// OPTIONS
        /// </summary>
        OPTIONS
    }

    /// <summary>
    /// Constants that represent HTTP content types.
    /// </summary>
    public enum ContentType
    {
        /// <summary>
        /// <c>null</c>
        /// </summary>
        NONE,

        /// <summary>
        /// application/xml
        /// </summary>
        XML,

        /// <summary>
        /// text/html
        /// </summary>
        HTML,

        /// <summary>
        /// application/javascript
        /// </summary>
        JS,

        /// <summary>
        /// application/json
        /// </summary>
        JSON,

        /// <summary>
        /// text/plain
        /// </summary>
        TEXT,

        /// <summary>
        /// application/x-www-form-urlencoded
        /// </summary>
        FORM
    }

    /// <summary>
    /// HTTP services.
    /// </summary>
    public static class Http
    {
        /// <summary>
        /// Gets the hard-coded <see cref="RequestMethod"/> names, to avoid reflection.
        /// </summary>
        /// <param name="method">The request method.</param>
        /// <returns>
        /// The name of a request method.
        /// </returns>
        public static string GetName(this RequestMethod method)
        {
            switch(method)
            {
                case RequestMethod.GET:
                    return "GET";
                case RequestMethod.HEAD:
                    return "HEAD";
                case RequestMethod.POST:
                    return "POST";
                case RequestMethod.PUT:
                    return "PUT";
                case RequestMethod.DELETE:
                    return "DELETE";
                case RequestMethod.TRACE:
                    return "TRACE";
                case RequestMethod.CONNECT:
                    return "CONNECT";
                case RequestMethod.OPTIONS:
                    return "OPTIONS";
                default:
                    throw new InvalidEnumArgumentException("method", (int)method, typeof(RequestMethod));
            }
        }

        /// <summary>
        /// Gets the hard-coded <see cref="ContentType"/> names, to avoid reflection.
        /// </summary>
        /// <param name="type">The content type.</param>
        /// <returns>
        /// The name of a content type.
        /// </returns>
        public static string GetName(this ContentType type)
        {
            switch(type)
            {
                case ContentType.NONE:
                    return null;
                case ContentType.XML:
                    return "application/xml; charset=UTF-8";
                case ContentType.HTML:
                    return "text/html; charset=UTF-8";
                case ContentType.JS:
                    return "application/javascript; charset=UTF-8";
                case ContentType.JSON:
                    return "application/json; charset=UTF-8";
                case ContentType.TEXT:
                    return "text/plain; charset=UTF-8";
                case ContentType.FORM:
                    return "application/x-www-form-urlencoded; charset=UTF-8";
                default:
                    throw new InvalidEnumArgumentException("type", (int)type, typeof(ContentType));
            }
        }

        /// <summary>
        /// Gets safely (swallows exceptions) the current request.
        /// </summary>
        public static HttpRequest CurrentRequest
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if(context != null)
                {
                    try
                    {
                        return context.Request;
                    }
                    catch(HttpException)
                    {
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Gets safely (swallows exceptions) the current response.
        /// </summary>
        public static HttpResponse CurrentResponse
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if(context != null)
                {
                    try
                    {
                        return context.Response;
                    }
                    catch(HttpException)
                    {
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Gets the initial UTC timestamp of the current request.
        /// </summary>
        public static DateTime CurrentTimestamp
        {
            get
            {
                HttpContext context = HttpContext.Current;
                if(context == null)
                    return DateTime.UtcNow;
                var dt = context.Items["Request Timestamp"] as DateTime?;
                if(dt == null)
                {
                    lock(context.Items.SyncRoot)
                    {
                        dt = context.Items["Request Timestamp"] as DateTime?;
                        if(dt == null)
                        {
                            dt = context.Timestamp.ToUniversalTime();
                            context.Items["Request Timestamp"] = dt.Value;
                        }
                    }
                }
                return dt.Value;
            }
        }

        /// <summary>
        /// Gets a real client IP address, ignoring any proxies.
        /// </summary>
        /// <param name="serverVariables">The server variables.</param>
        /// <returns>
        /// A client IP address.
        /// </returns>
        public static IPAddress GetClientIPAddress(NameValueCollection serverVariables)
        {
            Contract.Requires(serverVariables != null);

            IPAddress address;
            string ip = serverVariables["HTTP_CLIENT_IP"];
            if(string.IsNullOrWhiteSpace(ip) || !IPAddress.TryParse(ip.Split(',')[0], out address))
            {
                ip = serverVariables["HTTP_X_FORWARDED_FOR"];
                if(string.IsNullOrWhiteSpace(ip) || !IPAddress.TryParse(ip.Split(',')[0], out address))
                {
                    ip = serverVariables["HTTP_X_FORWARDED"];
                    if(string.IsNullOrWhiteSpace(ip) || !IPAddress.TryParse(ip.Split(',')[0], out address))
                    {
                        ip = serverVariables["HTTP_X_CLUSTER_CLIENT_IP"];
                        if(string.IsNullOrWhiteSpace(ip) || !IPAddress.TryParse(ip.Split(',')[0], out address))
                        {
                            ip = serverVariables["HTTP_FORWARDED_FOR"];
                            if(string.IsNullOrWhiteSpace(ip) || !IPAddress.TryParse(ip.Split(',')[0], out address))
                            {
                                ip = serverVariables["HTTP_FORWARDED"];
                                if(string.IsNullOrWhiteSpace(ip) || !IPAddress.TryParse(ip.Split(',')[0], out address))
                                {
                                    ip = serverVariables["REMOTE_ADDR"];
                                    if(string.IsNullOrWhiteSpace(ip) || !IPAddress.TryParse(ip.Split(',')[0], out address))
                                        return null;
                                }
                            }
                        }
                    }
                }
            }
            return address;
        }
        
        /// <summary>
        /// Converts an IPv4 address to a number.
        /// </summary>
        /// <param name="ip4">The IPv4 address.</param>
        /// <returns>
        /// A number.
        /// </returns>
        [CLSCompliant(false)]
        public static uint ConvertIPv4ToNumber(IPAddress ip4)
        {
            Contract.Requires(ip4 != null);
            Contract.Requires(ip4.AddressFamily == AddressFamily.InterNetwork);

            byte[] address = ip4.GetAddressBytes();
            if(BitConverter.IsLittleEndian)
                Array.Reverse(address);
            return BitConverter.ToUInt32(address, 0);
        }

        /// <summary>
        /// Converts an IPv6 address to a number.
        /// </summary>
        /// <param name="ip6">The IPv6 address.</param>
        /// <returns>
        /// A number.
        /// </returns>
        public static BigInteger ConvertIPv6ToNumber(IPAddress ip6)
        {
            Contract.Requires(ip6 != null);
            Contract.Requires(ip6.AddressFamily == AddressFamily.InterNetworkV6);

            byte[] address = ip6.GetAddressBytes();
            if(BitConverter.IsLittleEndian)
                Array.Reverse(address);
            if((address[address.Length - 1] & 128) != 0)
                Array.Resize(ref address, address.Length + 1);
            return new BigInteger(address);
        }

        /// <summary>
        /// Resolves safely (swallows exceptions) an IP address to an IPHostEntry instance.
        /// </summary>
        /// <param name="ip">The IP address.</param>
        /// <returns>
        /// An IPHostEntry instance.
        /// </returns>
        public static IPHostEntry GetHostEntry(IPAddress ip)
        {
            if(ip != null)
            {
                try
                {
                    return Dns.GetHostEntry(ip);
                }
                catch(ArgumentException)
                {
                }
                catch(SocketException)
                {
                }
            }
            return null;
        }

        /// <summary>
        /// Resolves safely (swallows exceptions) a host name or IP address to an IPHostEntry instance.
        /// </summary>
        /// <param name="hostNameOrAddress">The host name or IP address.</param>
        /// <returns>
        /// An IPHostEntry instance.
        /// </returns>
        public static IPHostEntry GetHostEntry(string hostNameOrAddress)
        {
            if(!string.IsNullOrWhiteSpace(hostNameOrAddress) && hostNameOrAddress.Length <= 255)
            {
                try
                {
                    return Dns.GetHostEntry(hostNameOrAddress);
                }
                catch(ArgumentException)
                {
                }
                catch(SocketException)
                {
                }
            }
            return null;
        }

        private static Stream InputStream(HttpRequest req)
        {
            Stream stream;
            if(req.ReadEntityBodyMode != ReadEntityBodyMode.None)
            {
                stream = req.InputStream;
                stream.Position = 0;
            }
            else
                stream = req.GetBufferedInputStream();
            if(stream.CanTimeout)
                stream.ReadTimeout = stream.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
            return stream;
        }

        /// <summary>
        /// Receives a request body.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A request body.
        /// </returns>
        public static string ReceiveRequestBody(HttpRequest req, CancellationToken cancel)
        {
            Contract.Requires(req != null);

            Stream stream = InputStream(req);
            cancel.ThrowIfCancellationRequested();
            return new StreamReader(stream, req.ContentEncoding).ReadToEnd();
        }

        /// <summary>
        /// Receives asynchronously a request body.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A request body.
        /// </returns>
        public static async Task<string> ReceiveRequestBodyAsync(HttpRequest req, CancellationToken cancel)
        {
            Contract.Requires(req != null);

            Stream stream = InputStream(req);
            cancel.ThrowIfCancellationRequested();
            return await new StreamReader(stream, req.ContentEncoding).ReadToEndAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// Gets a raw request.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <param name="body">The request body.</param>
        /// <returns>
        /// A raw request.
        /// </returns>
        public static string GetRawRequest(HttpRequest req, string body)
        {
            Contract.Requires(req != null);

            var sb = new StringBuilder(string.Format(CultureInfo.InvariantCulture, "{0} {1} {2}", req.HttpMethod, req.Url, req.ServerVariables["SERVER_PROTOCOL"]));
            sb.AppendLine();
            sb.AppendLine();
            sb.Append(req.ServerVariables["ALL_RAW"]);
            if(body.Length > 0)
            {
                sb.AppendLine();
                sb.Append(body);
                return sb.ToString();
            }
            return sb.ToString(0, sb.Length - Environment.NewLine.Length);
        }

        /// <summary>
        /// Gets a raw request.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A raw request.
        /// </returns>
        public static string GetRawRequest(HttpRequest req, CancellationToken cancel)
        {
            Contract.Requires(req != null);

            return GetRawRequest(req, ReceiveRequestBody(req, cancel));
        }

        /// <summary>
        /// Gets asynchronously a raw request.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A raw request.
        /// </returns>
        public static async Task<string> GetRawRequestAsync(HttpRequest req, CancellationToken cancel)
        {
            Contract.Requires(req != null);

            return GetRawRequest(req, await ReceiveRequestBodyAsync(req, cancel).ConfigureAwait(false));
        }

        /// <summary>
        /// Traces a request.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <param name="raw">The raw request.</param>
        /// <returns>
        /// A request trace.
        /// </returns>
        public static string TraceRequest(HttpRequest req, string raw)
        {
            Contract.Requires(req != null);

            var sb = new StringBuilder("Request Timestamp: " + req.RequestContext.HttpContext.Timestamp.ToUniversalTime().ToString("O", CultureInfo.InvariantCulture));
            sb.AppendLine();
            sb.AppendLine();
            IPAddress ip = GetClientIPAddress(req.ServerVariables);
            if(ip != null)
            {
                IPHostEntry host = GetHostEntry(ip);
                if(host != null)
                    sb.Append(string.Format(CultureInfo.InvariantCulture, "Host Name: {1}{0}Host Aliases: {2}{0}IP Address: {3}{0}IP Aliases: {4}{0}{0}", Environment.NewLine, host.HostName, string.Join(", ", host.Aliases), ip, string.Join<IPAddress>(" | ", Array.FindAll(host.AddressList, x => !ip.Equals(x)))));
            }
            sb.Append(raw);
            return sb.ToString();
        }

        /// <summary>
        /// Traces a request.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A request trace.
        /// </returns>
        public static string TraceRequest(HttpRequest req, CancellationToken cancel)
        {
            Contract.Requires(req != null);

            return TraceRequest(req, GetRawRequest(req, cancel));
        }

        /// <summary>
        /// Traces asynchronously a request.
        /// </summary>
        /// <param name="req">The request.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A request trace.
        /// </returns>
        public static async Task<string> TraceRequestAsync(HttpRequest req, CancellationToken cancel)
        {
            Contract.Requires(req != null);

            return TraceRequest(req, await GetRawRequestAsync(req, cancel).ConfigureAwait(false));
        }

        /// <summary>
        /// Creates a request that supports cookies and compression.
        /// </summary>
        /// <param name="method">The request method.</param>
        /// <param name="url">The uniform resource identifier.</param>
        /// <returns>
        /// A request.
        /// </returns>
        public static HttpWebRequest CreateRequest(RequestMethod method, string url)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(url));

            var request = WebRequest.CreateHttp(new Uri(url));
            request.Timeout = request.ContinueTimeout = request.ReadWriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            request.CookieContainer = new CookieContainer();
            request.ServicePoint.Expect100Continue = false;
            request.Method = method.GetName();
            return request;
        }

        /// <summary>
        /// Sends a request.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="req">The request to send.</param>
        /// <param name="type">(Optional) the content type.</param>
        /// <param name="body">(Optional) the request body.</param>
        /// <returns>
        /// A response.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        public static HttpWebResponse SendRequest(CancellationToken cancel, HttpWebRequest req, ContentType type = ContentType.NONE, string body = null)
        {
            Contract.Requires(req != null);

            req.ContentType = type.GetName();
            if(body != null)
            {
                byte[] content = Encoding.UTF8.GetBytes(body);
                req.ContentLength = content.Length;
                cancel.ThrowIfCancellationRequested();
                using(Stream stream = req.GetRequestStream())
                {
                    if(stream.CanTimeout)
                        stream.WriteTimeout = stream.ReadTimeout = (int)Config.TimeOut.TotalMilliseconds;
                    cancel.ThrowIfCancellationRequested();
                    stream.Write(content, 0, content.Length);
                }
            }
            cancel.ThrowIfCancellationRequested();
            return (HttpWebResponse)req.GetResponse();
        }

        /// <summary>
        /// Sends asynchronously a request.
        /// </summary>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="req">The request to send.</param>
        /// <param name="type">(Optional) the content type.</param>
        /// <param name="body">(Optional) the request body.</param>
        /// <returns>
        /// A response.
        /// </returns>
        public static async Task<HttpWebResponse> SendRequestAsync(CancellationToken cancel, HttpWebRequest req, ContentType type = ContentType.NONE, string body = null)
        {
            Contract.Requires(req != null);

            req.ContentType = type.GetName();
            if(body != null)
            {
                byte[] content = Encoding.UTF8.GetBytes(body);
                req.ContentLength = content.Length;
                cancel.ThrowIfCancellationRequested();
                using(Stream stream = await req.GetRequestStreamAsync().ConfigureAwait(false))
                {
                    if(stream.CanTimeout)
                        stream.WriteTimeout = stream.ReadTimeout = (int)Config.TimeOut.TotalMilliseconds;
                    await stream.WriteAsync(content, 0, content.Length, cancel).ConfigureAwait(false);
                }
            }
            cancel.ThrowIfCancellationRequested();
            return (HttpWebResponse)(await req.GetResponseAsync().ConfigureAwait(false));
        }

        /// <summary>
        /// Receives a response body.
        /// </summary>
        /// <param name="rsp">The response.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A response body.
        /// </returns>
        public static string ReceiveResponseBody(HttpWebResponse rsp, CancellationToken cancel)
        {
            Contract.Requires(rsp != null);

            cancel.ThrowIfCancellationRequested();
            using(Stream stream = rsp.GetResponseStream())
            {
                // ReSharper disable once PossibleNullReferenceException
                if(stream.CanTimeout)
                    stream.ReadTimeout = stream.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                cancel.ThrowIfCancellationRequested();
                return new StreamReader(stream, !string.IsNullOrEmpty(rsp.ContentEncoding) ? Encoding.GetEncoding(rsp.ContentEncoding) : Encoding.UTF8).ReadToEnd();
            }
        }

        /// <summary>
        /// Receives asynchronously a response body.
        /// </summary>
        /// <param name="rsp">The response.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A response body.
        /// </returns>
        public static async Task<string> ReceiveResponseBodyAsync(HttpWebResponse rsp, CancellationToken cancel)
        {
            Contract.Requires(rsp != null);

            cancel.ThrowIfCancellationRequested();
            using(Stream stream = rsp.GetResponseStream())
            {
                // ReSharper disable once PossibleNullReferenceException
                if(stream.CanTimeout)
                    stream.ReadTimeout = stream.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                cancel.ThrowIfCancellationRequested();
                return await new StreamReader(stream, !string.IsNullOrEmpty(rsp.ContentEncoding) ? Encoding.GetEncoding(rsp.ContentEncoding) : Encoding.UTF8).ReadToEndAsync().ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Gets a raw response.
        /// </summary>
        /// <param name="rsp">The response.</param>
        /// <param name="body">The response body.</param>
        /// <returns>
        /// A raw response.
        /// </returns>
        public static string GetRawResponse(HttpWebResponse rsp, string body)
        {
            Contract.Requires(rsp != null);
            Contract.Requires(body != null);

            var sb = new StringBuilder(string.Format(CultureInfo.InvariantCulture, "{0} {1} HTTP/{2} {3} {4}", rsp.Method, rsp.ResponseUri, rsp.ProtocolVersion, (int)rsp.StatusCode, rsp.StatusDescription));
            sb.AppendLine();
            sb.AppendLine();
            sb.Append(rsp.Headers);
            if(body.Length > 0)
                sb.AppendLine(body);
            return sb.ToString();
        }

        /// <summary>
        /// Decodes form variables from a response body.
        /// </summary>
        /// <param name="rsp">The response.</param>
        /// <param name="body">The response body.</param>
        /// <returns>
        /// A form.
        /// </returns>
        public static NameValueCollection GetResponseForm(HttpWebResponse rsp, string body)
        {
            Contract.Requires(rsp != null);
            Contract.Requires(body != null);

            // some web services send the wrong MIME type: "text/html"
            if(rsp.ContentType.Contains("application/x-www-form-urlencoded") || rsp.ContentType.Contains("text/html"))
                return HttpUtility.ParseQueryString(body, !string.IsNullOrEmpty(rsp.ContentEncoding) ? Encoding.GetEncoding(rsp.ContentEncoding) : Encoding.UTF8);
            return null;
        }
    } 
}
