﻿namespace Utils
{
    using System;
    using System.Collections;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web;

    using Utils.Types;

    /// <summary>
    /// Logging services.
    /// </summary>
    public static class Logger
    {
        private static readonly SemaphoreSlim ErrorLogWriterLock = new SemaphoreSlim(1, 1);
        private static readonly SemaphoreSlim ErrorLogReaderLock = new SemaphoreSlim(int.MaxValue);

        /// <summary>
        /// Gets the error log file.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly FileReadData ErrorLog = new FileReadData(Path.Combine(Config.DataPath, "Error.log"), ErrorLogReaderLock, ErrorLogWriterLock);

        private static readonly SemaphoreSlim ErrorsDirWriterLock = new SemaphoreSlim(1, 1);
        private static readonly SemaphoreSlim ErrorsDirReaderLock = new SemaphoreSlim(int.MaxValue);

        /// <summary>
        /// Gets the errors backup directory.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly FileReadData ErrorsDir = new FileReadData(Path.Combine(Config.DataPath, "Backups", "Errors"), ErrorsDirReaderLock, ErrorsDirWriterLock);

        /// <summary>
        /// Gets an error backup file.
        /// </summary>
        /// <param name="name">The error backup name.</param>
        /// <returns>
        /// An error backup file.
        /// </returns>
        public static FileReadData GetErrorBackup(string name)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            return new FileReadData(Path.Combine(ErrorsDir.Path, name), ErrorsDirReaderLock, ErrorsDirWriterLock);
        }

        /// <summary>
        /// Logs asynchronously a formatted error and a request trace.
        /// </summary>
        /// <param name="error">The error.</param>
        /// <param name="request">The current request.</param>
        /// <returns>
        /// An await-able <see cref="System.Threading.Tasks.Task"/>.
        /// </returns>
        // ReSharper disable once UnusedMethodReturnValue.Global
        public static async Task LogErrorAsync(string error, HttpRequest request)
        {
            var sb = new StringBuilder();
            if(request != null && !RegisteredTask.CancelToken.IsCancellationRequested)
            {
                try
                {
                    sb.AppendLine(await Http.TraceRequestAsync(request, RegisteredTask.CancelToken).ConfigureAwait(false));
                    sb.AppendLine();
                }
                catch(Exception ex)
                {
                    if(!(ex is TimeoutException) && !(ex is OperationCanceledException))
                    {
                        sb.AppendLine(FormatException(ex));
                        sb.AppendLine();
                        sb.AppendLine("---------------------------------------------");
                        sb.AppendLine();
                    }
                }
            }
            sb.AppendLine(error);
            sb.AppendLine();
            sb.AppendLine("---------------------------------------------");
            sb.AppendLine();
            var file = new FileWriteData(ErrorLog, sb.ToString(), true);
            try
            {
                await FileIO.WriteFileAsync(file, CancellationToken.None).ConfigureAwait(false);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch
            {
            }
        }

        private static string FormatException(Exception ex)
        {
            Contract.Requires(ex != null);

            var sb = new StringBuilder("Exception Message: " + ex.Message);
            sb.AppendLine();
            sb.AppendLine();
            sb.Append("HResult Code: " + ex.HResult.ToString(CultureInfo.InvariantCulture));
            var http = ex as HttpException;
            if(http != null)
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("Error Message: " + http.GetHtmlErrorMessage());
                sb.AppendLine();
                sb.AppendLine("Status Code: " + http.GetHttpCode().ToString(CultureInfo.InvariantCulture));
                sb.AppendLine();
                sb.Append("Event Code: " + http.WebEventCode.ToString(CultureInfo.InvariantCulture));
            }
            else
                sb.Append(Database.FormatException(ex));
            if(ex.Data.Count > 0)
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.Append("Data Collection:");
                foreach(DictionaryEntry de in ex.Data)
                {
                    sb.AppendLine();
                    sb.AppendFormat(CultureInfo.InvariantCulture, "{0} = {1}", de.Key, de.Value);
                }
            }
            if(ex.HelpLink != null)
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.Append("Help Link: " + ex.HelpLink);
            }
            if(ex.Source != null)
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.Append("Source Name: " + ex.Source);
            }
            if(ex.TargetSite != null)
            {
                sb.AppendLine();
                sb.AppendLine();
                sb.AppendLine("Target Site: " + ex.TargetSite);
                sb.AppendLine();
                sb.AppendLine("Stack Trace:");
                sb.Append(ex.StackTrace);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Logs asynchronously a formatted exception and a request trace.
        /// </summary>
        /// <param name="email"><c>true</c> to email, <c>false</c> to save.</param>
        /// <param name="ex">The exception.</param>
        /// <param name="request">The current request.</param>
        /// <returns>
        /// An await-able <see cref="System.Threading.Tasks.Task"/>.
        /// </returns>
        /// ReSharper disable once UnusedMethodReturnValue.Global
        public static async Task LogExceptionAsync(bool email, Exception ex, HttpRequest request)
        {
            Contract.Requires(ex != null);

            string error;
            ex = ex.GetBaseException();
            var aggr = ex as AggregateException;
            if(aggr != null)
            {
                var sb = new StringBuilder();
                foreach(Exception ie in aggr.Flatten().InnerExceptions)
                {
                    sb.AppendLine(FormatException(ie));
                    sb.AppendLine();
                    sb.AppendLine("------------------------------");
                    sb.AppendLine();
                }
                error = sb.ToString(0, sb.Length - (30 + (4 * Environment.NewLine.Length)));
            }
            else
                error = FormatException(ex);
            if(email && !string.IsNullOrWhiteSpace(Config.SmtpServer) && !RegisteredTask.CancelToken.IsCancellationRequested)
            {
                var sb = new StringBuilder();
                if(request != null)
                {
                    try
                    {
                        sb.AppendLine(await Http.TraceRequestAsync(request, RegisteredTask.CancelToken).ConfigureAwait(false));
                        sb.AppendLine();
                    }
                    catch(Exception e)
                    {
                        if(!(e is TimeoutException) && !(e is OperationCanceledException))
                        {
                            sb.AppendLine(FormatException(e));
                            sb.AppendLine();
                            sb.AppendLine("---------------------------------------------");
                            sb.AppendLine();
                        }
                    }
                }
                sb.Append(error);
                await Email.SendAsync(
                    new EmailMessage
                    {
                        To = Config.AdminEmail,
                        Subject = "Error occurred on website " + Link.Absolute("/", false),
                        Body = sb.ToString()
                    },
                    RegisteredTask.CancelToken).ConfigureAwait(false);
            }
            else
                await LogErrorAsync(error, request).ConfigureAwait(false);
        }
    } 
}
