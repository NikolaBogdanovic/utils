﻿namespace Utils
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.Contracts;
    using System.Drawing;
    using System.IO;
    using System.Runtime.Caching;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// File read data.
    /// </summary>
    public sealed class FileReadData
    {
        /// <summary>
        /// Gets the file path.
        /// </summary>
        public string Path { get; private set; }

        /// <summary>
        /// Gets the file reader lock.
        /// </summary>
        public SemaphoreSlim PathReaderLock { get; private set; }

        /// <summary>
        /// Gets the file writer lock.
        /// </summary>
        public SemaphoreSlim PathWriterLock { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileReadData"/> class.
        /// </summary>
        /// <param name="path">The file path.</param>
        /// <param name="readerLock">(Optional) the file reader lock (<c>new SemaphoreSlim(int.MaxValue)</c>).</param>
        /// <param name="writerLock">(Optional) the file writer lock (<c>new SemaphoreSlim(1, 1)</c>).</param>
        public FileReadData(string path, SemaphoreSlim readerLock = null, SemaphoreSlim writerLock = null)
        {
            Path = path;
            PathReaderLock = readerLock;
            PathWriterLock = writerLock;
        }
    }

    /// <summary>
    /// File write data.
    /// </summary>
    public sealed class FileWriteData
    {
        /// <summary>
        /// Gets the file read data.
        /// </summary>
        public FileReadData Read { get; private set; }

        /// <summary>
        /// Gets the file content.
        /// </summary>
        public string Content { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to append to, or overwrite the existing content.
        /// </summary>
        public bool Append { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileWriteData"/> class.
        /// </summary>
        /// <param name="read">The file read data.</param>
        /// <param name="content">The file content.</param>
        /// <param name="append"><c>true</c> to append to, <c>false</c> to overwrite the existing content.</param>
        public FileWriteData(FileReadData read, string content, bool append)
        {
            Contract.Requires(read != null);

            Read = read;
            Content = content;
            Append = append;
        }
    }

    /// <summary>
    /// File copy data.
    /// </summary>
    public sealed class FileCopyData
    {
        /// <summary>
        /// Gets the file read data.
        /// </summary>
        public FileReadData Read { get; private set; }

        /// <summary>
        /// Gets the copy read data.
        /// </summary>
        public FileReadData Copy { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to move or copy the original file.
        /// </summary>
        public bool Move { get; private set; }

        /// <summary>
        /// Gets a value indicating whether to overwrite or keep the existing copy.
        /// </summary>
        public bool Over { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileCopyData"/> class.
        /// </summary>
        /// <param name="read">The original file read data.</param>
        /// <param name="copy">The file copy read data.</param>
        /// <param name="move"><c>true</c> to move, <c>false</c> to copy the original file.</param>
        /// <param name="over"><c>true</c> to overwrite, <c>false</c> to keep the existing copy.</param>
        public FileCopyData(FileReadData read, FileReadData copy, bool move, bool over)
        {
            Contract.Requires(read != null);
            Contract.Requires(copy != null);

            Read = read;
            Copy = copy;
            Move = move;
            Over = over;
        }
    }

    /// <summary>
    /// File search data.
    /// </summary>
    public sealed class FileSearchData
    {
        /// <summary>
        /// Gets the file read data.
        /// </summary>
        public FileReadData Read { get; private set; }

        /// <summary>
        /// Gets the file search pattern.
        /// </summary>
        public string Pattern { get; private set; }

        /// <summary>
        /// Gets the file search option.
        /// </summary>
        public SearchOption Option { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSearchData"/> class.
        /// </summary>
        /// <param name="read">The file read data.</param>
        /// <param name="pattern">The file search pattern.</param>
        /// <param name="option">The file search option.</param>
        public FileSearchData(FileReadData read, string pattern, SearchOption option)
        {
            Contract.Requires(read != null);
            Contract.Requires(pattern != null);

            Read = read;
            Pattern = pattern;
            Option = option;
        }
    }

    /// <summary>
    /// File input output services.
    /// </summary>
    public static class FileIO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private static FileStream GetFile(string path, FileMode mode, FileAccess access, FileShare share, bool async, CancellationToken cancel)
        {
            // ReSharper disable InconsistentNaming
            //// const long STG_E_SHAREVIOLATION = 0x80030020, STG_E_LOCKVIOLATION = 0x80030021, STIERR_SHARING_VIOLATION = 0x80070020, STIERR_LOCK_VIOLATION = 0x80070021, ERROR_SHARING_VIOLATION = 0x20, ERROR_LOCK_VIOLATION = 0x21;
            const int STG_E_SHAREVIOLATION = -2147287008, STG_E_LOCKVIOLATION = -2147287007, STIERR_SHARING_VIOLATION = -2147024864, STIERR_LOCK_VIOLATION = -2147024863, ERROR_SHARING_VIOLATION = 32, ERROR_LOCK_VIOLATION = 33;
            // ReSharper restore InconsistentNaming
            cancel.ThrowIfCancellationRequested();
            FileStream file = null;
            try
            {
                file = new FileStream(path, mode, access, share, 8192, async);
            }
            catch(Exception ex)
            {
                if(file != null)
                    file.Dispose();
                if(ex is IOException && (ex.HResult == STG_E_SHAREVIOLATION || ex.HResult == STG_E_LOCKVIOLATION || ex.HResult == STIERR_SHARING_VIOLATION || ex.HResult == STIERR_LOCK_VIOLATION || ex.HResult == ERROR_SHARING_VIOLATION || ex.HResult == ERROR_LOCK_VIOLATION))
                    return null;
                throw;
            }
            if(file.CanTimeout)
                file.ReadTimeout = file.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
            return file;
        }

        /// <summary>
        /// Gets a file stream.
        /// </summary>
        /// <remarks>
        /// If a file raises a sharing or a lock violation, access is retried with increasing delay until timeout.
        /// </remarks>
        /// <param name="path">The file path.</param>
        /// <param name="mode">The file open mode.</param>
        /// <param name="access">The file access permissions.</param>
        /// <param name="share">The file sharing permissions.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A file stream.
        /// </returns>
        public static FileStream GetFile(string path, FileMode mode, FileAccess access, FileShare share, CancellationToken cancel)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(path));

            int delay = 10;
            while(true)
            {
                FileStream file = GetFile(path, mode, access, share, false, cancel);
                if(file != null)
                    return file;
                cancel.ThrowIfCancellationRequested();
                Thread.Sleep(delay);
                delay *= 2;
            }
        }

        /// <summary>
        /// Gets asynchronously a file stream.
        /// </summary>
        /// <remarks>
        /// If a file raises a sharing or a lock violation, access is retried with increasing delay until timeout.
        /// </remarks>
        /// <param name="path">The file path.</param>
        /// <param name="mode">The file open mode.</param>
        /// <param name="access">The file access permissions.</param>
        /// <param name="share">The file sharing permissions.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A file stream.
        /// </returns>
        public static async Task<FileStream> GetFileAsync(string path, FileMode mode, FileAccess access, FileShare share, CancellationToken cancel)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(path));

            int delay = 10;
            while(true)
            {
                FileStream file = GetFile(path, mode, access, share, true, cancel);
                if(file != null)
                    return file;
                await Task.Delay(delay, cancel).ConfigureAwait(false);
                delay *= 2;
            }
        }

        /// <summary>
        /// Gets the image dimensions from its properties, without reading the whole file.
        /// </summary>
        /// <param name="data">The file read data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The image width and height in pixels.
        /// </returns>
        public static Size GetImageSize(FileReadData data, CancellationToken cancel)
        {
            Contract.Requires(data != null);

            using(var file = GetFile(data.Path, FileMode.Open, FileAccess.Read, FileShare.Read, false, cancel))
            {
                cancel.ThrowIfCancellationRequested();
                BitmapDecoder bd = BitmapDecoder.Create(file, BitmapCreateOptions.DelayCreation | BitmapCreateOptions.IgnoreColorProfile, BitmapCacheOption.None);
                if(bd.Frames.Count > 0)
                    return new Size(bd.Frames[0].PixelWidth, bd.Frames[0].PixelHeight);
                cancel.ThrowIfCancellationRequested();
                using(Image img = Image.FromStream(file, false, false))
                    return img.Size;
            }
        }

        /// <summary>
        /// Gets asynchronously the image dimensions from its properties, without reading the whole file.
        /// </summary>
        /// <param name="data">The file read data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// The image width and height in pixels.
        /// </returns>
        public static async Task<Size> GetImageSizeAsync(FileReadData data, CancellationToken cancel)
        {
            Contract.Requires(data != null);

            return await Helper.TaskRun(
                o =>
                {
                    var state = (AsyncState)o;
                    return GetImageSize((FileReadData)state.ExtraData, state.CancelToken);
                },
                new AsyncState(data, cancel),
                cancel).ConfigureAwait(false);
        }

        /// <summary>
        /// Reads a file content.
        /// </summary>
        /// <param name="data">The file read data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A file content.
        /// </returns>
        public static string ReadFile(FileReadData data, CancellationToken cancel)
        {
            Contract.Requires(data != null);

            bool writer = false, reader = false;
            try
            {
                if(data.PathWriterLock != null && data.PathReaderLock != null)
                {
                    data.PathWriterLock.Wait(cancel);
                    writer = true;
                    data.PathReaderLock.Wait(cancel);
                    reader = true;
                    data.PathWriterLock.Release();
                    writer = false;
                }
                using(var file = GetFile(data.Path, FileMode.Open, FileAccess.Read, FileShare.Read, cancel))
                {
                    cancel.ThrowIfCancellationRequested();
                    return new StreamReader(file).ReadToEnd();
                }
            }
            finally
            {
                if(reader)
                    data.PathReaderLock.Release();
                if(writer)
                    data.PathWriterLock.Release();
            }
        }

        /// <summary>
        /// Reads asynchronously a file content.
        /// </summary>
        /// <param name="data">The file read data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A file content.
        /// </returns>
        public static async Task<string> ReadFileAsync(FileReadData data, CancellationToken cancel)
        {
            Contract.Requires(data != null);

            bool writer = false, reader = false;
            try
            {
                if(data.PathWriterLock != null && data.PathReaderLock != null)
                {
                    await data.PathWriterLock.WaitAsync(cancel).ConfigureAwait(false);
                    writer = true;
                    await data.PathReaderLock.WaitAsync(cancel).ConfigureAwait(false);
                    reader = true;
                    data.PathWriterLock.Release();
                    writer = false;
                }
                using(var file = await GetFileAsync(data.Path, FileMode.Open, FileAccess.Read, FileShare.Read, cancel).ConfigureAwait(false))
                {
                    cancel.ThrowIfCancellationRequested();
                    return await new StreamReader(file).ReadToEndAsync().ConfigureAwait(false);
                }
            }
            finally
            {
                if(reader)
                    data.PathReaderLock.Release();
                if(writer)
                    data.PathWriterLock.Release();
            }
        }

        /// <summary>
        /// Writes a file content.
        /// </summary>
        /// <param name="data">The file write data.</param>
        /// <param name="cancel">The cancellation token.</param>
        public static void WriteFile(FileWriteData data, CancellationToken cancel)
        {
            Contract.Requires(data != null);

            bool writer = false;
            try
            {
                int delay = 10;
                if(data.Read.PathWriterLock != null)
                {
                    data.Read.PathWriterLock.Wait(cancel);
                    writer = true;
                    if(data.Read.PathReaderLock != null)
                    {
                        while(data.Read.PathReaderLock.CurrentCount != int.MaxValue)
                        {
                            cancel.ThrowIfCancellationRequested();
                            Thread.Sleep(delay);
                            delay *= 2;
                        }
                    }
                }
                using(var file = GetFile(data.Read.Path, data.Append ? FileMode.Append : FileMode.Create, FileAccess.Write, FileShare.None, cancel))
                {
                    var stream = new StreamWriter(file);
                    stream.Write(data.Content);
                    stream.Flush();
                }
            }
            finally
            {
                if(writer)
                    data.Read.PathWriterLock.Release();
            }
        }

        /// <summary>
        /// Writes asynchronously a file content.
        /// </summary>
        /// <param name="data">The file write data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// An await-able <see cref="System.Threading.Tasks.Task"/>.
        /// </returns>
        public static async Task WriteFileAsync(FileWriteData data, CancellationToken cancel)
        {
            Contract.Requires(data != null);

            bool writer = false;
            try
            {
                int delay = 10;
                if(data.Read.PathWriterLock != null)
                {
                    await data.Read.PathWriterLock.WaitAsync(cancel).ConfigureAwait(false);
                    writer = true;
                    if(data.Read.PathReaderLock != null)
                    {
                        while(data.Read.PathReaderLock.CurrentCount != int.MaxValue)
                        {
                            await Task.Delay(delay, cancel).ConfigureAwait(false);
                            delay *= 2;
                        }
                    }
                }
                using(var file = await GetFileAsync(data.Read.Path, data.Append ? FileMode.Append : FileMode.Create, FileAccess.Write, FileShare.None, cancel).ConfigureAwait(false))
                {
                    var stream = new StreamWriter(file);
                    await stream.WriteAsync(data.Content).ConfigureAwait(false);
                    await stream.FlushAsync().ConfigureAwait(false);
                }
            }
            finally
            {
                if(writer)
                    data.Read.PathWriterLock.Release();
            }
        }

        /// <summary>
        /// Copies a file.
        /// </summary>
        /// <param name="data">The file copy data.</param>
        /// <param name="cancel">The cancellation token.</param>
        public static void CopyFile(FileCopyData data, CancellationToken cancel)
        {
            Contract.Requires(data != null);

            bool writerCopy = false, writerPath = false, readerPath = false;
            try
            {
                int delay = 10;
                if(data.Read.PathWriterLock != null)
                {
                    data.Read.PathWriterLock.Wait(cancel);
                    writerPath = true;
                    if(data.Read.PathReaderLock != null)
                    {
                        if(data.Move)
                        {
                            while(data.Read.PathReaderLock.CurrentCount != int.MaxValue)
                            {
                                cancel.ThrowIfCancellationRequested();
                                Thread.Sleep(delay);
                                delay *= 2;
                            }
                        }
                        else
                        {
                            data.Read.PathReaderLock.Wait(cancel);
                            readerPath = true;
                            data.Read.PathWriterLock.Release();
                            writerPath = false;
                        }
                    }
                }
                delay = 10;
                if(data.Copy.PathWriterLock != null)
                {
                    data.Copy.PathWriterLock.Wait(cancel);
                    writerCopy = true;
                    if(data.Copy.PathReaderLock != null)
                    {
                        while(data.Copy.PathReaderLock.CurrentCount != int.MaxValue)
                        {
                            cancel.ThrowIfCancellationRequested();
                            Thread.Sleep(delay);
                            delay *= 2;
                        }
                    }
                }
                using(var filePath = GetFile(data.Read.Path, FileMode.Open, data.Move ? FileAccess.ReadWrite : FileAccess.Read, data.Move ? FileShare.Delete : FileShare.Read, cancel))
                {
                    using(var fileCopy = GetFile(data.Copy.Path, data.Over ? FileMode.Create : FileMode.CreateNew, FileAccess.Write, FileShare.None, cancel))
                    {
                        cancel.ThrowIfCancellationRequested();
                        filePath.CopyTo(fileCopy, 8192);
                    }
                    if(data.Move)
                        File.Delete(data.Read.Path);
                }
            }
            finally
            {
                if(writerCopy)
                    data.Copy.PathWriterLock.Release();
                if(readerPath)
                    data.Read.PathReaderLock.Release();
                if(writerPath)
                    data.Read.PathWriterLock.Release();
            }
        }

        /// <summary>
        /// Copies asynchronously a file.
        /// </summary>
        /// <param name="data">The file copy data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// An await-able <see cref="System.Threading.Tasks.Task"/>.
        /// </returns>
        public static async Task CopyFileAsync(FileCopyData data, CancellationToken cancel)
        {
            Contract.Requires(data != null);
            
            bool writerCopy = false, writerPath = false, readerPath = false;
            try
            {
                int delay = 10;
                if(data.Read.PathWriterLock != null)
                {
                    await data.Read.PathWriterLock.WaitAsync(cancel).ConfigureAwait(false);
                    writerPath = true;
                    if(data.Read.PathReaderLock != null)
                    {
                        if(data.Move)
                        {
                            while(data.Read.PathReaderLock.CurrentCount != int.MaxValue)
                            {
                                await Task.Delay(delay, cancel).ConfigureAwait(false);
                                delay *= 2;
                            }
                        }
                        else
                        {
                            await data.Read.PathReaderLock.WaitAsync(cancel).ConfigureAwait(false);
                            readerPath = true;
                            data.Read.PathWriterLock.Release();
                            writerPath = false;
                        }
                    }
                }
                delay = 10;
                if(data.Copy.PathWriterLock != null)
                {
                    await data.Copy.PathWriterLock.WaitAsync(cancel).ConfigureAwait(false);
                    writerCopy = true;
                    if(data.Copy.PathReaderLock != null)
                    {
                        while(data.Copy.PathReaderLock.CurrentCount != int.MaxValue)
                        {
                            await Task.Delay(delay, cancel).ConfigureAwait(false);
                            delay *= 2;
                        }
                    }
                }
                using(var filePath = await GetFileAsync(data.Read.Path, FileMode.Open, data.Move ? FileAccess.ReadWrite : FileAccess.Read, data.Move ? FileShare.Delete : FileShare.Read, cancel).ConfigureAwait(false))
                {
                    using(var fileCopy = await GetFileAsync(data.Copy.Path, data.Over ? FileMode.Create : FileMode.CreateNew, FileAccess.Write, FileShare.None, cancel).ConfigureAwait(false))
                        await filePath.CopyToAsync(fileCopy, 8192, cancel).ConfigureAwait(false);
                    if(data.Move)
                        File.Delete(data.Read.Path);
                }
            }
            finally
            {
                if(writerCopy)
                    data.Copy.PathWriterLock.Release();
                if(readerPath)
                    data.Read.PathReaderLock.Release();
                if(writerPath)
                    data.Read.PathWriterLock.Release();
            }
        }

        private static IReadOnlyList<T> SearchDirectory<T>(Func<string, T> func, FileSearchData data, CancellationToken cancel) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(data != null);

            var files = new List<T>(0);
            cancel.ThrowIfCancellationRequested();
            foreach(string name in Directory.EnumerateFiles(data.Read.Path, data.Pattern, data.Option))
            {
                files.Add(func(name));
                cancel.ThrowIfCancellationRequested();
            }
            return files;
        }

        /// <summary>
        /// Searches for files.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="modify"><c>true</c> to modify, <c>false</c> to keep found files.</param>
        /// <param name="data">The file search data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// Found files.
        /// </returns>
        public static IReadOnlyList<T> FindFiles<T>(Func<string, T> func, bool modify, FileSearchData data, CancellationToken cancel) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(data != null);

            bool writer = false, reader = false;
            try
            {
                int delay = 10;
                if(data.Read.PathWriterLock != null)
                {
                    data.Read.PathWriterLock.Wait(cancel);
                    writer = true;
                    if(data.Read.PathReaderLock != null)
                    {
                        if(modify)
                        {
                            while(data.Read.PathReaderLock.CurrentCount != int.MaxValue)
                            {
                                cancel.ThrowIfCancellationRequested();
                                Thread.Sleep(delay);
                                delay *= 2;
                            }
                        }
                        else
                        {
                            data.Read.PathReaderLock.Wait(cancel);
                            reader = true;
                            data.Read.PathWriterLock.Release();
                            writer = false;
                        }
                    }
                }
                return SearchDirectory(func, data, cancel);
            }
            finally
            {
                if(reader)
                    data.Read.PathReaderLock.Release();
                if(writer)
                    data.Read.PathWriterLock.Release();
            }
        }

        /// <summary>
        /// Searches asynchronously for files.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="modify"><c>true</c> to modify, <c>false</c> to keep found files.</param>
        /// <param name="data">The file search data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// Found files.
        /// </returns>
        public static async Task<IReadOnlyList<T>> FindFilesAsync<T>(Func<string, T> func, bool modify, FileSearchData data, CancellationToken cancel) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(data != null);
            
            bool writer = false, reader = false;
            try
            {
                int delay = 10;
                if(data.Read.PathWriterLock != null)
                {
                    await data.Read.PathWriterLock.WaitAsync(cancel).ConfigureAwait(false);
                    writer = true;
                    if(data.Read.PathReaderLock != null)
                    {
                        if(modify)
                        {
                            while(data.Read.PathReaderLock.CurrentCount != int.MaxValue)
                            {
                                await Task.Delay(delay, cancel).ConfigureAwait(false);
                                delay *= 2;
                            }
                        }
                        else
                        {
                            await data.Read.PathReaderLock.WaitAsync(cancel).ConfigureAwait(false);
                            reader = true;
                            data.Read.PathWriterLock.Release();
                            writer = false;
                        }
                    }
                }
                return await Helper.TaskRun(
                    o =>
                    {
                        var state = (AsyncState)o;
                        var extra = (FileSearchData)state.ExtraData;
                        return SearchDirectory(func, extra, state.CancelToken);
                    },
                    new AsyncState(data, cancel),
                    cancel).ConfigureAwait(false);
            }
            finally
            {
                if(reader)
                    data.Read.PathReaderLock.Release();
                if(writer)
                    data.Read.PathWriterLock.Release();
            }
        }

        /// <summary>
        /// Gets a cached file content.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="data">The file read data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// A cached file content.
        /// </returns>
        public static T ReadFileCache<T>(Func<string, T> func, FileReadData data, CancellationToken cancel, SemaphoreSlim sync, bool update = false, CacheItemPolicy policy = null) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(data != null);
            Contract.Requires(sync != null);

            MemoryCache cache = MemoryCache.Default;
            T result = !update ? cache[data.Path] as T : null;
            if(result == null)
            {
                sync.WaitAsync(cancel);
                try
                {
                    if(update)
                        cache.Remove(data.Path);
                    else
                        result = cache.Get(data.Path) as T;
                    if(result == null)
                    {
                        result = func(ReadFile(data, cancel));
                        if(policy == null)
                            policy = new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            };
                        policy.ChangeMonitors.Add(new HostFileChangeMonitor(new[] { data.Path }));
                        if(result != null)
                            cache.Add(data.Path, result, policy);
                    }
                }
                finally
                {
                    sync.Release();
                }
            }
            return result;
        }

        /// <summary>
        /// Gets asynchronously a cached file content.
        /// </summary>
        /// <typeparam name="T">The generic class type parameter.</typeparam>
        /// <param name="func">The factory pattern method.</param>
        /// <param name="data">The file read data.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <param name="sync">The cache lock.</param>
        /// <param name="update">(Optional) <c>true</c> to update, or <c>false</c> to read cache.</param>
        /// <param name="policy">(Optional) eviction details; sliding expiration by default.</param>
        /// <returns>
        /// A cached file content.
        /// </returns>
        public static async Task<T> ReadFileCacheAsync<T>(Func<string, T> func, FileReadData data, CancellationToken cancel, SemaphoreSlim sync, bool update = false, CacheItemPolicy policy = null) where T : class
        {
            Contract.Requires(func != null);
            Contract.Requires(data != null);
            Contract.Requires(sync != null);

            MemoryCache cache = MemoryCache.Default;
            T result = !update ? cache[data.Path] as T : null;
            if(result == null)
            {
                await sync.WaitAsync(cancel).ConfigureAwait(false);
                try
                {
                    if(update)
                        cache.Remove(data.Path);
                    else
                        result = cache.Get(data.Path) as T;
                    if(result == null)
                    {
                        result = func(await ReadFileAsync(data, cancel).ConfigureAwait(false));
                        if(policy == null)
                            policy = new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            };
                        policy.ChangeMonitors.Add(new HostFileChangeMonitor(new[] { data.Path }));
                        if(result != null)
                            cache.Add(data.Path, result, policy);
                    }
                }
                finally
                {
                    sync.Release();
                }
            }
            return result;
        }
    } 
}
