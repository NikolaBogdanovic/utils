﻿namespace Utils
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Runtime.Caching;
    using System.Text;
    using System.Web;

    using Utils.Types;

    /// <summary>
    /// Globalization (g11n), internationalization (i18n), and localization (l10n) services.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public static class G11n
    {
        /// <summary>
        /// Gets a cached neutral or a specified culture.
        /// </summary>
        /// <remarks>
        /// Safely returns <c>null</c> if the culture is not found, but throws all other exceptions.
        /// </remarks>
        /// <param name="name">The culture name.</param>
        /// <returns>
        /// A cached neutral or a specified culture.
        /// </returns>
        public static CultureInfo GetCulture(string name)
        {
            if(!string.IsNullOrWhiteSpace(name))
            {
                if(name.Contains("-"))
                {
                    try
                    {
                        return CultureInfo.GetCultureInfo(name);
                    }
                    catch(CultureNotFoundException)
                    {
                    }
                }
                else
                {
                    try
                    {
                        return CultureInfo.ReadOnly(CultureInfo.CreateSpecificCulture(name));
                    }
                    catch(CultureNotFoundException)
                    {
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets a cached windows time zone.
        /// </summary>
        /// <remarks>
        /// Safely returns <c>null</c> if the time zone is not found, but throws all other exceptions.
        /// </remarks>
        /// <param name="id">The time zone identifier.</param>
        /// <returns>
        /// A cached windows time zone.
        /// </returns>
        public static TimeZoneInfo GetTimeZone(string id)
        {
            if(!string.IsNullOrWhiteSpace(id))
            {
                try
                {
                    return TimeZoneInfo.FindSystemTimeZoneById(id);
                }
                catch(TimeZoneNotFoundException)
                {
                }
            }
            return null;
        }
        
        private static readonly object SpecificCulturesLock = new object();

        /// <summary>
        /// Gets the cached specific cultures list.
        /// </summary>
        public static IReadOnlyList<KeyValuePair<string, string>> SpecificCultures
        {
            get
            {
                MemoryCache cache = MemoryCache.Default;
                var array = cache["Specific Cultures"] as KeyValuePair<string, string>[];
                if(array == null)
                {
                    lock(SpecificCulturesLock)
                    {
                        array = cache.Get("Specific Cultures") as KeyValuePair<string, string>[];
                        if(array == null)
                        {
                            CultureInfo[] cults = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
                            array = new KeyValuePair<string, string>[cults.Length];
                            for(int i = 0, j = cults.Length; i < j; i++)
                            {
                                CultureInfo ci = cults[i];
                                array[i] = new KeyValuePair<string, string>(ci.Name, ci.EnglishName);
                            }
                            Array.Sort(array, (kvp1, kvp2) => string.CompareOrdinal(kvp1.Value, kvp2.Value));
                            cache.Add(
                                "Specific Cultures",
                                array,
                                new CacheItemPolicy
                                {
                                    SlidingExpiration = Config.SlidingExpiration
                                });
                        }
                    }
                }
                return array;
            }
        }

        private static readonly object IsoCurrencySymbolsLock = new object();

        /// <summary>
        /// Gets the cached ISO currency symbols list.
        /// </summary>
        public static IEnumerable<KeyValuePair<string, string>> IsoCurrencySymbols
        {
            get
            {
                MemoryCache cache = MemoryCache.Default;
                var list = cache["ISO Currency Symbols"] as List<KeyValuePair<string, string>>;
                if(list == null)
                {
                    lock(IsoCurrencySymbolsLock)
                    {
                        list = cache.Get("ISO Currency Symbols") as List<KeyValuePair<string, string>>;
                        if(list == null)
                        {
                            IReadOnlyList<KeyValuePair<string, string>> cults = SpecificCultures;
                            list = new List<KeyValuePair<string, string>>(cults.Count);
                            foreach(KeyValuePair<string, string> kvp in cults)
                            {
                                var reg = new RegionInfo(kvp.Key);
                                if(!list.Exists(x => x.Key == reg.ISOCurrencySymbol))
                                    list.Add(new KeyValuePair<string, string>(reg.ISOCurrencySymbol, reg.CurrencyEnglishName));
                            }
                            list.Sort((kvp1, kvp2) => string.CompareOrdinal(kvp1.Value, kvp2.Value));
                            cache.Add(
                                "ISO Currency Symbols",
                                list,
                                new CacheItemPolicy
                                {
                                    SlidingExpiration = Config.SlidingExpiration
                                });
                        }
                    }
                }
                return list;
            }
        }

        private static readonly object WindowsTimeZonesLock = new object();

        /// <summary>
        /// Gets the cached windows time zones list.
        /// </summary>
        public static IReadOnlyList<KeyValuePair<string, string>> WindowsTimeZones
        {
            get
            {
                MemoryCache cache = MemoryCache.Default;
                var array = cache["Windows Time Zones"] as KeyValuePair<string, string>[];
                if(array == null)
                {
                    lock(WindowsTimeZonesLock)
                    {
                        array = cache.Get("Windows Time Zones") as KeyValuePair<string, string>[];
                        if(array == null)
                        {
                            ReadOnlyCollection<TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones();
                            TimeZoneInfo.ClearCachedData();
                            array = new KeyValuePair<string, string>[zones.Count];
                            for(int i = 0, j = zones.Count; i < j; i++)
                            {
                                TimeZoneInfo tzi = zones[i];
                                array[i] = new KeyValuePair<string, string>(tzi.Id, tzi.DisplayName);
                            }
                            cache.Add(
                                "Windows Time Zones",
                                array,
                                new CacheItemPolicy
                                {
                                    SlidingExpiration = Config.SlidingExpiration
                                });
                        }
                    }
                }
                return array;
            }
        }

        /// <summary>
        /// Gets the client time zone.
        /// </summary>
        /// <remarks>
        /// If <c>Context.Items["ClientTimeZone"]</c> is not set for the current request, the server time zone is used instead.
        /// </remarks>
        public static TimeZoneInfo ClientTimeZone
        {
            get
            {
                return GetTimeZone((string)HttpContext.Current.Items["ClientTimeZone"]) ?? Config.ServerTimeZone;
            }
        }

        /// <summary>
        /// Converts a date and time from server to client time zone.
        /// </summary>
        /// <remarks>
        /// The <see cref="System.DateTime.Kind"/> must not be <see cref="System.DateTimeKind.Local"/>, even if the local time zone is the same as server.
        /// </remarks>
        /// <param name="date">The date and time in server time zone.</param>
        /// <returns>
        /// A date and time in client time zone.
        /// </returns>
        public static DateTime ToClientTimeZone(DateTime date)
        {
            if(date.Kind == DateTimeKind.Local)
                throw new ArgumentOutOfRangeException("date", date.Kind, @"Argument Out Of Range Exception");
            if(date.Kind == DateTimeKind.Utc)
                return TimeZoneInfo.ConvertTimeFromUtc(date, ClientTimeZone);
            return TimeZoneInfo.ConvertTime(date, Config.ServerTimeZone, ClientTimeZone);
        }

        /// <summary>
        /// Converts an invariant culture string representation of a date and time in server time zone to a <see cref="System.DateTime"/> equivalent in client time zone.
        /// </summary>
        /// <param name="date">The date and time in invariant culture and server time zone.</param>
        /// <returns>
        /// A date and time in client time zone.
        /// </returns>
        public static DateTime ToClientTimeZone(string date)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(date));

            return ToClientTimeZone(DateTime.Parse(date, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal));
        }

        /// <summary>
        /// Converts a date and time in client time zone to a HTML &lt;TIME&gt; tag equivalent.
        /// </summary>
        /// <remarks>
        /// The <see cref="System.DateTime.Kind"/> must not be <see cref="System.DateTimeKind.Local"/>, even if the local time zone is the same as client.
        /// </remarks>
        /// <param name="date">The date and time in client time zone.</param>
        /// <param name="time"><c>true</c> for time component, <c>false</c> for date only.</param>
        /// <param name="format">The standard or custom date and time format string.</param>
        /// <returns>
        /// A HTML &lt;TIME&gt; tag.
        /// </returns>
        public static string HtmlTimeTag(DateTime? date, bool time, string format)
        {
            if(date == null)
                return null;
            if(date.Value.Kind == DateTimeKind.Local)
                throw new ArgumentOutOfRangeException("date", date.Value.Kind, @"Argument Out Of Range Exception");
            if(time)
            {
                DateTime utc;
                if(date.Value.Kind == DateTimeKind.Utc)
                {
                    utc = date.Value;
                    date = ToClientTimeZone(date.Value);
                }
                else
                    utc = TimeZoneInfo.ConvertTimeToUtc(date.Value, ClientTimeZone);
                return string.Format(CultureInfo.CurrentCulture, "<time datetime='{0}'>{1}</time>", utc.ToString(Config.IsoDateTimeZone, CultureInfo.InvariantCulture), date.Value.ToString(format, CultureInfo.CurrentCulture));
            }
            return string.Format(CultureInfo.CurrentCulture, "<time datetime='{0}'>{1}</time>", date.Value.ToString(Config.IsoDate, CultureInfo.InvariantCulture), date.Value.Date.ToString(format, CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Converts a date in client time zone to a HTML &lt;TIME&gt; tag equivalent.
        /// </summary>
        /// <param name="date">The date in client time zone.</param>
        /// <param name="format">The standard or custom date format string.</param>
        /// <returns>
        /// A HTML &lt;TIME&gt; tag.
        /// </returns>
        public static string HtmlTimeTag(Date? date, string format)
        {
            if(date != null)
                return HtmlTimeTag(date.Value.DateTime, false, format);
            return null;
        }

        /// <summary>
        /// Converts a date and time from client to server time zone.
        /// </summary>
        /// <remarks>
        /// The <see cref="System.DateTime.Kind"/> must not be <see cref="System.DateTimeKind.Local"/>, even if the local time zone is the same as client.
        /// </remarks>
        /// <param name="date">The date and time in server time zone.</param>
        /// <returns>
        /// A date and time in server time zone.
        /// </returns>
        public static DateTime ToServerTimeZone(DateTime date)
        {
            if(date.Kind == DateTimeKind.Local)
                throw new ArgumentOutOfRangeException("date", date.Kind, @"Argument Out Of Range Exception");
            if(date.Kind == DateTimeKind.Utc)
                return TimeZoneInfo.ConvertTimeFromUtc(date, Config.ServerTimeZone);
            return TimeZoneInfo.ConvertTime(date, ClientTimeZone, Config.ServerTimeZone);
        }

        /// <summary>
        /// Converts a current culture string representation of a date and time in client time zone to a <see cref="System.DateTime"/> equivalent in servers time zone.
        /// </summary>
        /// <param name="date">The date and time in current culture and client time zone.</param>
        /// <returns>
        /// A date and time in servers time zone.
        /// </returns>
        public static DateTime ToServerTimeZone(string date)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(date));

            return ToServerTimeZone(DateTime.Parse(date, CultureInfo.CurrentCulture, DateTimeStyles.AdjustToUniversal));
        }

        /// <summary>
        /// Converts a date and time in servers time zone to its equivalent string representation in server culture.
        /// </summary>
        /// <param name="date">The date and time in servers time zone.</param>
        /// <param name="time"><c>true</c> for time component, <c>false</c> for date only.</param>
        /// <param name="ao"><c>true</c> for OLE Automation, <c>false</c> for string literal date and time.</param>
        /// <returns>
        /// An OLE Automation or a string literal (<paramref name="ao"/>) date and time in server culture.
        /// </returns>
        public static string ServerDateLiteral(DateTime date, bool time, bool ao)
        {
            if(ao)
                return (time ? date : date.Date).ToOADate().ToString(Config.ServerCulture);
            return date.ToString(time ? "G" : "d", Config.ServerCulture);
        }

        /// <summary>
        /// Converts a date in server time zone to its equivalent string representation in server culture.
        /// </summary>
        /// <param name="date">The date in server time zone.</param>
        /// <param name="ao"><c>true</c> for OLE Automation, <c>false</c> for string literal date.</param>
        /// <returns>
        /// An OLE Automation or a string literal (<paramref name="ao"/>) date in server culture.
        /// </returns>
        public static string ServerDateLiteral(Date date, bool ao)
        {
            return ServerDateLiteral(date.DateTime, false, ao);
        }

        /// <summary>
        /// Localizes an enumeration name from resources.
        /// </summary>
        /// <remarks>
        /// Supports flags attribute.
        /// </remarks>
        /// <param name="name">The hard-coded enumeration name.</param>
        /// <returns>
        /// A localized enumeration name.
        /// </returns>
        public static string EnumGetName(string name)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(name));

            var sb = new StringBuilder();
            foreach(string s in name.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries))
                sb.AppendFormat(CultureInfo.CurrentCulture, "{0}, ", (string)HttpContext.GetLocalResourceObject("~/Enums", s, CultureInfo.CurrentCulture));
            if(sb.Length != 0)
                return sb.ToString(0, sb.Length - 2);
            return null;
        }
    } 
}
