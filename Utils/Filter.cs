﻿namespace Utils
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Text;
    using System.Web;
    using System.Web.UI.WebControls;

    using Utils.Types;

    /// <summary>
    /// Constants that represent string conditions.
    /// </summary>
    public enum StringCondition
    {
        /// <summary>
        /// null
        /// </summary>
        Null = 0,

        /// <summary>
        /// NOT null
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Null_Not = 1,

        /// <summary>
        /// sort order precedes X
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Compare_LT = 2,

        /// <summary>
        /// sort order precedes OR same as X
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Compare_LE = 3,

        /// <summary>
        /// sort order follows X
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Compare_GT = 4,

        /// <summary>
        /// sort order follows OR same as X
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Compare_GE = 5,

        /// <summary>
        /// equal to X
        /// </summary>
        Equals = 6,

        /// <summary>
        /// NOT equal to X
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Equals_Not = 7,

        /// <summary>
        /// starts with X
        /// </summary>
        Starts = 8,

        /// <summary>
        /// NOT starts with X
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Starts_Not = 9,

        /// <summary>
        /// ends with X
        /// </summary>
        Ends = 10,

        /// <summary>
        /// NOT ends with X
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Ends_Not = 11,

        /// <summary>
        /// contains X
        /// </summary>
        Contains = 12,

        /// <summary>
        /// NOT contains X
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Contains_Not = 13
    }

    /// <summary>
    /// Constants that represent numeric conditions.
    /// </summary>
    public enum NumericCondition
    {
        /// <summary>
        /// null
        /// </summary>
        Null = 0,

        /// <summary>
        /// NOT null
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        Null_Not = 1,

        /// <summary>
        /// lesser than MAX
        /// </summary>
        LT = 2,

        /// <summary>
        /// lesser than OR equal to MAX
        /// </summary>
        LE = 3,

        /// <summary>
        /// greater than MIN
        /// </summary>
        GT = 4,

        /// <summary>
        /// greater than OR equal to MIN
        /// </summary>
        GE = 5,

        /// <summary>
        /// (greater than MIN) AND (lesser than MAX)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        GT_And_LT = 6,

        /// <summary>
        /// (greater than MIN) AND (lesser than OR equal to MAX)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        GT_And_LE = 7,

        /// <summary>
        /// (greater than OR equal to MIN) AND (lesser than MAX)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        GE_And_LT = 8,

        /// <summary>
        /// (greater than OR equal to MIN) AND (lesser than OR equal to MAX)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        GE_And_LE = 9,

        /// <summary>
        /// (lesser than MIN) OR (greater than MAX)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        LT_Or_GT = 10,

        /// <summary>
        /// (lesser than MIN) OR (greater than OR equal to MAX)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        LT_Or_GE = 11,

        /// <summary>
        /// (lesser than OR equal to MIN) OR (greater than MAX)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        LE_Or_GT = 12,

        /// <summary>
        /// (lesser than OR equal to MIN) OR (greater than OR equal to MAX)
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
        LE_Or_GE = 13
    }

    /// <summary>
    /// Constants that represent numeric types.
    /// </summary>
    public enum NumericType
    {
        /// <summary>
        /// byte, short, int, long, BigInteger
        /// </summary>
        Integral = 0,

        /// <summary>
        /// decimal floating point
        /// </summary>
        Decimal = 1,

        /// <summary>
        /// single or double precision binary floating point
        /// </summary>
        Float = 2,

        /// <summary>
        /// date only, without the time component
        /// </summary>
        Date = 3,

        /// <summary>
        /// DateTime, DateTimeOffset
        /// </summary>
        DateTime = 4
    }

    /// <summary>
    /// Database and <see cref="System.Data.DataColumn.Expression"/> filters.
    /// </summary>
    public static class Filter
    {
        /// <summary>
        /// Gets the hard-coded <see cref="StringCondition"/> names, to avoid reflection.
        /// </summary>
        /// <param name="condition">The string condition.</param>
        /// <returns>
        /// The name of a string condition.
        /// </returns>
        public static string GetName(this StringCondition condition)
        {
            switch(condition)
            {
                case StringCondition.Null:
                    return "Null";
                case StringCondition.Null_Not:
                    return "Null_Not";
                case StringCondition.Compare_LT:
                    return "Compare_LT";
                case StringCondition.Compare_LE:
                    return "Compare_LE";
                case StringCondition.Compare_GT:
                    return "Compare_GT";
                case StringCondition.Compare_GE:
                    return "Compare_GE";
                case StringCondition.Equals:
                    return "Equals";
                case StringCondition.Equals_Not:
                    return "Equals_Not";
                case StringCondition.Starts:
                    return "Starts";
                case StringCondition.Starts_Not:
                    return "Starts_Not";
                case StringCondition.Ends:
                    return "Ends";
                case StringCondition.Ends_Not:
                    return "Ends_Not";
                case StringCondition.Contains:
                    return "Contains";
                case StringCondition.Contains_Not:
                    return "Contains_Not";
                default:
                    throw new InvalidEnumArgumentException("condition", (int)condition, typeof(StringCondition));
            }
        }

        /// <summary>
        /// Gets the hard-coded <see cref="NumericCondition"/> names, to avoid reflection.
        /// </summary>
        /// <param name="condition">The numeric condition.</param>
        /// <returns>
        /// The name of a numeric condition.
        /// </returns>
        public static string GetName(this NumericCondition condition)
        {
            switch(condition)
            {
                case NumericCondition.Null:
                    return "Null";
                case NumericCondition.Null_Not:
                    return "Null_Not";
                case NumericCondition.LT:
                    return "LT";
                case NumericCondition.LE:
                    return "LE";
                case NumericCondition.GT:
                    return "GT";
                case NumericCondition.GE:
                    return "GE";
                case NumericCondition.GT_And_LT:
                    return "GT_And_LT";
                case NumericCondition.GT_And_LE:
                    return "GT_And_LE";
                case NumericCondition.GE_And_LT:
                    return "GE_And_LT";
                case NumericCondition.GE_And_LE:
                    return "GE_And_LE";
                case NumericCondition.LT_Or_GT:
                    return "LT_Or_GT";
                case NumericCondition.LT_Or_GE:
                    return "LT_Or_GE";
                case NumericCondition.LE_Or_GT:
                    return "LE_Or_GT";
                case NumericCondition.LE_Or_GE:
                    return "LE_Or_GE";
                default:
                    throw new InvalidEnumArgumentException("condition", (int)condition, typeof(NumericCondition));
            }
        }

        /// <summary>
        /// Localizes a filter condition description from resources.
        /// </summary>
        /// <param name="name">The hard-coded filter condition name.</param>
        /// <returns>
        /// A localized filter condition description.
        /// </returns>
        public static string FilterCondition(string name)
        {
            return (string)HttpContext.GetLocalResourceObject("~/Filter", name, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Gets the localized negation condition description from resources.
        /// </summary>
        public static string NegationCondition
        {
            get
            {
                return FilterCondition("negation");
            }
        }

        /// <summary>
        /// Gets the localized nullable condition description from resources.
        /// </summary>
        public static string NullableCondition
        {
            get
            {
                return FilterCondition("nullable");
            }
        }

        /// <summary>
        /// Populates string filters list.
        /// </summary>
        /// <param name="select">The list to populate.</param>
        public static void StringConditions(ListItemCollection select)
        {
            var li = new ListItem(null, null);
            li.Attributes["label"] = " ";
            select.Add(li);
            // ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
            foreach(StringCondition value in Helper.EnumGetValues(typeof(StringCondition)))
            {
                li = new ListItem(G11n.EnumGetName(value.GetName()), ((int)value).ToString(CultureInfo.CurrentCulture));
                li.Attributes["title"] = FilterCondition(value.GetName());
                select.Add(li);
            }
        }

        /// <summary>
        /// Populates numeric filters list.
        /// </summary>
        /// <param name="select">The list to populate.</param>
        public static void NumericConditions(ListItemCollection select)
        {
            var li = new ListItem(null, null);
            li.Attributes["label"] = " ";
            select.Add(li);
            // ReSharper disable once PossibleInvalidCastExceptionInForeachLoop
            foreach(NumericCondition value in Helper.EnumGetValues(typeof(NumericCondition)))
            {
                li = new ListItem(G11n.EnumGetName(value.GetName()), ((int)value).ToString(CultureInfo.CurrentCulture));
                li.Attributes["title"] = FilterCondition(value.GetName());
                select.Add(li);
            }
        }

        /// <summary>
        /// Escapes all special characters ('\'', '[', ']', '%', '*'), and optionally surrounds the string with single quotes - to prevent SQL injection attacks.
        /// </summary>
        /// <param name="text">The string to escape.</param>
        /// <param name="quotes">(Optional) <c>true</c> for quotes, otherwise <c>false</c>.</param>
        /// <returns>
        /// An escaped string.
        /// </returns>
        public static string EscapeString(string text, bool quotes = true)
        {
            Contract.Requires(text != null);

            var sb = new StringBuilder();
            if(quotes)
                sb.Append('\'');
            foreach(char c in text)
            {
                switch(c)
                {
                    case '[':
                    case ']':
                    case '%':
                    case '*':
                    {
                        sb.AppendFormat(CultureInfo.InvariantCulture, "[{0}]", c);
                        break;
                    }
                    case '\'':
                    {
                        sb.Append("''");
                        break;
                    }
                    default:
                    {
                        sb.Append(c);
                        break;
                    }
                }
            }
            if(quotes)
                sb.Append('\'');
            return sb.ToString();
        }

        private static string StringArray(string format, string column, string value, bool nullable)
        {
            Contract.Requires(value != null);

            string[] vals = EscapeString(value, false).Split(';');
            switch(vals.Length)
            {
                case 0:
                    return null;
                case 1:
                    return string.Format(CultureInfo.InvariantCulture, nullable ? string.Format(CultureInfo.InvariantCulture, "({{0}} IS NULL OR {0}) AND ", format) : format + " AND ", column, vals[0]);
                default:
                {
                    var sb = new StringBuilder();
                    if(format.Contains(" IN "))
                    {
                        foreach(string s in vals)
                            sb.AppendFormat(CultureInfo.InvariantCulture, "'{0}', ", s);
                        if(sb.Length > 0)
                            return string.Format(CultureInfo.InvariantCulture, nullable ? string.Format(CultureInfo.InvariantCulture, "({{0}} IS NULL OR {0}) AND ", format) : format + " AND ", column, sb.ToString(0, sb.Length - 2));
                    }
                    else
                    {
                        bool negation = format.Contains(" NOT ");
                        foreach(string s in vals)
                        {
                            sb.AppendFormat(CultureInfo.InvariantCulture, format, column, s);
                            sb.Append(negation ? " AND " : " OR ");
                        }
                        if(sb.Length > 0)
                        {
                            if(nullable)
                                return string.Format(CultureInfo.InvariantCulture, negation ? "({0} IS NULL OR ({1})) AND " : "({0} IS NULL OR {1}) AND ", column, sb.ToString(0, sb.Length - (negation ? 5 : 4)));
                            if(!negation)
                                return string.Format(CultureInfo.InvariantCulture, "({0}) AND ", sb.ToString(0, sb.Length - 4));
                            return sb.ToString();
                        }
                    }
                    return null;
                }
            }
        }

        /// <summary>
        /// Generates string filters.
        /// </summary>
        /// <remarks>
        /// Supports semicolon separated values. Escapes all special characters ('\'', '[', ']', '%', '*'), and surrounds values with single quotes - to prevent SQL injection attacks.
        /// </remarks>
        /// <param name="column">The column to filter.</param>
        /// <param name="condition">The string condition.</param>
        /// <param name="value">The value to compare.</param>
        /// <param name="nullable"><c>true</c> to include, <c>false</c> to exclude <c>null</c> values.</param>
        /// <returns>
        /// An automated filter.
        /// </returns>
        public static string StringFilter(string column, string condition, string value, bool nullable)
        {
            int i;
            if(string.IsNullOrWhiteSpace(condition) || !int.TryParse(condition, NumberStyles.None, CultureInfo.CurrentCulture, out i))
                return null;
            switch((StringCondition)i)
            {
                case StringCondition.Null:
                    return string.Format(CultureInfo.InvariantCulture, "{0} IS NULL AND ", column);
                case StringCondition.Null_Not:
                    return string.Format(CultureInfo.InvariantCulture, "{0} IS NOT NULL AND ", column);
                case StringCondition.Compare_LT:
                    return StringArray("{0} < '{1}'", column, value, nullable);
                case StringCondition.Compare_LE:
                    return StringArray("{0} <= '{1}'", column, value, nullable);
                case StringCondition.Compare_GT:
                    return StringArray("{0} > '{1}'", column, value, nullable);
                case StringCondition.Compare_GE:
                    return StringArray("{0} >= '{1}'", column, value, nullable);
                case StringCondition.Equals:
                    return StringArray("{0} IN ({1})", column, value, nullable);
                case StringCondition.Equals_Not:
                    return StringArray("{0} NOT IN ({1})", column, value, nullable);
                case StringCondition.Starts:
                    return StringArray("{0} LIKE '{1}%'", column, value, nullable);
                case StringCondition.Starts_Not:
                    return StringArray("{0} NOT LIKE '{1}%'", column, value, nullable);
                case StringCondition.Ends:
                    return StringArray("{0} LIKE '%{1}'", column, value, nullable);
                case StringCondition.Ends_Not:
                    return StringArray("{0} NOT LIKE '%{1}'", column, value, nullable);
                case StringCondition.Contains:
                    return StringArray("{0} LIKE '%{1}%'", column, value, nullable);
                case StringCondition.Contains_Not:
                    return StringArray("{0} NOT LIKE '%{1}%'", column, value, nullable);
                default:
                    throw new InvalidEnumArgumentException("condition", i, typeof(StringCondition));
            }
        }

        /// <summary>
        /// Converts a date and time to a string literal.
        /// </summary>
        /// <param name="date">The date and time to convert.</param>
        /// <param name="time"><c>true</c> for time component, <c>false</c> for date only.</param>
        /// <param name="sql"><c>true</c> for SQL databases, <c>false</c> for Access or <see cref="System.Data.DataColumn.Expression"/>.</param>
        /// <returns>
        /// A date and time string literal.
        /// </returns>
        public static string DateLiteral(DateTime date, bool time, bool sql)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}{1}{0}", sql && Config.DatabaseType != DatabaseType.OleDb ? "'" : "#", date.ToString(time ? Config.IsoDateTime : Config.IsoDate, CultureInfo.InvariantCulture));
        }

        /// <summary>
        /// Converts a date to a string literal.
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <param name="sql"><c>true</c> for SQL databases, <c>false</c> for Access or <see cref="System.Data.DataColumn.Expression"/>.</param>
        /// <returns>
        /// A date string literal.
        /// </returns>
        public static string DateLiteral(Date date, bool sql)
        {
            return DateLiteral(date.DateTime, false, sql);
        }

        /// <summary>
        /// Generates numeric, date and time filters.
        /// </summary>
        /// <remarks>
        /// Supports numeric ranges matching (inclusive, exclusive, and inverted).
        /// </remarks>
        /// <param name="column">The column to filter.</param>
        /// <param name="condition">The numeric condition.</param>
        /// <param name="valueMin">The minimum range value, in current culture and client time zone.</param>
        /// <param name="valueMax">The maximum range value, in current culture and client time zone.</param>
        /// <param name="nullable"><c>true</c> to include, <c>false</c> to exclude <c>null</c> values.</param>
        /// <param name="sql"><c>true</c> for SQL databases, <c>false</c> for Access or <see cref="System.Data.DataColumn.Expression"/>.</param>
        /// <param name="type">The numeric type.</param>
        /// <returns>
        /// An automated filter.
        /// </returns>
        public static string NumericFilter(string column, string condition, string valueMin, string valueMax, bool nullable, bool sql, NumericType type)
        {
            int i;
            if(string.IsNullOrWhiteSpace(condition) || !int.TryParse(condition, NumberStyles.None, CultureInfo.CurrentCulture, out i))
                return null;
            var num = (NumericCondition)i;
            if(num == NumericCondition.Null)
                return string.Format(CultureInfo.InvariantCulture, "{0} IS NULL AND ", column);
            if(num == NumericCondition.Null_Not)
                return string.Format(CultureInfo.InvariantCulture, "{0} IS NOT NULL AND ", column);
            switch(type)
            {
                case NumericType.Integral:
                {
                    valueMin = !string.IsNullOrWhiteSpace(valueMin) ? long.Parse(valueMin, CultureInfo.CurrentCulture).ToString(CultureInfo.InvariantCulture) : null;
                    valueMax = !string.IsNullOrWhiteSpace(valueMax) ? long.Parse(valueMax, CultureInfo.CurrentCulture).ToString(CultureInfo.InvariantCulture) : null;
                    break;
                }
                case NumericType.Decimal:
                {
                    valueMin = !string.IsNullOrWhiteSpace(valueMin) ? decimal.Parse(valueMin, CultureInfo.CurrentCulture).ToString(CultureInfo.InvariantCulture) : null;
                    valueMax = !string.IsNullOrWhiteSpace(valueMax) ? decimal.Parse(valueMax, CultureInfo.CurrentCulture).ToString(CultureInfo.InvariantCulture) : null;
                    break;
                }
                case NumericType.Float:
                {
                    valueMin = !string.IsNullOrWhiteSpace(valueMin) ? double.Parse(valueMin, CultureInfo.CurrentCulture).ToString(CultureInfo.InvariantCulture) : null;
                    valueMax = !string.IsNullOrWhiteSpace(valueMax) ? double.Parse(valueMax, CultureInfo.CurrentCulture).ToString(CultureInfo.InvariantCulture) : null;
                    break;
                }
                case NumericType.Date:
                {
                    valueMin = !string.IsNullOrWhiteSpace(valueMin) ? DateLiteral(Date.Parse(valueMin, CultureInfo.CurrentCulture), sql) : null;
                    valueMax = !string.IsNullOrWhiteSpace(valueMax) ? DateLiteral(Date.Parse(valueMax, CultureInfo.CurrentCulture), sql) : null;
                    break;
                }
                case NumericType.DateTime:
                {
                    valueMin = !string.IsNullOrWhiteSpace(valueMin) ? DateLiteral(G11n.ToServerTimeZone(valueMin), true, sql) : null;
                    valueMax = !string.IsNullOrWhiteSpace(valueMax) ? DateLiteral(G11n.ToServerTimeZone(valueMax), true, sql) : null;
                    break;
                }
                default:
                    throw new InvalidEnumArgumentException("type", (int)type, typeof(NumericType));
            }
            switch(num)
            {
                case NumericCondition.LE:
                {
                    if(valueMax != null)
                        return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR {0} < {1}) AND " : "{0} < {1} AND ", column, valueMax);
                    return null;
                }
                case NumericCondition.LT:
                {
                    if(valueMax != null)
                        return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR {0} <= {1}) AND " : "{0} <= {1} AND ", column, valueMax);
                    return null;
                }
                case NumericCondition.GT:
                {
                    if(valueMin != null)
                        return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR {0} > {1}) AND " : "{0} > {1} AND ", column, valueMin);
                    return null;
                }
                case NumericCondition.GE:
                {
                    if(valueMin != null)
                        return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR {0} >= {1}) AND " : "{0} >= {1} AND ", column, valueMin);
                    return null;
                }
                case NumericCondition.GT_And_LT:
                {
                    if(valueMin == null)
                        goto case NumericCondition.LT;
                    if(valueMax == null)
                        goto case NumericCondition.GT;
                    return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR ({0} > {1} AND {0} < {2})) AND " : "{0} > {1} AND {0} < {2} AND ", column, valueMin, valueMax);
                }
                case NumericCondition.GT_And_LE:
                {
                    if(valueMin == null)
                        goto case NumericCondition.LE;
                    if(valueMax == null)
                        goto case NumericCondition.GT;
                    return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR ({0} > {1} AND {0} <= {2})) AND " : "{0} > {1} AND {0} <= {2} AND ", column, valueMin, valueMax);
                }
                case NumericCondition.GE_And_LT:
                {
                    if(valueMin == null)
                        goto case NumericCondition.LT;
                    if(valueMax == null)
                        goto case NumericCondition.GE;
                    return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR ({0} >= {1} AND {0} < {2})) AND " : "{0} >= {1} AND {0} < {2} AND ", column, valueMin, valueMax);
                }
                case NumericCondition.GE_And_LE:
                {
                    if(valueMin == null)
                        goto case NumericCondition.LE;
                    if(valueMax == null)
                        goto case NumericCondition.GE;
                    return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR ({0} >= {1} AND {0} <= {2})) AND " : "{0} >= {1} AND {0} <= {2} AND ", column, valueMin, valueMax);
                }
                case NumericCondition.LT_Or_GT:
                {
                    if(valueMin == null)
                    {
                        valueMin = valueMax;
                        goto case NumericCondition.GT;
                    }
                    if(valueMax == null)
                    {
                        valueMax = valueMin;
                        goto case NumericCondition.LT;
                    }
                    return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR {0} < {1} OR {0} > {2}) AND " : "({0} < {1} OR {0} > {2}) AND ", column, valueMin, valueMax);
                }
                case NumericCondition.LT_Or_GE:
                {
                    if(valueMin == null)
                    {
                        valueMin = valueMax;
                        goto case NumericCondition.GE;
                    }
                    if(valueMax == null)
                    {
                        valueMax = valueMin;
                        goto case NumericCondition.LT;
                    }
                    return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR {0} < {1} OR {0} >= {2}) AND " : "({0} < {1} OR {0} >= {2}) AND ", column, valueMin, valueMax);
                }
                case NumericCondition.LE_Or_GT:
                {
                    if(valueMin == null)
                    {
                        valueMin = valueMax;
                        goto case NumericCondition.GT;
                    }
                    if(valueMax == null)
                    {
                        valueMax = valueMin;
                        goto case NumericCondition.LE;
                    }
                    return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR {0} <= {1} OR {0} > {2}) AND " : "({0} <= {1} OR {0} > {2}) AND ", column, valueMin, valueMax);
                }
                case NumericCondition.LE_Or_GE:
                {
                    if(valueMin == null)
                    {
                        valueMin = valueMax;
                        goto case NumericCondition.GE;
                    }
                    if(valueMax == null)
                    {
                        valueMax = valueMin;
                        goto case NumericCondition.LE;
                    }
                    return string.Format(CultureInfo.InvariantCulture, nullable ? "({0} IS NULL OR {0} <= {1} OR {0} >= {2}) AND " : "({0} <= {1} OR {0} >= {2}) AND ", column, valueMin, valueMax);
                }
                default:
                    throw new InvalidEnumArgumentException("condition", i, typeof(NumericCondition));
            }
        }

        /// <summary>
        /// Generates numeric filters for multiple values that are not in a range (foreign keys).
        /// </summary>
        /// <remarks>
        /// Supports not normalized one-to-many and many-to-many relationships, where multiple foreign keys are in a single column value, separated by a string.
        /// </remarks>
        /// <param name="column">The column to filter.</param>
        /// <param name="negation"><c>true</c> for "NOT equal to", <c>false</c> for "equal to" filter condition.</param>
        /// <param name="select">The list of values in current culture, to use if selected.</param>
        /// <param name="separator">(Optional) the values separator.</param>
        /// <returns>
        /// An automated filter.
        /// </returns>
        public static string SelectMultiple(string column, bool negation, ListItemCollection select, string separator = null)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(column));
            Contract.Requires(select != null);

            var sb = new StringBuilder();
            foreach(ListItem li in select)
            {
                if(!li.Selected)
                    continue;
                if(separator != null)
                    sb.AppendFormat(CultureInfo.InvariantCulture, "{0} {1} '{2}{3}%' {4} {0} {1} '%{3}{2}' {4} {0} {1} '%{3}{2}{3}%' {4} ", column, negation ? "NOT LIKE" : "LIKE", int.Parse(li.Value, CultureInfo.CurrentCulture), separator, negation ? "AND" : "OR");
                else
                    sb.AppendFormat(CultureInfo.InvariantCulture, "{0} {1} {2} {3} ", column, negation ? "<>" : "=", int.Parse(li.Value, CultureInfo.CurrentCulture), negation ? "AND" : "OR");
            }
            if(sb.Length > 0)
            {
                if(separator != null)
                    return string.Format(CultureInfo.InvariantCulture, "({0} {1} NULL {2} ({3})) AND ", column, negation ? "IS" : "IS NOT", negation ? "OR" : "AND", sb.ToString(0, sb.Length - (negation ? 5 : 4)));
                if(!negation)
                    return string.Format(CultureInfo.InvariantCulture, "({0}) AND ", sb.ToString(0, sb.Length - 4));
                return sb.ToString();
            }
            return null;
        }
    } 
}
