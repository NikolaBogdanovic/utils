﻿namespace Utils
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.IO.Compression;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web.Configuration;

    /// <summary>
    /// Constants that represent hashing algorithms.
    /// </summary>
    public enum HashAlgorithm
    {
        /// <summary>
        /// Message Digest 5 (128-bit)
        /// </summary>
        MD5 = 128,

        /// <summary>
        /// Secure Hash Algorithm 1 (160-bit)
        /// </summary>
        SHA1 = 160,

        /// <summary>
        /// Secure Hash Algorithm 2 (256-bit)
        /// </summary>
        SHA256 = 256,

        /// <summary>
        /// Secure Hash Algorithm 2 (384-bit)
        /// </summary>
        SHA384 = 384,

        /// <summary>
        /// Secure Hash Algorithm 2 (512-bit)
        /// </summary>
        SHA512 = 512
    }

    /// <summary>
    /// Cryptographic services.
    /// </summary>
    public static class Crypto
    {
        /// <summary>
        /// Gets the hard-coded <see cref="HashAlgorithm"/> names, to avoid reflection.
        /// </summary>
        /// <param name="hash">The hashing algorithm.</param>
        /// <returns>
        /// The name of a hashing algorithm.
        /// </returns>
        public static string GetName(this HashAlgorithm hash)
        {
            switch(hash)
            {
                case HashAlgorithm.MD5:
                    return "MD5";
                case HashAlgorithm.SHA1:
                    return "SHA1";
                case HashAlgorithm.SHA256:
                    return "SHA256";
                case HashAlgorithm.SHA384:
                    return "SHA384";
                case HashAlgorithm.SHA512:
                    return "SHA512";
                default:
                    throw new InvalidEnumArgumentException("hash", (int)hash, typeof(HashAlgorithm));
            }
        }

        /// <summary>
        /// The key that is used to encrypt and decrypt forms authentication and view state data.
        /// </summary>
        public static readonly string DecryptionKey = ((MachineKeySection)WebConfigurationManager.GetSection("system.web/machineKey")).DecryptionKey;
        
        /// <summary>
        /// The key that is used to hash and validate forms authentication and view state data.
        /// </summary>
        public static readonly string ValidationKey = ((MachineKeySection)WebConfigurationManager.GetSection("system.web/machineKey")).ValidationKey;

        /// <summary>
        /// The algorithm that is used for hashing and validating forms authentication and view state data.
        /// </summary>
        public static readonly HashAlgorithm ValidationHash = InitializeValidationHash();
        private static HashAlgorithm InitializeValidationHash()
        {
            switch(((MachineKeySection)WebConfigurationManager.GetSection("system.web/machineKey")).Validation)
            {
                case MachineKeyValidation.MD5:
                    return HashAlgorithm.MD5;
                case MachineKeyValidation.SHA1:
                    return HashAlgorithm.SHA1;
                case MachineKeyValidation.HMACSHA256:
                    return HashAlgorithm.SHA256;
                case MachineKeyValidation.HMACSHA384:
                    return HashAlgorithm.SHA384;
                case MachineKeyValidation.HMACSHA512:
                    return HashAlgorithm.SHA512;
                default:
                    goto case MachineKeyValidation.HMACSHA256;
            }
        }

        /// <summary>
        /// Hashes a data.
        /// </summary>
        /// <param name="alg">The hashing algorithm.</param>
        /// <param name="data">The data to be hashed.</param>
        /// <param name="salt">(Optional) the salt.</param>
        /// <param name="key">(Optional) the HMAC key.</param>
        /// <returns>
        /// A hash of the data.
        /// </returns>
        public static byte[] Hash(HashAlgorithm alg, byte[] data, byte[] salt = null, byte[] key = null)
        {
            if(data != null)
            {
                if(salt != null && salt.Length > 0)
                {
                    var temp = new byte[salt.Length + data.Length];
                    salt.CopyTo(temp, 0);
                    data.CopyTo(temp, salt.Length);
                    data = temp;
                }
                switch(alg)
                {
                    case HashAlgorithm.MD5:
                    {
                        if(key != null)
                        {
                            using(var md5 = new HMACMD5(key))
                                return md5.ComputeHash(data);
                        }
                        using(var md5 = new MD5CryptoServiceProvider())
                            return md5.ComputeHash(data);
                    }
                    case HashAlgorithm.SHA1:
                    {
                        if(key != null)
                        {
                            using(var sha1 = new HMACSHA1(key))
                                return sha1.ComputeHash(data);
                        }
                        using(var sha1 = new SHA1CryptoServiceProvider())
                            return sha1.ComputeHash(data);
                    }
                    case HashAlgorithm.SHA256:
                    {
                        if(key != null)
                        {
                            using(var sha2 = new HMACSHA256(key))
                                return sha2.ComputeHash(data);
                        }
                        using(var sha2 = new SHA256CryptoServiceProvider())
                            return sha2.ComputeHash(data);
                    }
                    case HashAlgorithm.SHA384:
                    {
                        if(key != null)
                        {
                            using(var sha3 = new HMACSHA384(key))
                                return sha3.ComputeHash(data);
                        }
                        using(var sha3 = new SHA384CryptoServiceProvider())
                            return sha3.ComputeHash(data);
                    }
                    case HashAlgorithm.SHA512:
                    {
                        if(key != null)
                        {
                            using(var sha5 = new HMACSHA512(key))
                                return sha5.ComputeHash(data);
                        }
                        using(var sha5 = new SHA512CryptoServiceProvider())
                            return sha5.ComputeHash(data);
                    }
                    default:
                        throw new InvalidEnumArgumentException("alg", (int)alg, typeof(HashAlgorithm));
                }
            }
            return null;
        }

        /// <summary>
        /// Hashes a text.
        /// </summary>
        /// <param name="alg">The hashing algorithm.</param>
        /// <param name="text">The UTF8 text to be hashed.</param>
        /// <param name="salt">(Optional) the UTF8 salt.</param>
        /// <param name="key">(Optional) the Base16 HMAC key.</param>
        /// <param name="hex">(Optional) <c>true</c> for Base16, or <c>false</c> for Base64 hash encoding.</param>
        /// <returns>
        /// A Base16/Base64 (<paramref name="hex"/>) hash of the text.
        /// </returns>
        public static string Hash(HashAlgorithm alg, string text, string salt = null, string key = null, bool hex = true)
        {
            if(text != null)
            {
                byte[] data = Hash(alg, Encoding.UTF8.GetBytes(text), !string.IsNullOrWhiteSpace(salt) ? Encoding.UTF8.GetBytes(salt) : null, key != null ? Helper.ConvertFromBase16String(key) : null);
                if(hex)
                    return Helper.ConvertToBase16String(data);
                return Convert.ToBase64String(data);
            }
            return null;
        }

        /// <summary>
        /// Validates two hashes.
        /// </summary>
        /// <param name="hash1">The Base16/Base64 (<paramref name="hex"/>) first hash.</param>
        /// <param name="hash2">The Base16/Base64 (<paramref name="hex"/>) second hash.</param>
        /// <param name="hex">(Optional) <c>true</c> for Base16, or <c>false</c> for Base64 hash encoding.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        public static bool Validate(string hash1, string hash2, bool hex = true)
        {
            if(string.IsNullOrWhiteSpace(hash1) && string.IsNullOrWhiteSpace(hash2))
                return true;
            if(!string.IsNullOrWhiteSpace(hash1) && !string.IsNullOrWhiteSpace(hash2))
            {
                if(hex)
                {
                    hash1 = hash1.Replace("-", string.Empty);
                    hash2 = hash2.Replace("-", string.Empty);
                }
                return string.Equals(hash1, hash2, StringComparison.OrdinalIgnoreCase);
            }
            return false;
        }

        /// <summary>
        /// Validates two hashes.
        /// </summary>
        /// <param name="hash1">The first hash.</param>
        /// <param name="hash2">The second hash.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        public static bool Validate(byte[] hash1, byte[] hash2)
        {
            if((hash1 == null || hash1.Length == 0) && (hash2 == null || hash2.Length == 0))
                return true;
            if(hash1 != null && hash1.Length > 0 && hash2 != null && hash2.Length > 0)
                return Validate(Convert.ToBase64String(hash1), Convert.ToBase64String(hash2), false);
            return false;
        }

        /// <summary>
        /// Validates a hash.
        /// </summary>
        /// <param name="alg">The hashing algorithm.</param>
        /// <param name="hash">The hash to validate.</param>
        /// <param name="data">The data to be hashed.</param>
        /// <param name="salt">(Optional) the salt.</param>
        /// <param name="key">(Optional) the HMAC key.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        public static bool Validate(HashAlgorithm alg, byte[] hash, byte[] data, byte[] salt = null, byte[] key = null)
        {
            if(data == null && (hash == null || hash.Length == 0))
                return true;
            if(data != null && hash != null && hash.Length > 0)
                return Validate(hash, Hash(alg, data, salt, key));
            return false;
        }

        /// <summary>
        /// Validates a hash.
        /// </summary>
        /// <param name="alg">The hashing algorithm.</param>
        /// <param name="hash">The Base16/Base64 (<paramref name="hex"/>) hash to validate.</param>
        /// <param name="text">The UTF8 text to be hashed.</param>
        /// <param name="salt">(Optional) the UTF8 salt.</param>
        /// <param name="key">(Optional) the Base16 HMAC key.</param>
        /// <param name="hex">(Optional) <c>true</c> for Base16, or <c>false</c> for Base64 hash encoding.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        public static bool Validate(HashAlgorithm alg, string hash, string text, string salt = null, string key = null, bool hex = true)
        {
            if(text == null && string.IsNullOrWhiteSpace(hash))
                return true;
            if(text != null && !string.IsNullOrWhiteSpace(hash))
                return Validate(hash, Hash(alg, text, salt, key, hex), hex);
            return false;
        }

        /// <summary>
        /// Encrypts a data with Advanced Encryption Standard (Rijndael).
        /// </summary>
        /// <param name="data">The data to encrypt.</param>
        /// <param name="key">The 128/256-bit symmetric key.</param>
        /// <param name="iv">(Optional) the initialization vector.</param>
        /// <returns>
        /// A cipher.
        /// </returns>
        public static byte[] AesEncrypt(byte[] data, byte[] key, byte[] iv = null)
        {
            Contract.Requires(key != null);

            if(data != null)
            {
                using(var aes = new AesCryptoServiceProvider())
                {
                    aes.Key = key;
                    if(iv != null)
                    {
                        if(iv.Length > 0)
                        {
                            Array.Resize(ref iv, aes.IV.Length);
                            aes.IV = iv;
                        }
                        else
                            iv = null;
                    }
                    using(var ms = new MemoryStream())
                    {
                        if(ms.CanTimeout)
                            ms.ReadTimeout = ms.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                        if(iv == null)
                            // ReSharper disable once PossibleNullReferenceException
                            // ReSharper disable once AssignNullToNotNullAttribute
                            ms.Write(aes.IV, 0, aes.IV.Length);
                        using(ICryptoTransform ct = aes.CreateEncryptor())
                        {
                            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
                            if(cs.CanTimeout)
                                cs.ReadTimeout = cs.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                            cs.Write(data, 0, data.Length);
                            cs.FlushFinalBlock();
                        }
                        return ms.ToArray();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Encrypts a text with Advanced Encryption Standard (Rijndael).
        /// </summary>
        /// <param name="text">The UTF8 text to encrypt.</param>
        /// <param name="key">The 128/256-bit Base16 symmetric key.</param>
        /// <param name="iv">(Optional) the Base64 initialization vector.</param>
        /// <returns>
        /// A Base64 cipher.
        /// </returns>
        public static string AesEncrypt(string text, string key, string iv = null)
        {
            Contract.Requires(key != null);

            if(text != null)
                return Convert.ToBase64String(AesEncrypt(Encoding.UTF8.GetBytes(text), Helper.ConvertFromBase16String(key), !string.IsNullOrWhiteSpace(iv) ? Convert.FromBase64String(iv) : null));
            return null;
        }

        /// <summary>
        /// Decrypts a cipher with Advanced Encryption Standard (Rijndael).
        /// </summary>
        /// <param name="cipher">The cipher to decrypt.</param>
        /// <param name="key">The 128/256-bit symmetric key.</param>
        /// <param name="iv">(Optional) the initialization vector.</param>
        /// <returns>
        /// A data.
        /// </returns>
        public static byte[] AesDecrypt(byte[] cipher, byte[] key, byte[] iv = null)
        {
            Contract.Requires(key != null);

            if(cipher != null && cipher.Length != 0)
            {
                using(var aes = new AesCryptoServiceProvider())
                {
                    aes.Key = key;
                    if(iv != null)
                    {
                        if(iv.Length > 0)
                        {
                            Array.Resize(ref iv, aes.IV.Length);
                            aes.IV = iv;
                        }
                        else
                            iv = null;
                    }
                    if(iv == null)
                    {
                        // ReSharper disable once PossibleNullReferenceException
                        iv = new byte[aes.IV.Length];
                        Array.Copy(cipher, iv, iv.Length);
                        aes.IV = iv;
                        iv = null;
                    }
                    using(var ms = new MemoryStream())
                    {
                        if(ms.CanTimeout)
                            ms.ReadTimeout = ms.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                        using(ICryptoTransform ct = aes.CreateDecryptor())
                        {
                            var cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
                            if(cs.CanTimeout)
                                cs.ReadTimeout = cs.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                            cs.Write(cipher, iv != null ? 0 : aes.IV.Length, iv != null ? cipher.Length : cipher.Length - aes.IV.Length);
                            cs.FlushFinalBlock();
                        }
                        return ms.ToArray();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Decrypts a cipher with Advanced Encryption Standard (Rijndael).
        /// </summary>
        /// <param name="cipher">The Base64 cipher to decrypt.</param>
        /// <param name="key">The 128/256-bit Base16 symmetric key.</param>
        /// <param name="iv">(Optional) the Base64 initialization vector.</param>
        /// <returns>
        /// An UTF8 text.
        /// </returns>
        public static string AesDecrypt(string cipher, string key, string iv = null)
        {
            Contract.Requires(key != null);

            if(!string.IsNullOrWhiteSpace(cipher))
                return Encoding.UTF8.GetString(AesDecrypt(Convert.FromBase64String(cipher), Helper.ConvertFromBase16String(key), !string.IsNullOrWhiteSpace(iv) ? Convert.FromBase64String(iv) : null));
            return null;
        }

        /// <summary>
        /// Protects a text by encrypting and signing it.
        /// </summary>
        /// <param name="text">The UTF8 text to protect.</param>
        /// <param name="purposes">(Optional) the variable-length list of purposes. If this value is specified, the same list must be passed to the <see cref="Unprotect(string, string[])"/> method.</param>
        /// <returns>
        /// A Base64 cipher.
        /// </returns>
        public static string Protect(string text, params string[] purposes)
        {
            Contract.Requires(purposes == null || (purposes.Length != 0 && !Array.Exists(purposes, string.IsNullOrWhiteSpace)));

            if(text != null)
            {
                string purpose = purposes != null ? string.Join(null, purposes) : string.Empty;
                byte[] cipher = AesEncrypt(Encoding.UTF8.GetBytes(text), Helper.ConvertFromBase16String(DecryptionKey), Encoding.UTF8.GetBytes(purpose));
                byte[] hash = Hash(ValidationHash, cipher, Encoding.UTF8.GetBytes(purpose), Helper.ConvertFromBase16String(ValidationKey));
                var data = new byte[cipher.Length + ((int)ValidationHash / 8)];
                cipher.CopyTo(data, 0);
                hash.CopyTo(data, cipher.Length);
                return Convert.ToBase64String(data);
            }
            return null;
        }

        /// <summary>
        /// Unprotects a cipher by decrypting and verifying it.
        /// </summary>
        /// <param name="cipher">The Base64 cipher to unprotect.</param>
        /// <param name="purposes">(Optional) the variable-length list of purposes. If this value is specified, must be the same list passed to the <see cref="Protect(string, string[])"/> method.</param>
        /// <returns>
        /// An UTF8 text.
        /// </returns>
        public static string Unprotect(string cipher, params string[] purposes)
        {
            Contract.Requires(purposes == null || (purposes.Length != 0 && !Array.Exists(purposes, string.IsNullOrWhiteSpace)));

            if(!string.IsNullOrWhiteSpace(cipher))
            {
                byte[] data = Convert.FromBase64String(cipher);
                var hash = new byte[(int)ValidationHash / 8];
                if(data.Length > hash.Length)
                {
                    Array.Copy(data, data.Length - hash.Length, hash, 0, hash.Length);
                    Array.Resize(ref data, data.Length - hash.Length);
                    string purpose = purposes != null ? string.Join(null, purposes) : string.Empty;
                    if(Validate(ValidationHash, hash, data, Encoding.UTF8.GetBytes(purpose), Helper.ConvertFromBase16String(ValidationKey)))
                        return Encoding.UTF8.GetString(AesDecrypt(data, Helper.ConvertFromBase16String(DecryptionKey), Encoding.UTF8.GetBytes(purpose)));
                }
            }
            return null;
        }

        /// <summary>
        /// Generates a private RSA key blob.
        /// </summary>
        /// <param name="bits">The key size in bits.</param>
        /// <returns>
        /// A private RSA key blob.
        /// </returns>
        public static byte[] RsaPrivateBlob(int bits)
        {
            Contract.Requires(bits >= 384 && bits <= 16384 && bits % 8 == 0);

            using(var rsa = new RSACryptoServiceProvider(bits))
                return rsa.ExportCspBlob(true);
        }

        /// <summary>
        /// Generates a private RSA key blob.
        /// </summary>
        /// <param name="bits">The key size in bits.</param>
        /// <param name="utf8"><c>true</c> for UTF8, or <c>false</c> for Base64 blob encoding.</param>
        /// <returns>
        /// An UTF8/Base64 (<paramref name="utf8"/>) private RSA key blob.
        /// </returns>
        public static string RsaPrivateBlob(int bits, bool utf8)
        {
            Contract.Requires(bits >= 384 && bits <= 16384 && bits % 8 == 0);

            byte[] key = RsaPrivateBlob(bits);
            if(utf8)
                return Encoding.UTF8.GetString(key);
            return Convert.ToBase64String(key);
        }

        /// <summary>
        /// Generates a private RSA key xml.
        /// </summary>
        /// <param name="bits">The key size in bits.</param>
        /// <returns>
        /// An UTF8 private RSA key xml.
        /// </returns>
        public static string RsaPrivateXml(int bits)
        {
            Contract.Requires(bits >= 384 && bits <= 16384 && bits % 8 == 0);

            using(var rsa = new RSACryptoServiceProvider(bits))
                return rsa.ToXmlString(true);
        }

        /// <summary>
        /// Generates a public RSA key blob.
        /// </summary>
        /// <param name="blob">The private RSA key blob.</param>
        /// <returns>
        /// A public RSA key blob.
        /// </returns>
        public static byte[] RsaPublicBlob(byte[] blob)
        {
            Contract.Requires(blob != null);

            using(var rsa = new RSACryptoServiceProvider())
            {
                rsa.ImportCspBlob(blob);
                return rsa.ExportCspBlob(false);
            }
        }

        /// <summary>
        /// Generates a public RSA key blob.
        /// </summary>
        /// <param name="blob">The UTF8/Base64 (<paramref name="utf8"/>) private RSA key blob.</param>
        /// <param name="utf8"><c>true</c> for UTF8, or <c>false</c> for Base64 blob encoding.</param>
        /// <returns>
        /// An UTF8/Base64 (<paramref name="utf8"/>) public RSA key blob.
        /// </returns>
        public static string RsaPublicBlob(string blob, bool utf8)
        {
            Contract.Requires(blob != null);

            byte[] key = RsaPublicBlob(utf8 ? Encoding.UTF8.GetBytes(blob) : Convert.FromBase64String(blob));
            if(utf8)
                return Encoding.UTF8.GetString(key);
            return Convert.ToBase64String(key);
        }

        /// <summary>
        /// Generates a public RSA key xml.
        /// </summary>
        /// <param name="xml">The private RSA key xml.</param>
        /// <returns>
        /// An UTF8 public RSA key xml.
        /// </returns>
        public static string RsaPublicXml(string xml)
        {
            Contract.Requires(xml != null);

            using(var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(xml);
                return rsa.ToXmlString(false);
            }
        }

        private static byte[] RsaEncrypt(RSACryptoServiceProvider rsa, byte[] data, bool oaep = true)
        {
            if(data != null)
            {
                byte[] cipher;
                int max = oaep ? (rsa.KeySize / 8) - 2 - (2 * 20) : (rsa.KeySize / 8) - 11;
                if(max < data.Length)
                {
                    using(var ms = new MemoryStream())
                    {
                        if(ms.CanTimeout)
                            ms.ReadTimeout = ms.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                        using(var temp = new MemoryStream(data))
                        {
                            if(temp.CanTimeout)
                                temp.ReadTimeout = temp.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                            int count;
                            do
                            {
                                var buffer = new byte[max];
                                count = temp.Read(buffer, 0, max);
                                if(count != max)
                                {
                                    temp.Seek(-count, SeekOrigin.Current);
                                    buffer = new byte[count];
                                    temp.Read(buffer, 0, count);
                                    count = 0;
                                }
                                cipher = rsa.Encrypt(buffer, oaep);
                                ms.Write(cipher, 0, cipher.Length);
                            }
                            while(count > 0);
                        }
                        cipher = ms.ToArray();
                    }
                }
                else
                    cipher = rsa.Encrypt(data, oaep);
                Array.Reverse(cipher);
                return cipher;
            }
            return null;
        }

        /// <summary>
        /// Encrypts a data with FIPS compliant RSA.
        /// </summary>
        /// <param name="data">The data to encrypt.</param>
        /// <param name="blob">The public RSA key blob.</param>
        /// <param name="oaep">(Optional) the optimal asymmetric encryption padding.</param>
        /// <returns>
        /// A cipher.
        /// </returns>
        public static byte[] RsaEncryptBlob(byte[] data, byte[] blob, bool oaep = true)
        {
            Contract.Requires(blob != null);

            if(data != null)
            {
                using(var rsa = new RSACryptoServiceProvider())
                {
                    rsa.ImportCspBlob(blob);
                    return RsaEncrypt(rsa, data, oaep);
                }
            }
            return null;
        }

        /// <summary>
        /// Encrypts a text with FIPS compliant RSA.
        /// </summary>
        /// <param name="text">The UTF8 text to encrypt.</param>
        /// <param name="blob">The UTF8/Base64 (<paramref name="utf8"/>) public RSA key blob.</param>
        /// <param name="utf8">(Optional) <c>true</c> for UTF8, or <c>false</c> for Base64 blob encoding.</param>
        /// <param name="oaep">(Optional) the optimal asymmetric encryption padding.</param>
        /// <returns>
        /// A Base64 cipher.
        /// </returns>
        public static string RsaEncryptBlob(string text, string blob, bool utf8 = false, bool oaep = true)
        {
            Contract.Requires(blob != null);

            if(text != null)
                return Convert.ToBase64String(RsaEncryptBlob(Encoding.UTF8.GetBytes(text), utf8 ? Encoding.UTF8.GetBytes(blob) : Convert.FromBase64String(blob), oaep));
            return null;
        }

        /// <summary>
        /// Encrypts a text with FIPS compliant RSA.
        /// </summary>
        /// <param name="text">The UTF8 text to encrypt.</param>
        /// <param name="xml">The UTF8 public RSA key xml.</param>
        /// <param name="oaep">(Optional) the optimal asymmetric encryption padding.</param>
        /// <returns>
        /// A Base64 cipher.
        /// </returns>
        public static string RsaEncryptXml(string text, string xml, bool oaep = true)
        {
            Contract.Requires(xml != null);

            if(text != null)
            {
                using(var rsa = new RSACryptoServiceProvider())
                {
                    rsa.FromXmlString(xml);
                    return Convert.ToBase64String(RsaEncrypt(rsa, Encoding.UTF8.GetBytes(text), oaep));
                }
            }
            return null;
        }

        private static byte[] RsaDecrypt(RSACryptoServiceProvider rsa, byte[] cipher, bool oaep = true)
        {
            if(cipher != null)
            {
                Array.Reverse(cipher);
                int max = rsa.KeySize / 8;
                if(max >= cipher.Length)
                    return rsa.Decrypt(cipher, oaep);
                using(var ms = new MemoryStream())
                {
                    if(ms.CanTimeout)
                        ms.ReadTimeout = ms.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                    using(var temp = new MemoryStream(cipher))
                    {
                        if(temp.CanTimeout)
                            temp.ReadTimeout = temp.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                        var buffer = new byte[max];
                        while(temp.Read(buffer, 0, max) > 0)
                        {
                            byte[] text = rsa.Decrypt(buffer, oaep);
                            ms.Write(text, 0, text.Length);
                        }
                    }
                    return ms.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Decrypts a cipher with FIPS compliant RSA.
        /// </summary>
        /// <param name="cipher">The cipher to decrypt.</param>
        /// <param name="blob">The private RSA key blob.</param>
        /// <param name="oaep">(Optional) the optimal asymmetric encryption padding.</param>
        /// <returns>
        /// A data.
        /// </returns>
        public static byte[] RsaDecryptBlob(byte[] cipher, byte[] blob, bool oaep = true)
        {
            Contract.Requires(blob != null);

            if(cipher != null && cipher.Length > 0)
            {
                using(var rsa = new RSACryptoServiceProvider())
                {
                    rsa.ImportCspBlob(blob);
                    return RsaDecrypt(rsa, cipher, oaep);
                }
            }
            return null;
        }

        /// <summary>
        /// Decrypts a cipher with FIPS compliant RSA.
        /// </summary>
        /// <param name="cipher">The Base64 cipher to decrypt.</param>
        /// <param name="blob">The UTF8/Base64 (<paramref name="utf8"/>) private RSA key blob.</param>
        /// <param name="utf8">(Optional) <c>true</c> for UTF8, or <c>false</c> for Base64 blob encoding.</param>
        /// <param name="oaep">(Optional) the optimal asymmetric encryption padding.</param>
        /// <returns>
        /// An UTF8 text.
        /// </returns>
        public static string RsaDecryptBlob(string cipher, string blob, bool utf8 = false, bool oaep = true)
        {
            Contract.Requires(blob != null);

            if(!string.IsNullOrWhiteSpace(cipher))
                return Encoding.UTF8.GetString(RsaDecryptBlob(Convert.FromBase64String(cipher), utf8 ? Encoding.UTF8.GetBytes(blob) : Convert.FromBase64String(blob), oaep));
            return null;
        }

        /// <summary>
        /// Decrypts a cipher with FIPS compliant RSA.
        /// </summary>
        /// <param name="cipher">The Base64 cipher to decrypt.</param>
        /// <param name="xml">The UTF8 private RSA key xml.</param>
        /// <param name="oaep">(Optional) the optimal asymmetric encryption padding.</param>
        /// <returns>
        /// An UTF8 text.
        /// </returns>
        public static string RsaDecryptXml(string cipher, string xml, bool oaep = true)
        {
            Contract.Requires(xml != null);

            if(!string.IsNullOrWhiteSpace(cipher))
            {
                using(var rsa = new RSACryptoServiceProvider())
                {
                    rsa.FromXmlString(xml);
                    return Encoding.UTF8.GetString(RsaDecrypt(rsa, Convert.FromBase64String(cipher), oaep));
                }
            }
            return null;
        }

        /// <summary>
        /// Signs a data with RSA.
        /// </summary>
        /// <param name="data">The data to be signed.</param>
        /// <param name="blob">The private RSA key blob.</param>
        /// <param name="salt">(Optional) the hash salt.</param>
        /// <param name="alg">(Optional) the hashing algorithm.</param>
        /// <returns>
        /// A signature of the data.
        /// </returns>
        public static byte[] RsaSignBlob(byte[] data, byte[] blob, byte[] salt = null, HashAlgorithm alg = HashAlgorithm.SHA256)
        {
            Contract.Requires(blob != null);

            if(data != null)
            {
                data = Hash(alg, data, salt);
                using(var rsa = new RSACryptoServiceProvider())
                {
                    rsa.ImportCspBlob(blob);
                    return rsa.SignHash(data, alg.GetName());
                }
            }
            return null;
        }

        /// <summary>
        /// Signs a text with RSA.
        /// </summary>
        /// <param name="text">The UTF8 text to be signed.</param>
        /// <param name="blob">The UTF8/Base64 (<paramref name="utf8"/>) private RSA key blob.</param>
        /// <param name="utf8">(Optional) <c>true</c> for UTF8, or <c>false</c> for Base64 blob encoding.</param>
        /// <param name="salt">(Optional) the UTF8 hash salt.</param>
        /// <param name="alg">(Optional) the hashing algorithm.</param>
        /// <returns>
        /// A Base64 signature of the text.
        /// </returns>
        public static string RsaSignBlob(string text, string blob, bool utf8 = false, string salt = null, HashAlgorithm alg = HashAlgorithm.SHA256)
        {
            Contract.Requires(blob != null);

            if(text != null)
                return Convert.ToBase64String(RsaSignBlob(Encoding.UTF8.GetBytes(text), utf8 ? Encoding.UTF8.GetBytes(blob) : Convert.FromBase64String(blob), !string.IsNullOrWhiteSpace(salt) ? Encoding.UTF8.GetBytes(salt) : null, alg));
            return null;
        }

        /// <summary>
        /// Signs a text with RSA.
        /// </summary>
        /// <param name="text">The UTF8 text to be signed.</param>
        /// <param name="xml">The UTF8 private RSA key xml.</param>
        /// <param name="salt">(Optional) the UTF8 hash salt.</param>
        /// <param name="alg">(Optional) the hashing algorithm.</param>
        /// <returns>
        /// A Base64 signature of the text.
        /// </returns>
        public static string RsaSignXml(string text, string xml, string salt = null, HashAlgorithm alg = HashAlgorithm.SHA256)
        {
            Contract.Requires(xml != null);

            if(text != null)
            {
                byte[] data = Hash(alg, Encoding.UTF8.GetBytes(text), !string.IsNullOrWhiteSpace(salt) ? Encoding.UTF8.GetBytes(salt) : null);
                using(var rsa = new RSACryptoServiceProvider())
                {
                    rsa.FromXmlString(xml);
                    return Convert.ToBase64String(rsa.SignHash(data, alg.GetName()));
                }
            }
            return null;
        }

        /// <summary>
        /// Verifies a signature with RSA.
        /// </summary>
        /// <param name="sign">The signature to verify.</param>
        /// <param name="data">The data to be signed.</param>
        /// <param name="blob">The public RSA key blob.</param>
        /// <param name="salt">(Optional) the hash salt.</param>
        /// <param name="alg">(Optional) the hashing algorithm.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        public static bool RsaVerifyBlob(byte[] sign, byte[] data, byte[] blob, byte[] salt = null, HashAlgorithm alg = HashAlgorithm.SHA256)
        {
            Contract.Requires(blob != null);

            if(data == null && (sign == null || sign.Length == 0))
                return true;
            if(data != null && sign != null && sign.Length > 0)
            {
                data = Hash(alg, data, salt);
                using(var rsa = new RSACryptoServiceProvider())
                {
                    rsa.ImportCspBlob(blob);
                    return rsa.VerifyHash(data, alg.GetName(), sign);
                }
            }
            return false;
        }

        /// <summary>
        /// Verifies a signature with RSA.
        /// </summary>
        /// <param name="sign">The Base64 signature to verify.</param>
        /// <param name="text">The UTF8 text to be signed.</param>
        /// <param name="blob">The UTF8/Base64 (<paramref name="utf8"/>) public RSA key blob.</param>
        /// <param name="utf8">(Optional) <c>true</c> for UTF8, or <c>false</c> for Base64 blob encoding.</param>
        /// <param name="salt">(Optional) the UTF8 hash salt.</param>
        /// <param name="alg">(Optional) the hashing algorithm.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        public static bool RsaVerifyBlob(string sign, string text, string blob, bool utf8 = false, string salt = null, HashAlgorithm alg = HashAlgorithm.SHA256)
        {
            Contract.Requires(blob != null);

            if(text == null && string.IsNullOrWhiteSpace(sign))
                return true;
            if(text != null && !string.IsNullOrWhiteSpace(sign))
                return RsaVerifyBlob(Convert.FromBase64String(sign), Encoding.UTF8.GetBytes(text), utf8 ? Encoding.UTF8.GetBytes(blob) : Convert.FromBase64String(blob), !string.IsNullOrWhiteSpace(salt) ? Encoding.UTF8.GetBytes(salt) : null, alg);
            return false;
        }

        /// <summary>
        /// Verifies a signature with RSA.
        /// </summary>
        /// <param name="sign">The Base64 signature to verify.</param>
        /// <param name="text">The UTF8 text to be signed.</param>
        /// <param name="xml">The UTF8 public RSA key xml.</param>
        /// <param name="salt">(Optional) the UTF8 hash salt.</param>
        /// <param name="alg">(Optional) the hashing algorithm.</param>
        /// <returns>
        /// <c>true</c> if it succeeds, <c>false</c> if it fails.
        /// </returns>
        public static bool RsaVerifyXml(string sign, string text, string xml, string salt = null, HashAlgorithm alg = HashAlgorithm.SHA256)
        {
            Contract.Requires(xml != null);

            if(text == null && string.IsNullOrWhiteSpace(sign))
                return true;
            if(text != null && !string.IsNullOrWhiteSpace(sign))
            {
                byte[] data = Hash(alg, Encoding.UTF8.GetBytes(text), !string.IsNullOrWhiteSpace(salt) ? Encoding.UTF8.GetBytes(salt) : null);
                using(var rsa = new RSACryptoServiceProvider())
                {
                    rsa.FromXmlString(xml);
                    return rsa.VerifyHash(data, alg.GetName(), Convert.FromBase64String(sign));
                }
            }
            return false;
        }

        /// <summary>
        /// Compresses a data with Deflate algorithm.
        /// </summary>
        /// <param name="data">The data to compress.</param>
        /// <returns>
        /// A cipher.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static byte[] DeflateCompress(byte[] data)
        {
            if(data != null)
            {
                using(var ms = new MemoryStream())
                {
                    if(ms.CanTimeout)
                        ms.ReadTimeout = ms.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                    using(var deflate = new DeflateStream(ms, CompressionMode.Compress, true))
                    {
                        if(deflate.CanTimeout)
                            deflate.ReadTimeout = deflate.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                        deflate.Write(data, 0, data.Length);
                    }
                    return ms.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Compresses a text with Deflate algorithm.
        /// </summary>
        /// <param name="text">The UTF8/Base64 (<paramref name="utf8"/>) text to compress.</param>
        /// <param name="utf8">(Optional) <c>true</c> for UTF8, or <c>false</c> for Base64 text encoding.</param>
        /// <returns>
        /// A Base64 cipher.
        /// </returns>
        public static string DeflateCompress(string text, bool utf8)
        {
            if(text != null)
                return Convert.ToBase64String(DeflateCompress(utf8 ? Encoding.UTF8.GetBytes(text) : Convert.FromBase64String(text)));
            return null;
        }

        /// <summary>
        /// Decompresses a cipher with Deflate algorithm.
        /// </summary>
        /// <param name="cipher">The cipher to decompress.</param>
        /// <returns>
        /// A data.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static byte[] DeflateDecompress(byte[] cipher)
        {
            if(cipher != null && cipher.Length != 0)
            {
                using(var ms = new MemoryStream())
                {
                    if(ms.CanTimeout)
                        ms.ReadTimeout = ms.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                    using(var temp = new MemoryStream(cipher))
                    {
                        if(temp.CanTimeout)
                            temp.ReadTimeout = temp.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                        using(var deflate = new DeflateStream(temp, CompressionMode.Decompress, true))
                        {
                            if(deflate.CanTimeout)
                                deflate.ReadTimeout = deflate.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                            deflate.CopyTo(ms);
                        }
                    }
                    return ms.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Decompresses a cipher with Deflate algorithm.
        /// </summary>
        /// <param name="cipher">The Base64 cipher to decompress.</param>
        /// <param name="utf8">(Optional) <c>true</c> for UTF8, or <c>false</c> for Base64 text encoding.</param>
        /// <returns>
        /// An UTF8/Base64 (<paramref name="utf8"/>) text.
        /// </returns>
        public static string DeflateDecompress(string cipher, bool utf8)
        {
            if(!string.IsNullOrWhiteSpace(cipher))
            {
                var data = DeflateDecompress(Convert.FromBase64String(cipher));
                if(utf8)
                    return Encoding.UTF8.GetString(data);
                return Convert.ToBase64String(data);
            }
            return null;
        }

        /// <summary>
        /// Compresses a data with GZip algorithm.
        /// </summary>
        /// <param name="data">The data to compress.</param>
        /// <returns>
        /// A cipher.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static byte[] GZipCompress(byte[] data)
        {
            if(data != null)
            {
                using(var ms = new MemoryStream())
                {
                    if(ms.CanTimeout)
                        ms.ReadTimeout = ms.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                    using(var gzip = new GZipStream(ms, CompressionMode.Compress, true))
                    {
                        if(gzip.CanTimeout)
                            gzip.ReadTimeout = gzip.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                        gzip.Write(data, 0, data.Length);
                    }
                    return ms.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Compresses a text with GZip algorithm.
        /// </summary>
        /// <param name="text">The UTF8/Base64 (<paramref name="utf8"/>) text to compress.</param>
        /// <param name="utf8">(Optional) <c>true</c> for UTF8, or <c>false</c> for Base64 text encoding.</param>
        /// <returns>
        /// A Base64 cipher.
        /// </returns>
        public static string GZipCompress(string text, bool utf8)
        {
            if(text != null)
                return Convert.ToBase64String(GZipCompress(utf8 ? Encoding.UTF8.GetBytes(text) : Convert.FromBase64String(text)));
            return null;
        }

        /// <summary>
        /// Decompresses a cipher with GZip algorithm.
        /// </summary>
        /// <param name="cipher">The cipher to decompress.</param>
        /// <returns>
        /// A data.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2202:Do not dispose objects multiple times")]
        public static byte[] GZipDecompress(byte[] cipher)
        {
            if(cipher != null && cipher.Length != 0)
            {
                using(var ms = new MemoryStream())
                {
                    if(ms.CanTimeout)
                        ms.ReadTimeout = ms.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                    using(var temp = new MemoryStream(cipher))
                    {
                        if(temp.CanTimeout)
                            temp.ReadTimeout = temp.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                        using(var gzip = new GZipStream(temp, CompressionMode.Decompress, true))
                        {
                            if(gzip.CanTimeout)
                                gzip.ReadTimeout = gzip.WriteTimeout = (int)Config.TimeOut.TotalMilliseconds;
                            gzip.CopyTo(ms);
                        }
                    }
                    return ms.ToArray();
                }
            }
            return null;
        }

        /// <summary>
        /// Decompresses a cipher with GZip algorithm.
        /// </summary>
        /// <param name="cipher">The Base64 cipher to decompress.</param>
        /// <param name="utf8">(Optional) <c>true</c> for UTF8, or <c>false</c> for Base64 text encoding.</param>
        /// <returns>
        /// An UTF8/Base64 (<paramref name="utf8"/>) text.
        /// </returns>
        public static string GZipDecompress(string cipher, bool utf8)
        {
            if(!string.IsNullOrWhiteSpace(cipher))
            {
                var data = GZipDecompress(Convert.FromBase64String(cipher));
                if(utf8)
                    return Encoding.UTF8.GetString(data);
                return Convert.ToBase64String(data);
            }
            return null;
        }

        /// <summary>
        /// Generates a cryptographically strong sequence of random values, suitable as a salt, an IV, or a hash/symmetric key.
        /// </summary>
        /// <param name="bytes">The data size in bytes.</param>
        /// <returns>
        /// A random data.
        /// </returns>
        public static byte[] RandomValues(int bytes)
        {
            Contract.Requires(bytes > 0);

            var data = new byte[bytes];
            using(var rng = new RNGCryptoServiceProvider())
                rng.GetBytes(data);
            return data;
        }

        /// <summary>
        /// Generates a cryptographically strong sequence of random values, suitable as a salt, an IV, or a hash/symmetric key.
        /// </summary>
        /// <param name="bytes">The text size in bytes.</param>
        /// <param name="hex"><c>true</c> for Base16, or <c>false</c> for Base64 text encoding.</param>
        /// <returns>
        /// A Base16/Base64 (<paramref name="hex"/>) random text.
        /// </returns>
        public static string RandomValues(int bytes, bool hex)
        {
            Contract.Requires(bytes > 0);

            byte[] data = RandomValues(bytes);
            if(hex)
                return Helper.ConvertToBase16String(data);
            return Convert.ToBase64String(data);
        }
    } 
}
