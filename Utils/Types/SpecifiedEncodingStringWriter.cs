﻿namespace Utils.Types
{
    using System;
    using System.Diagnostics.Contracts;
    using System.IO;
    using System.Text;

    /// <summary>
    /// <see cref="System.IO.StringWriter"/> with specified <see cref="System.Text.Encoding"/>.
    /// </summary>
    /// <remarks>
    /// Defaults to <see cref="System.Text.Encoding.UTF8"/> instead of <see cref="System.Text.Encoding.Unicode"/> (UTF16).
    /// </remarks>
    public sealed class SpecifiedEncodingStringWriter : StringWriter
    {
        private readonly Encoding _encoding;

        /// <inheritdoc/>
        public override Encoding Encoding
        {
            get { return _encoding; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedEncodingStringWriter"/> class.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.IO.StringWriter.#ctor")]
        public SpecifiedEncodingStringWriter()
        {
            _encoding = Encoding.UTF8;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedEncodingStringWriter"/> class.
        /// </summary>
        /// <param name="encoding">The Encoding in which the output is written.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.IO.StringWriter.#ctor")]
        public SpecifiedEncodingStringWriter(Encoding encoding)
        {
            _encoding = encoding;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedEncodingStringWriter"/> class.
        /// </summary>
        /// <param name="sb">The StringBuilder to write to.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.IO.StringWriter.#ctor(System.Text.StringBuilder)")]
        public SpecifiedEncodingStringWriter(StringBuilder sb) : base(sb)
        {
            Contract.Requires(sb != null);

            _encoding = Encoding.UTF8;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedEncodingStringWriter"/> class.
        /// </summary>
        /// <param name="sb">The StringBuilder to write to.</param>
        /// <param name="encoding">The Encoding in which the output is written.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.IO.StringWriter.#ctor(System.Text.StringBuilder)")]
        public SpecifiedEncodingStringWriter(StringBuilder sb, Encoding encoding) : base(sb)
        {
            Contract.Requires(sb != null);

            _encoding = encoding;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedEncodingStringWriter"/> class.
        /// </summary>
        /// <param name="sb">The StringBuilder to write to.</param>
        /// <param name="formatProvider">A format provider that controls formatting.</param>
        public SpecifiedEncodingStringWriter(StringBuilder sb, IFormatProvider formatProvider) : base(sb, formatProvider)
        {
            Contract.Requires(sb != null);

            _encoding = Encoding.UTF8;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedEncodingStringWriter"/> class.
        /// </summary>
        /// <param name="sb">The StringBuilder to write to.</param>
        /// <param name="formatProvider">A format provider that controls formatting.</param>
        /// <param name="encoding">The Encoding in which the output is written.</param>
        public SpecifiedEncodingStringWriter(StringBuilder sb, IFormatProvider formatProvider, Encoding encoding) : base(sb, formatProvider)
        {
            Contract.Requires(sb != null);

            _encoding = encoding;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedEncodingStringWriter"/> class.
        /// </summary>
        /// <param name="formatProvider">A format provider that controls formatting.</param>
        public SpecifiedEncodingStringWriter(IFormatProvider formatProvider) : base(formatProvider)
        {
            _encoding = Encoding.UTF8;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpecifiedEncodingStringWriter"/> class.
        /// </summary>
        /// <param name="formatProvider">A format provider that controls formatting.</param>
        /// <param name="encoding">The Encoding in which the output is written.</param>
        public SpecifiedEncodingStringWriter(IFormatProvider formatProvider, Encoding encoding) : base(formatProvider)
        {
            _encoding = encoding;
        }
    }
}
