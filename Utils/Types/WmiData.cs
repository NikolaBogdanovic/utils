﻿namespace Utils.Types
{
    using System;

    // ReSharper disable InconsistentNaming
    #pragma warning disable 1591

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public class CIM_ManagedSystemElement
    {
        public string Name { get; private set; }
        public string Status { get; private set; }

        public CIM_ManagedSystemElement(string name, string status)
        {
            Name = name;
            Status = status;
        }
    }

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Win32_CDROMDrive : CIM_ManagedSystemElement
    {
        public string VolumeName { get; private set; }
        public string DeviceID { get; private set; }

        public Win32_CDROMDrive(string name, string status, string volume, string drive) : base(name, status)
        {
            VolumeName = volume;
            DeviceID = drive;
        }
    }

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Win32_LogicalDisk : CIM_ManagedSystemElement
    {
        public ulong Size { get; private set; }
        public ulong FreeSpace { get; private set; }
        public string DeviceID { get; private set; }
        public string VolumeName { get; private set; }
        public string VolumeSerialNumber { get; private set; }

        public Win32_LogicalDisk(string name, string status, ulong size, ulong free, string drive, string volume, string serial) : base(name, status)
        {
            Size = size;
            FreeSpace = free;
            DeviceID = drive;
            VolumeName = volume;
            VolumeSerialNumber = serial;
        }
    }

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Win32_VideoController : CIM_ManagedSystemElement
    {
        public string DriverVersion { get; private set; }
        public uint AdapterRAM { get; private set; }
        public string VideoProcessor { get; private set; }

        public Win32_VideoController(string name, string status, string driver, uint ram, string processor) : base(name, status)
        {
            DriverVersion = driver;
            AdapterRAM = ram;
            VideoProcessor = processor;
        }
    }

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Win32_Keyboard : CIM_ManagedSystemElement
    {
        public string Layout { get; private set; }

        public Win32_Keyboard(string name, string status, string layout) : base(name, status)
        {
            Layout = layout;
        }
    }

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Win32_NetworkAdapter : CIM_ManagedSystemElement
    {
        public string MACAddress { get; private set; }

        public Win32_NetworkAdapter(string name, string status, string mac) : base(name, status)
        {
            MACAddress = mac;
        }
    }

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Win32_OperatingSystem : CIM_ManagedSystemElement
    {
        public ulong FreePhysicalMemory { get; private set; }
        public ulong TotalVisibleMemorySize { get; private set; }
        public string RegisteredUser { get; private set; }
        public string SerialNumber { get; private set; }
        public string WindowsDirectory { get; private set; }
        public string CSName { get; private set; }

        public Win32_OperatingSystem(string name, string status, ulong free, ulong ram, string user, string serial, string dir, string comp) : base(name, status)
        {
            FreePhysicalMemory = free;
            TotalVisibleMemorySize = ram;
            RegisteredUser = user;
            SerialNumber = serial;
            WindowsDirectory = dir;
            CSName = comp;
        }
    }

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Win32_PointingDevice : CIM_ManagedSystemElement
    {
        public string HardwareType { get; private set; }

        public Win32_PointingDevice(string name, string status, string type) : base(name, status)
        {
            HardwareType = type;
        }
    }

    /// <exclude/>
    [CLSCompliant(false)]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public sealed class Win32_Processor : CIM_ManagedSystemElement
    {
        public string Version { get; private set; }
        public uint MaxClockSpeed { get; private set; }
        public string Manufacturer { get; private set; }

        public Win32_Processor(string name, string status, string version, uint speed, string mfr) : base(name, status)
        {
            Version = version;
            MaxClockSpeed = speed;
            Manufacturer = mfr;
        }
    }

    #pragma warning restore 1591
    // ReSharper restore InconsistentNaming
}
