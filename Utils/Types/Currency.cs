﻿namespace Utils.Types
{
    using System.Globalization;

    /// <summary>
    /// Currency converter.
    /// </summary>
    public sealed class Currency
    {
        /// <summary>
        /// Gets the ISO symbol.
        /// </summary>
        public string IsoSymbol { get; private set; }

        /// <summary>
        /// Gets the number of decimal places.
        /// </summary>
        public int DecimalDigits { get; private set; }

        /// <summary>
        /// Gets the lowest denomination.
        /// </summary>
        public decimal LowestDenomination { get; private set; }

        /// <summary>
        /// Gets the foreign exchange rate for server currency.
        /// </summary>
        public decimal ExchangeRate { get; private set; }

        /// <summary>
        /// Gets the foreign exchange rate last updated date.
        /// </summary>
        public Date? ExchangeDate { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Currency"/> class.
        /// </summary>
        /// <param name="iso">The ISO symbol.</param>
        /// <param name="decimals">The number of decimal places.</param>
        /// <param name="exchange">The foreign exchange rate for server currency.</param>
        /// <param name="date">The foreign exchange last updated date.</param>
        public Currency(string iso, int decimals, decimal exchange, Date? date = null)
        {
            IsoSymbol = iso;
            DecimalDigits = decimals;
            ExchangeRate = exchange;
            ExchangeDate = date;

            decimal step = 1m;
            for(int i = 1, j = DecimalDigits; i <= j; i++)
                step *= 0.1m;
            LowestDenomination = step;
        }

        /// <summary>
        /// Formats an amount of money as currency.
        /// </summary>
        /// <param name="amount">The amount of money.</param>
        /// <returns>
        /// The currency.
        /// </returns>
        public string Money(decimal? amount)
        {
            if(amount != null)
                return string.Format(CultureInfo.CurrentCulture, "{0} {1}", amount.Value.ToString("N" + DecimalDigits.ToString(CultureInfo.InvariantCulture), CultureInfo.CurrentCulture), IsoSymbol);
            return null;
        }

        /// <summary>
        /// Formats an amount of money as currency.
        /// </summary>
        /// <param name="amount">The amount of money.</param>
        /// <returns>
        /// The currency.
        /// </returns>
        public string Money(double? amount)
        {
            return Money((decimal?)amount);
        }
    } 
}
