﻿namespace Utils.Types
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Hosting;

    /// <summary>
    /// Registers a not awaited (fire-and-forget) <see cref="System.Threading.Tasks.Task"/> for cancellation on Application Pool recycle (otherwise it forcibly ends in an unknown state).
    /// </summary>
    /// <remarks>
    /// On recycle, a registered <see cref="System.Threading.Tasks.Task"/> has 30 seconds to complete successfully, before the cancellation is requested.
    /// </remarks>
    public sealed class RegisteredTask : IRegisteredObject, IDisposable
    {
        private static readonly RegisteredTask Instance = new RegisteredTask();

        private int _count;
        private ManualResetEventSlim _lock;
        private CancellationTokenSource _source;

        /// <summary>
        /// Gets the cancellation token that signals on Application Pool recycle.
        /// </summary>
        /// <remarks>
        /// Can be linked with other cancellation sources: <c>CancellationTokenSource.CreateLinkedTokenSource(RegisteredTask.CancelToken, new CancellationTokenSource(delay).Token).Token</c>
        /// </remarks>
        public static CancellationToken CancelToken
        {
            get { return Instance._source.Token; }
        }

        private RegisteredTask()
        {
            _count = 1;
            _lock = new ManualResetEventSlim();
            _source = new CancellationTokenSource();
            HostingEnvironment.RegisterObject(this);
        }

        /// <summary>
        /// Increments Application Busy Count with each not awaited (fire-and-forget) <see cref="System.Threading.Tasks.Task"/>, and decrements whenever one is completed, to prevent Application Pool with no active requests from becoming idle and triggering a recycle.
        /// </summary>
        /// <remarks>
        /// Usage: <c>RegisteredTask.Start(Task.Run(action, RegisteredTask.CancelToken))</c>
        /// </remarks>
        /// <param name="task">The not awaited (fire-and-forget) task.</param>
        public static void Start(Task task)
        {
            if(Instance._count != 0 && !Instance._source.IsCancellationRequested)
            {
                Interlocked.Increment(ref Instance._count);
                HostingEnvironment.IncrementBusyCount();
                task.ContinueWith(
                    t =>
                    {
                        HostingEnvironment.DecrementBusyCount();
                        if(Interlocked.Decrement(ref Instance._count) == 0)
                        {
                            HostingEnvironment.UnregisterObject(Instance);
                            Instance._lock.Set();
                        }
                    },
                    TaskContinuationOptions.ExecuteSynchronously);
            }
        }

        /// <inheritdoc/>
        public void Stop(bool immediate)
        {
            if(!immediate)
            {
                if(Interlocked.Decrement(ref _count) == 0)
                    HostingEnvironment.UnregisterObject(this);

                // immediate timeout is hard-coded by framework to 30 seconds
                _source.CancelAfter(30000);
            }
            else
            {
                if(!_source.IsCancellationRequested)
                    _source.Cancel();

                // still wait even after immediate timeout, but not forever
                _lock.Wait((int)Config.TimeOut.TotalMilliseconds - 30000);
            }
        }

        /// <inheritdoc/>
        public void Dispose()
        {
            if(_lock != null)
            {
                _lock.Dispose();
                _lock = null;
            }
            if(_source != null)
            {
                _source.Dispose();
                _source = null;
            }
        }
    }
}
