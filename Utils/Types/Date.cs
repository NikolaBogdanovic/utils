﻿namespace Utils.Types
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Runtime.Serialization;

    /// <summary>
    /// <see cref="System.DateTime"/> without the time component.
    /// </summary>
    [Serializable]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Date")]
    public struct Date : IComparable, IComparable<DateTime>, IComparable<DateTimeOffset>, IComparable<Date>, IEquatable<DateTime>, IEquatable<DateTimeOffset>, IEquatable<Date>, IFormattable, IConvertible, ISerializable
    {
        private readonly DateTime _dt;

        /// <summary>
        /// Represents the smallest possible value of <see cref="Date"/>. This field is read-only.
        /// </summary>
        public static readonly Date MinValue = new Date(DateTime.MinValue);

        /// <summary>
        /// Represents the largest possible value of <see cref="Date"/>. This field is read-only.
        /// </summary>
        public static readonly Date MaxValue = new Date(DateTime.MaxValue);

        /// <summary>
        /// Gets the day of the month represented by this instance.
        /// </summary>
        public int Day
        {
            get
            {
                return _dt.Day;
            }
        }

        /// <summary>
        /// Gets the day of the week represented by this instance.
        /// </summary>
        public DayOfWeek DayOfWeek
        {
            get
            {
                return _dt.DayOfWeek;
            }
        }

        /// <summary>
        /// Gets the day of the year represented by this instance.
        /// </summary>
        public int DayOfYear
        {
            get
            {
                return _dt.DayOfYear;
            }
        }

        /// <summary>
        /// Gets the month component of the date represented by this instance.
        /// </summary>
        public int Month
        {
            get
            {
                return _dt.Month;
            }
        }

        /// <summary>
        /// Gets the current date.
        /// </summary>
        public static Date Today
        {
            get
            {
                return new Date(DateTime.Today);
            }
        }

        /// <summary>
        /// Gets the year component of the date represented by this instance.
        /// </summary>
        public int Year
        {
            get
            {
                return _dt.Year;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Date"/> struct.
        /// </summary>
        /// <param name="dt">The date and time value.</param>
        public Date(DateTime dt)
        {
            _dt = dt.Kind != DateTimeKind.Unspecified ? DateTime.SpecifyKind(dt.Date, DateTimeKind.Unspecified) : dt.Date;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Date"/> struct.
        /// </summary>
        /// <param name="dto">The date and time offset value.</param>
        public Date(DateTimeOffset dto)
        {
            _dt = dto.Date;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Date"/> struct.
        /// </summary>
        /// <param name="year">The year (1 through 9999).</param>
        /// <param name="month">The month (1 through 12).</param>
        /// <param name="day">The day (1 through the number of days in <paramref name="month"/>).</param>
        public Date(int year, int month, int day)
        {
            Contract.Requires(1 <= year && year <= 9999);
            Contract.Requires(1 <= month && month <= 12);
            Contract.Requires(1 <= day && day <= 31);

            _dt = new DateTime(year, month, day);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Date"/> struct.
        /// </summary>
        /// <param name="year">The year (1 through the number of years in <paramref name="calendar"/>).</param>
        /// <param name="month">The month (1 through the number of months in <paramref name="calendar"/>).</param>
        /// <param name="day">The day (1 through the number of days in <paramref name="month"/>).</param>
        /// <param name="calendar">The calendar that is used to interpret <paramref name="year"/>, <paramref name="month"/>, and <paramref name="day"/>.</param>
        public Date(int year, int month, int day, Calendar calendar)
        {
            Contract.Requires(calendar != null);

            _dt = new DateTime(year, month, day, calendar);
        }

        private Date(SerializationInfo info, StreamingContext context)
        {
            Contract.Requires(info != null);

            _dt = new DateTime(info.GetInt64("ticks"));
        }

        /// <summary>
        /// User-defined explicit conversion from a <see cref="System.DateTime"/> object to a <see cref="Date"/> object.
        /// </summary>
        /// <param name="dt">The date and time value to convert.</param>
        /// <returns>
        /// A date object.
        /// </returns>
        public static explicit operator Date(DateTime dt)
        {
            return new Date(dt);
        }

        /// <summary>
        /// User-defined explicit conversion from a <see cref="System.DateTimeOffset"/> object to a <see cref="Date"/> object.
        /// </summary>
        /// <param name="dto">The date and time offset value to convert.</param>
        /// <returns>
        /// A date object.
        /// </returns>
        public static explicit operator Date(DateTimeOffset dto)
        {
            return new Date(dto);
        }

        /// <summary>
        /// User-defined implicit conversion from a <see cref="Date"/> object to a <see cref="System.DateTime"/> object.
        /// </summary>
        /// <param name="d">The date value to convert.</param>
        /// <returns>
        /// A date and time object.
        /// </returns>
        public static implicit operator DateTime(Date d)
        {
            return d._dt;
        }

        /// <summary>
        /// User-defined implicit conversion from a <see cref="Date"/> object to a <see cref="System.DateTimeOffset"/> object.
        /// </summary>
        /// <param name="d">The date value to convert.</param>
        /// <returns>
        /// A date and time offset object.
        /// </returns>
        public static implicit operator DateTimeOffset(Date d)
        {
            return new DateTimeOffset(d._dt);
        }

        /// <summary>
        /// Adds a specified time interval to a specified date, yielding a new date.
        /// </summary>
        /// <param name="d">The date value to add.</param>
        /// <param name="t">The time interval to add.</param>
        /// <returns>
        /// A new <see cref="Date"/> that is the sum of the values of <paramref name="d"/> and <paramref name="t"/>.
        /// </returns>
        public static Date operator +(Date d, TimeSpan t)
        {
            return new Date(d._dt + t);
        }

        /// <summary>
        /// Subtracts a specified time interval from a specified date and returns a new date.
        /// </summary>
        /// <param name="d">The date value to subtract from.</param>
        /// <param name="t">The time interval to subtract.</param>
        /// <returns>
        /// A new <see cref="Date"/> whose value is the value of <paramref name="d"/> minus the value of <paramref name="t"/>.
        /// </returns>
        public static Date operator -(Date d, TimeSpan t)
        {
            return new Date(d._dt - t);
        }

        /// <summary>
        /// Subtracts a specified date from another specified date and returns a time interval.
        /// </summary>
        /// <param name="d1">The date value to subtract from (the minuend).</param>
        /// <param name="d2">The date value to subtract (the subtrahend).</param>
        /// <returns>
        /// The time interval between <paramref name="d1"/> and <paramref name="d2"/>; that is, <paramref name="d1"/> minus <paramref name="d2"/>.
        /// </returns>
        public static TimeSpan operator -(Date d1, Date d2)
        {
            return d1._dt - d2._dt;
        }

        /// <summary>
        /// Subtracts a specified date and time from a specified date and returns a time interval.
        /// </summary>
        /// <param name="d">The date value to subtract from (the minuend).</param>
        /// <param name="dt">The date and time value to subtract (the subtrahend).</param>
        /// <returns>
        /// The time interval between <paramref name="d"/> and <paramref name="dt"/>; that is, <paramref name="d"/> minus <paramref name="dt"/>.
        /// </returns>
        public static TimeSpan operator -(Date d, DateTime dt)
        {
            return d._dt - dt;
        }

        /// <summary>
        /// Subtracts a specified date and time offset from a specified date and returns a time interval.
        /// </summary>
        /// <param name="d">The date value to subtract from (the minuend).</param>
        /// <param name="dto">The date and time offset value to subtract (the subtrahend).</param>
        /// <returns>
        /// The time interval between <paramref name="d"/> and <paramref name="dto"/>; that is, <paramref name="d"/> minus <paramref name="dto"/>.
        /// </returns>
        public static TimeSpan operator -(Date d, DateTimeOffset dto)
        {
            return d._dt - dto.DateTime;
        }

        /// <summary>
        /// Determines whether two specified dates are equal.
        /// </summary>
        /// <param name="d1">The first date value to compare.</param>
        /// <param name="d2">The second date value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d1"/> and <paramref name="d2"/> represent the same date; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator ==(Date d1, Date d2)
        {
            return d1._dt == d2._dt;
        }

        /// <summary>
        /// Determines whether a specified date and time and a specified date are equal.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dt">The date and time value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> and <paramref name="dt"/> represent the same date and time; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator ==(Date d, DateTime dt)
        {
            return d._dt == dt;
        }

        /// <summary>
        /// Determines whether a specified date and time offset and a specified date are equal.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dto">The date and time offset value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> and <paramref name="dto"/> represent the same date and time; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator ==(Date d, DateTimeOffset dto)
        {
            return d._dt == dto.DateTime;
        }

        /// <summary>
        /// Determines whether two specified dates are not equal.
        /// </summary>
        /// <param name="d1">The first date value to compare.</param>
        /// <param name="d2">The second date value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d1"/> and <paramref name="d2"/> do not represent the same date; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator !=(Date d1, Date d2)
        {
            return d1._dt != d2._dt;
        }

        /// <summary>
        /// Determines whether a specified date and time and a specified date are not equal.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dt">The date and time value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> and <paramref name="dt"/> do not represent the same date and time; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator !=(Date d, DateTime dt)
        {
            return d._dt != dt;
        }

        /// <summary>
        /// Determines whether a specified date and time offset and a specified date are not equal.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dto">The date and time offset value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> and <paramref name="dto"/> do not represent the same date and time; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator !=(Date d, DateTimeOffset dto)
        {
            return d._dt != dto.DateTime;
        }

        /// <summary>
        /// Determines whether one specified date is less than another specified date.
        /// </summary>
        /// <param name="d1">The first date value to compare.</param>
        /// <param name="d2">The second date value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d1"/> is less than <paramref name="d2"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator <(Date d1, Date d2)
        {
            return d1._dt < d2._dt;
        }

        /// <summary>
        /// Determines whether a specified date value is less than a specified date and time.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dt">The date and time value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> is less than <paramref name="dt"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator <(Date d, DateTime dt)
        {
            return d._dt < dt;
        }

        /// <summary>
        /// Determines whether a specified date value is less than a specified date and time offset.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dto">The date and time offset value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> is less than <paramref name="dto"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator <(Date d, DateTimeOffset dto)
        {
            return d._dt < dto.DateTime;
        }

        /// <summary>
        /// Determines whether one specified date is less than or equal to another specified date.
        /// </summary>
        /// <param name="d1">The first date value to compare.</param>
        /// <param name="d2">The second date value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d1"/> is less than or equal to <paramref name="d2"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator <=(Date d1, Date d2)
        {
            return d1._dt <= d2._dt;
        }

        /// <summary>
        /// Determines whether a specified date value is less than or equal to a specified date and time.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dt">The date and time value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> is less than or equal to <paramref name="dt"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator <=(Date d, DateTime dt)
        {
            return d._dt <= dt;
        }

        /// <summary>
        /// Determines whether a specified date value is less than or equal to a specified date and time offset.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dto">The date and time offset value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> is less than or equal to <paramref name="dto"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator <=(Date d, DateTimeOffset dto)
        {
            return d._dt <= dto.DateTime;
        }

        /// <summary>
        /// Determines whether one specified date is greater than another specified date.
        /// </summary>
        /// <param name="d1">The first date value to compare.</param>
        /// <param name="d2">The second date value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d1"/> is greater than <paramref name="d2"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator >(Date d1, Date d2)
        {
            return d1._dt > d2._dt;
        }

        /// <summary>
        /// Determines whether a specified date value is greater than a specified date and time.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dt">The date and time value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> is greater than <paramref name="dt"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator >(Date d, DateTime dt)
        {
            return d._dt > dt;
        }

        /// <summary>
        /// Determines whether a specified date value is greater than a specified date and time offset.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dto">The date and time offset value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> is greater than <paramref name="dto"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator >(Date d, DateTimeOffset dto)
        {
            return d._dt > dto.DateTime;
        }

        /// <summary>
        /// Determines whether one specified date is greater than or equal to another specified date.
        /// </summary>
        /// <param name="d1">The first date value to compare.</param>
        /// <param name="d2">The second date value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d1"/> is greater than or equal to <paramref name="d2"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator >=(Date d1, Date d2)
        {
            return d1._dt >= d2._dt;
        }

        /// <summary>
        /// Determines whether a specified date value is greater than or equal to a specified date and time.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dt">The date and time value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> is greater than or equal to <paramref name="dt"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator >=(Date d, DateTime dt)
        {
            return d._dt >= dt;
        }

        /// <summary>
        /// Determines whether a specified date value is greater than or equal a specified date and time offset.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dto">The date and time offset value to compare.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="d"/> is greater than or equal <paramref name="dto"/>; otherwise, <c>false</c>.
        /// </returns>
        public static bool operator >=(Date d, DateTimeOffset dto)
        {
            return d._dt >= dto.DateTime;
        }

        /// <summary>
        /// Returns a new date that adds the value of the specified time interval to the value of this instance.
        /// </summary>
        /// <param name="value">A positive or negative time interval.</param>
        /// <returns>
        /// A new <see cref="Date"/> whose value is the sum of the date represented by this instance and the time interval represented by <paramref name="value"/>.
        /// </returns>
        public Date Add(TimeSpan value)
        {
            return new Date(_dt.Add(value));
        }

        /// <summary>
        /// Returns a new date that adds the specified number of days to the value of this instance.
        /// </summary>
        /// <param name="value">A positive or negative number of days.</param>
        /// <returns>
        /// A new <see cref="Date"/> whose value is the sum of the date represented by this instance and the number of days represented by <paramref name="value"/>.
        /// </returns>
        public Date AddDays(int value)
        {
            return new Date(_dt.AddDays(value));
        }

        /// <summary>
        /// Returns a new date that adds the specified number of months to the value of this instance.
        /// </summary>
        /// <param name="value">A positive or negative number of months.</param>
        /// <returns>
        /// A new <see cref="Date"/> whose value is the sum of the date represented by this instance and <paramref name="value"/>.
        /// </returns>
        public Date AddMonths(int value)
        {
            return new Date(_dt.AddMonths(value));
        }

        /// <summary>
        /// Returns a new date that adds the specified number of years to the value of this instance.
        /// </summary>
        /// <param name="value">A positive or negative number of years.</param>
        /// <returns>
        /// A new <see cref="Date"/> whose value is the sum of the date represented by this instance and the number of years represented by <paramref name="value"/>.
        /// </returns>
        public Date AddYears(int value)
        {
            return new Date(_dt.AddYears(value));
        }

        /// <summary>
        /// Compares two specified dates and returns an integer that indicates whether the first date is earlier than, the same as, or later than the second date.
        /// </summary>
        /// <param name="d1">The first date value to compare.</param>
        /// <param name="d2">The second date value to compare.</param>
        /// <returns>
        /// A signed number indicating the relative values of <paramref name="d1"/> and <paramref name="d2"/>. Less than zero indicates <paramref name="d1"/> is earlier than <paramref name="d2"/>. Zero indicates <paramref name="d1"/> is the same as <paramref name="d2"/>. Greater than zero indicates <paramref name="d1"/> is later than <paramref name="d2"/>.
        /// </returns>
        public static int Compare(Date d1, Date d2)
        {
            return DateTime.Compare(d1._dt, d2._dt);
        }

        /// <summary>
        /// Compares a specified date and a specified date and time and returns an integer that indicates whether the date is earlier than, the same as, or later than the date and time.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dt">The date and time value to compare.</param>
        /// <returns>
        /// A signed number indicating the relative values of <paramref name="d"/> and <paramref name="dt"/>. Less than zero indicates <paramref name="d"/> is earlier than <paramref name="dt"/>. Zero indicates <paramref name="d"/> is the same as <paramref name="dt"/>. Greater than zero indicates <paramref name="d"/> is later than <paramref name="dt"/>.
        /// </returns>
        public static int Compare(Date d, DateTime dt)
        {
            return DateTime.Compare(d._dt, dt);
        }

        /// <summary>
        /// Compares a specified date and a specified date and time offset and returns an integer that indicates whether the date is earlier than, the same as, or later than the date and time offset.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dto">The date and time offset value to compare.</param>
        /// <returns>
        /// A signed number indicating the relative values of <paramref name="d"/> and <paramref name="dto"/>. Less than zero indicates <paramref name="d"/> is earlier than <paramref name="dto"/>. Zero indicates <paramref name="d"/> is the same as <paramref name="dto"/>. Greater than zero indicates <paramref name="d"/> is later than <paramref name="dto"/>.
        /// </returns>
        public static int Compare(Date d, DateTimeOffset dto)
        {
            return DateTime.Compare(d._dt, dto.DateTime);
        }

        /// <inheritdoc/>
        public int CompareTo(Date other)
        {
            return _dt.CompareTo(other._dt);
        }

        /// <inheritdoc/>
        public int CompareTo(DateTime other)
        {
            return _dt.CompareTo(other);
        }

        /// <inheritdoc/>
        public int CompareTo(DateTimeOffset other)
        {
            return _dt.CompareTo(other.DateTime);
        }

        /// <inheritdoc/>
        public int CompareTo(object obj)
        {
            if(obj is Date)
                return CompareTo((Date)obj);
            if(obj is DateTime)
                return CompareTo((DateTime)obj);
            if(obj is DateTimeOffset)
                return CompareTo((DateTimeOffset)obj);
            throw new ArgumentException(@"Argument is not a Date, or a DateTime, or a DateTimeOffset type.", "obj");
        }

        /// <summary>
        /// Returns the number of days in the specified month and year.
        /// </summary>
        /// <param name="year">The year (1 through 9999).</param>
        /// <param name="month">The month (1 through 12).</param>
        /// <returns>
        /// The number of days in <paramref name="month"/> for the specified <paramref name="year"/>. For example, if <paramref name="month"/> equals 2 for February, the return value is 28 or 29 depending upon whether <paramref name="year"/> is a leap year.
        /// </returns>
        public static int DaysInMonth(int year, int month)
        {
            Contract.Requires(1 <= year && year <= 9999);
            Contract.Requires(1 <= month && month <= 12);

            return DateTime.DaysInMonth(year, month);
        }

        /// <inheritdoc/>
        public bool Equals(Date other)
        {
            return _dt.Equals(other._dt);
        }

        /// <inheritdoc/>
        public bool Equals(DateTime other)
        {
            return _dt.Equals(other);
        }

        /// <inheritdoc/>
        public bool Equals(DateTimeOffset other)
        {
            return _dt.Equals(other.DateTime);
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            if(obj is Date)
                return Equals((Date)obj);
            if(obj is DateTime)
                return Equals((DateTime)obj);
            if(obj is DateTimeOffset)
                return Equals((DateTimeOffset)obj);
            return false;
        }

        /// <summary>
        /// Returns a value indicating whether two specified dates have the same date value.
        /// </summary>
        /// <param name="d1">The first date value to compare.</param>
        /// <param name="d2">The second date value to compare.</param>
        /// <returns>
        /// <c>true</c> if the two values are equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool Equals(Date d1, Date d2)
        {
            return DateTime.Equals(d1._dt, d2._dt);
        }

        /// <summary>
        /// Returns a value indicating whether a specified date and a specified date and time have the same date and time value.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dt">The date and time value to compare.</param>
        /// <returns>
        /// <c>true</c> if the two values are equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool Equals(Date d, DateTime dt)
        {
            return DateTime.Equals(d._dt, dt);
        }

        /// <summary>
        /// Returns a value indicating whether a specified date and a specified date and time offset have the same date and time value.
        /// </summary>
        /// <param name="d">The date value to compare.</param>
        /// <param name="dto">The date and time offset value to compare.</param>
        /// <returns>
        /// <c>true</c> if the two values are equal; otherwise, <c>false</c>.
        /// </returns>
        public static bool Equals(Date d, DateTimeOffset dto)
        {
            return DateTime.Equals(d._dt, dto.DateTime);
        }

        /// <summary>
        /// Deserializes a 64-bit binary value and recreates an original serialized date.
        /// </summary>
        /// <param name="dateData">A 64-bit signed integer that encodes the date value.</param>
        /// <returns>
        /// A <see cref="Date"/> that is equivalent to the date that was serialized by the <see cref="Date.ToBinary"/> method.
        /// </returns>
        public static Date FromBinary(long dateData)
        {
            return new Date(DateTime.FromBinary(dateData));
        }

        /// <summary>
        /// Converts the specified Windows file time to an equivalent date.
        /// </summary>
        /// <param name="fileTime">A Windows file time expressed in ticks.</param>
        /// <returns>
        /// A <see cref="Date"/> that represents the equivalent date as <paramref name="fileTime"/>.
        /// </returns>
        public static Date FromFileTime(long fileTime)
        {
            return new Date(DateTime.FromFileTime(fileTime));
        }

        /// <summary>
        /// Returns a date equivalent to the specified OLE Automation date value.
        /// </summary>
        /// <param name="d">An OLE Automation date value.</param>
        /// <returns>
        /// A <see cref="Date"/> that represents the equivalent date as <paramref name="d"/>.
        /// </returns>
        public static Date FromOADate(double d)
        {
            return new Date(DateTime.FromOADate(d));
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if(info == null)
                throw new ArgumentNullException("info", @"Argument Null Exception");
            info.AddValue("ticks", _dt.Ticks);
        }

        /// <summary>
        /// Serializes the date to a 64-bit binary value that subsequently can be used to recreate the original date.
        /// </summary>
        /// <returns>
        /// A 64-bit signed integer that encodes the date.
        /// </returns>
        public long ToBinary()
        {
            return _dt.ToBinary();
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return _dt.GetHashCode();
        }

        /// <summary>
        /// Returns an indication whether the specified year is a leap year.
        /// </summary>
        /// <param name="year">A 4-digit year.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="year"/> is a leap year; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsLeapYear(int year)
        {
            Contract.Requires(1 <= year && year <= 9999);

            return DateTime.IsLeapYear(year);
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent.
        /// </summary>
        /// <param name="s">A string containing a date to convert.</param>
        /// <returns>
        /// A <see cref="Date"/> that is equivalent to the date contained in <paramref name="s"/>.
        /// </returns>
        public static Date Parse(string s)
        {
            return new Date(DateTimeOffset.Parse(s, CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent using the specified culture-specific format information.
        /// </summary>
        /// <param name="s">A string containing a date to convert.</param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="s"/>.</param>
        /// <returns>
        /// A <see cref="Date"/> that is equivalent to the date contained in <paramref name="s"/>, as specified by <paramref name="provider"/>.
        /// </returns>
        public static Date Parse(string s, IFormatProvider provider)
        {
            return new Date(DateTimeOffset.Parse(s, provider));
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent using the specified culture-specific format information and formatting style.
        /// </summary>
        /// <param name="s">A string containing a date to convert.</param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="s"/>.</param>
        /// <param name="styles">A bit-wise combination of the enumeration values that indicates the style elements that can be present in <paramref name="s"/> for the parse operation to succeed and that defines how to interpret the parsed date in relation to the current date. A typical value to specify is <see cref="System.Globalization.DateTimeStyles.None"/>.</param>
        /// <returns>
        /// A <see cref="Date"/> that is equivalent to the date contained in <paramref name="s"/>, as specified by <paramref name="provider"/> and <paramref name="styles"/>.
        /// </returns>
        public static Date Parse(string s, IFormatProvider provider, DateTimeStyles styles)
        {
            return new Date(DateTimeOffset.Parse(s, provider, styles));
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent using the specified format and culture-specific format information. The format of the string representation must match the specified format exactly.
        /// </summary>
        /// <param name="s">A string containing a date to convert.</param>
        /// <param name="format">A format specifier that defines the required format of <paramref name="s"/>.</param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="s"/>.</param>
        /// <returns>
        /// A <see cref="Date"/> that is equivalent to the date contained in <paramref name="s"/>, as specified by <paramref name="format"/> and <paramref name="provider"/>.
        /// </returns>
        public static Date ParseExact(string s, string format, IFormatProvider provider)
        {
            return new Date(DateTimeOffset.ParseExact(s, format, provider));
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent using the specified format, culture-specific format information, and style. The format of the string representation must match the specified format exactly.
        /// </summary>
        /// <param name="s">A string containing a date to convert.</param>
        /// <param name="format">A format specifier that defines the required format of <paramref name="s"/>.</param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="s"/>.</param>
        /// <param name="style">A bit-wise combination of the enumeration values that provides additional information about <paramref name="s"/>, about style elements that may be present in <paramref name="s"/>, or about the conversion from <paramref name="s"/> to a date. A typical value to specify is <see cref="System.Globalization.DateTimeStyles.None"/>.</param>
        /// <returns>
        /// A <see cref="Date"/> that is equivalent to the date contained in <paramref name="s"/>, as specified by <paramref name="format"/>, <paramref name="provider"/>, and <paramref name="style"/>.
        /// </returns>
        public static Date ParseExact(string s, string format, IFormatProvider provider, DateTimeStyles style)
        {
            return new Date(DateTimeOffset.ParseExact(s, format, provider, style));
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent using the specified array of formats, culture-specific format information, and style. The format of the string representation must match at least one of the specified formats exactly.
        /// </summary>
        /// <param name="s">A string containing one or more dates to convert.</param>
        /// <param name="formats">An array of allowable formats of <paramref name="s"/>.</param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="s"/>.</param>
        /// <param name="style">A bit-wise combination of enumeration values that indicates the permitted format of <paramref name="s"/>. A typical value to specify is <see cref="System.Globalization.DateTimeStyles.None"/>.</param>
        /// <returns>
        /// A <see cref="Date"/> that is equivalent to the date contained in <paramref name="s"/>, as specified by <paramref name="formats"/>, <paramref name="provider"/>, and <paramref name="style"/>.
        /// </returns>
        public static Date ParseExact(string s, string[] formats, IFormatProvider provider, DateTimeStyles style)
        {
            return new Date(DateTimeOffset.ParseExact(s, formats, provider, style));
        }

        /// <summary>
        /// Subtracts the specified duration from this instance.
        /// </summary>
        /// <param name="value">The time interval to subtract.</param>
        /// <returns>
        /// A new <see cref="Date"/> that is equal to the date represented by this instance minus the time interval represented by <paramref name="value"/>.
        /// </returns>
        public Date Subtract(TimeSpan value)
        {
            return new Date(_dt.Subtract(value));
        }

        /// <summary>
        /// Subtracts the specified date from this instance.
        /// </summary>
        /// <param name="value">The date value to subtract.</param>
        /// <returns>
        /// A time interval that is equal to the date represented by this instance minus the date represented by <paramref name="value"/>.
        /// </returns>
        public TimeSpan Subtract(Date value)
        {
            return _dt.Subtract(value._dt);
        }

        /// <summary>
        /// Subtracts the specified date and time from this instance.
        /// </summary>
        /// <param name="value">The date and time value to subtract.</param>
        /// <returns>
        /// A time interval that is equal to the date represented by this instance minus the date and time represented by <paramref name="value"/>.
        /// </returns>
        public TimeSpan Subtract(DateTime value)
        {
            return _dt.Subtract(value);
        }
        
        /// <summary>
        /// Subtracts the specified date and time offset from this instance.
        /// </summary>
        /// <param name="value">The date and time value to subtract.</param>
        /// <returns>
        /// A time interval that is equal to the date represented by this instance minus the date and time offset represented by <paramref name="value"/>.
        /// </returns>
        public TimeSpan Subtract(DateTimeOffset value)
        {
            return _dt.Subtract(value.DateTime);
        }

        /// <summary>
        /// Converts the value of this instance to the equivalent OLE Automation date.
        /// </summary>
        /// <returns>
        /// A double-precision floating-point number that contains an OLE Automation date equivalent to the value of this instance.
        /// </returns>
        public double ToOADate()
        {
            return _dt.ToOADate();
        }

        /// <summary>
        /// Converts the value of this instance to an equivalent Windows file time.
        /// </summary>
        /// <returns>
        /// The value of this instance expressed as a Windows file time.
        /// </returns>
        public long ToFileTime()
        {
            return _dt.ToFileTime();
        }

        /// <summary>
        /// Converts the value of this instance to its equivalent long date string representation.
        /// </summary>
        /// <returns>
        /// A string that contains the long date string representation of this instance.
        /// </returns>
        public string ToLongDateString()
        {
            return _dt.ToLongDateString();
        }

        /// <summary>
        /// Converts the value of this instance to its equivalent short date string representation.
        /// </summary>
        /// <returns>
        /// A string that contains the short date string representation of this instance.
        /// </returns>
        public string ToShortDateString()
        {
            return _dt.ToShortDateString();
        }

        /// <summary>
        /// Converts the value of this instance to its equivalent string representation.
        /// </summary>
        /// <returns>
        /// A string representation of the value of this instance.
        /// </returns>
        public override string ToString()
        {
            return ToShortDateString();
        }

        /// <summary>
        /// Converts the value of this instance to its equivalent string representation using the specified format.
        /// </summary>
        /// <param name="format">A standard or custom date format string.</param>
        /// <returns>
        /// A string representation of value of this instance as specified by <paramref name="format"/>.
        /// </returns>
        public string ToString(string format)
        {
            return _dt.ToString(format, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Converts the value of this instance to its equivalent string representation using the specified culture-specific format information.
        /// </summary>
        /// <param name="provider">An object that supplies culture-specific formatting information.</param>
        /// <returns>
        /// A string representation of value of this instance as specified by <paramref name="provider"/>.
        /// </returns>
        public string ToString(IFormatProvider provider)
        {
            return _dt.ToString(provider);
        }

        /// <summary>
        /// Converts the value of this instance to its equivalent string representation using the specified format and culture-specific format information.
        /// </summary>
        /// <param name="format">A standard or custom date and time format string.</param>
        /// <param name="formatProvider">An object that supplies culture-specific formatting information.</param>
        /// <returns>
        /// A string representation of value of this instance as specified by <paramref name="format"/> and <paramref name="formatProvider"/>.
        /// </returns>
        public string ToString(string format, IFormatProvider formatProvider)
        {
            return _dt.ToString(format, formatProvider);
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">A string containing a date to convert.</param>
        /// <param name="result">When this method returns, contains the <see cref="Date"/> value equivalent to the date contained in <paramref name="s"/>, if the conversion succeeded, or <see cref="Date.MinValue"/> if the conversion failed. The conversion fails if <paramref name="s"/> is <c>null</c>, an <see cref="System.String.Empty"/>, or does not contain a valid string representation of a date and time. This parameter is passed uninitialized.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="s"/> was converted successfully; otherwise, <c>false</c>.
        /// </returns>
        public static bool TryParse(string s, out Date result)
        {
            DateTimeOffset dto;
            bool success = DateTimeOffset.TryParse(s, out dto);
            result = new Date(dto);
            return success;
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent using the specified culture-specific format information and formatting style, and returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">A string containing a date to convert.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="s"/>.</param>
        /// <param name="styles">A bit-wise combination of enumeration values that defines how to interpret the parsed date in relation to the current date. A typical value to specify is <see cref="System.Globalization.DateTimeStyles.None"/>.</param>
        /// <param name="result">When this method returns, contains the <see cref="Date"/> value equivalent to the date contained in <paramref name="s"/>, if the conversion succeeded, or <see cref="Date.MinValue"/> if the conversion failed. The conversion fails if <paramref name="s"/> is <c>null</c>, an <see cref="System.String.Empty"/>, or does not contain a valid string representation of a date and time. This parameter is passed uninitialized.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="s"/> was converted successfully; otherwise, <c>false</c>.
        /// </returns>
        public static bool TryParse(string s, IFormatProvider provider, DateTimeStyles styles, out Date result)
        {
            DateTimeOffset dto;
            bool success = DateTimeOffset.TryParse(s, provider, styles, out dto);
            result = new Date(dto);
            return success;
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent using the specified format, culture-specific format information, and style. The format of the string representation must match the specified format exactly. The method returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">A string containing a date to convert.</param>
        /// <param name="format">The required format of <paramref name="s"/>.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information about <paramref name="s"/>.</param>
        /// <param name="style">A bit-wise combination of one or more enumeration values that indicate the permitted format of <paramref name="s"/>.</param>
        /// <param name="result">When this method returns, contains the <see cref="Date"/> value equivalent to the date contained in <paramref name="s"/>, if the conversion succeeded, or <see cref="Date.MinValue"/> if the conversion failed. The conversion fails if either <paramref name="s"/> or <paramref name="format"/> is <c>null</c>, an <see cref="System.String.Empty"/>, or does not contain a date that correspond to the pattern specified in <paramref name="format"/>. This parameter is passed uninitialized.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="s"/> was converted successfully; otherwise, <c>false</c>.
        /// </returns>
        public static bool TryParseExact(string s, string format, IFormatProvider provider, DateTimeStyles style, out Date result)
        {
            DateTimeOffset dto;
            bool success = DateTimeOffset.TryParseExact(s, format, provider, style, out dto);
            result = new Date(dto);
            return success;
        }

        /// <summary>
        /// Converts the specified string representation of a date to its <see cref="Date"/> equivalent using the specified array of formats, culture-specific format information, and style. The format of the string representation must match at least one of the specified formats exactly. The method returns a value that indicates whether the conversion succeeded.
        /// </summary>
        /// <param name="s">A string containing one or more dates to convert.</param>
        /// <param name="formats">An array of allowable formats of <paramref name="s"/>.</param>
        /// <param name="provider">An object that supplies culture-specific format information about <paramref name="s"/>.</param>
        /// <param name="style">A bit-wise combination of enumeration values that indicates the permitted format of <paramref name="s"/>. A typical value to specify is <see cref="System.Globalization.DateTimeStyles.None"/>.</param>
        /// <param name="result">When this method returns, contains the <see cref="Date"/> value equivalent to the date contained in <paramref name="s"/>, if the conversion succeeded, or <see cref="Date.MinValue"/> if the conversion failed. The conversion fails if <paramref name="s"/> or <paramref name="formats"/> is <c>null</c>, <paramref name="s"/> or an element of <paramref name="formats"/> is an <c>null</c>, <see cref="System.String.Empty"/>, or the format of <paramref name="s"/> is not exactly as specified by at least one of the format patterns in <paramref name="formats"/>. This parameter is passed uninitialized.</param>
        /// <returns>
        /// <c>true</c> if <paramref name="s"/> was converted successfully; otherwise, <c>false</c>.
        /// </returns>
        public static bool TryParseExact(string s, string[] formats, IFormatProvider provider, DateTimeStyles style, out Date result)
        {
            DateTimeOffset dto;
            bool success = DateTimeOffset.TryParseExact(s, formats, provider, style, out dto);
            result = new Date(dto);
            return success;
        }

        /// <summary>
        /// Converts the value of this instance to all the string representations supported by the standard date format specifiers.
        /// </summary>
        /// <returns>
        /// A string array where each element is the representation of the value of this instance formatted with one of the standard date format specifiers.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.DateTime.GetDateTimeFormats")]
        public string[] GetDateTimeFormats()
        {
            return _dt.GetDateTimeFormats();
        }

        /// <summary>
        /// Converts the value of this instance to all the string representations supported by the standard date format specifiers and the specified culture-specific formatting information.
        /// </summary>
        /// <param name="provider">An object that supplies culture-specific formatting information about this instance.</param>
        /// <returns>
        /// A string array where each element is the representation of the value of this instance formatted with one of the standard date format specifiers.
        /// </returns>
        public string[] GetDateTimeFormats(IFormatProvider provider)
        {
            return _dt.GetDateTimeFormats(provider);
        }

        /// <summary>
        /// Converts the value of this instance to all the string representations supported by the specified standard date format specifier.
        /// </summary>
        /// <param name="format">A standard date format string.</param>
        /// <returns>
        /// A string array where each element is the representation of the value of this instance formatted with <paramref name="format"/> standard date format specifier.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.DateTime.GetDateTimeFormats(System.Char)")]
        public string[] GetDateTimeFormats(char format)
        {
            return _dt.GetDateTimeFormats(format);
        }

        /// <summary>
        /// Converts the value of this instance to all the string representations supported by the specified standard date format specifier and culture-specific formatting information.
        /// </summary>
        /// <param name="format">A date format string.</param>
        /// <param name="provider">An object that supplies culture-specific formatting information about this instance.</param>
        /// <returns>
        /// A string array where each element is the representation of the value of this instance formatted with one of the standard date format specifiers.
        /// </returns>
        public string[] GetDateTimeFormats(char format, IFormatProvider provider)
        {
            return _dt.GetDateTimeFormats(format, provider);
        }

        /// <inheritdoc/>
        public TypeCode GetTypeCode()
        {
            return TypeCode.Object;
        }

        bool IConvertible.ToBoolean(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Boolean");
        }

        char IConvertible.ToChar(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Char");
        }

        sbyte IConvertible.ToSByte(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to SByte");
        }

        byte IConvertible.ToByte(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Byte");
        }

        short IConvertible.ToInt16(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Int16");
        }

        ushort IConvertible.ToUInt16(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to UInt16");
        }

        int IConvertible.ToInt32(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Int32");
        }

        uint IConvertible.ToUInt32(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to UInt32");
        }

        long IConvertible.ToInt64(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Boolean");
        }

        ulong IConvertible.ToUInt64(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to UInt64");
        }

        float IConvertible.ToSingle(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Int64");
        }

        double IConvertible.ToDouble(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Double");
        }

        decimal IConvertible.ToDecimal(IFormatProvider provider)
        {
            throw new InvalidCastException("Invalid cast from Date to Decimal");
        }

        DateTime IConvertible.ToDateTime(IFormatProvider provider)
        {
            return _dt;
        }

        object IConvertible.ToType(Type conversionType, IFormatProvider provider)
        {
            return Convert.ChangeType(this, conversionType, provider);
        }

        /// <summary>
        /// Gets a <see cref="System.DateTime"/> value that represents this instance.
        /// </summary>
        public DateTime DateTime
        {
            get
            {
                return _dt;
            }
        }

        /// <summary>
        /// Gets a <see cref="System.DateTimeOffset"/> value that represents this instance.
        /// </summary>
        public DateTimeOffset DateTimeOffset
        {
            get
            {
                return new DateTimeOffset(_dt);
            }
        }
    }

    /// <summary>
    /// <see cref="System.DateTime"/> and <see cref="System.DateTimeOffset"/> extension methods.
    /// </summary>
    public static class DateExtensions
    {
        /// <summary>
        /// Gets a <see cref="Date"/> value that represents the date component of this <see cref="System.DateTime"/> object.
        /// </summary>
        /// <param name="dt">A date and time value.</param>
        /// <returns>
        /// A <see cref="Date"/> value that represents the date component of this <see cref="System.DateTime"/> object.
        /// </returns>
        public static Date ToDate(this DateTime dt)
        {
            return new Date(dt);
        }

        /// <summary>
        /// Gets a <see cref="Date"/> value that represents the date component of this <see cref="System.DateTimeOffset"/> object.
        /// </summary>
        /// <param name="dto">A date and time offset value.</param>
        /// <returns>
        /// A <see cref="Date"/> value that represents the date component of this <see cref="System.DateTimeOffset"/> object.
        /// </returns>
        public static Date ToDate(this DateTimeOffset dto)
        {
            return new Date(dto);
        }
    }
}
