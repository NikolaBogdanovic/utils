﻿namespace Utils
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics.Contracts;
    using System.Runtime.Caching;
    using System.Threading;
    using System.Threading.Tasks;

    using Utils.Types;

    /// <summary>
    /// Constants that represent sort orders.
    /// </summary>
    public enum SortOrder
    {
        /// <summary>
        /// " ASC" or an <see cref="System.String.Empty"/>
        /// </summary>
        Ascending = 0,

        /// <summary>
        /// " DESC"
        /// </summary>
        Descending = 1
    }

    /// <summary>
    /// The asynchronous state.
    /// </summary>
    public sealed class AsyncState
    {
        /// <summary>
        /// Gets the extra state data.
        /// </summary>
        public object ExtraData { get; private set; }

        /// <summary>
        /// Gets the cancellation token.
        /// </summary>
        public CancellationToken CancelToken { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AsyncState"/> class.
        /// </summary>
        /// <param name="data">The extra state data.</param>
        /// <param name="cancel">The cancellation token.</param>
        public AsyncState(object data, CancellationToken cancel)
        {
            ExtraData = data;
            CancelToken = cancel;
        }
    }

    /// <summary>
    /// Helper services for existing methods, or their missing overloads.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Gets a text excerpt.
        /// </summary>
        /// <param name="text">The full text.</param>
        /// <param name="length">The excerpt length.</param>
        /// <returns>
        /// A text excerpt.
        /// </returns>
        public static string TextExcerpt(string text, int length)
        {
            Contract.Requires(length > -1);

            if(string.IsNullOrWhiteSpace(text))
                return string.Empty;
            int i = text.LastIndexOfAny(new[] { '!', ':', ';', ',', '.', '?', ' ', Environment.NewLine[0] }, text.Length > length ? length : text.Length - 1);
            return text.Substring(0, i != -1 ? i : text.Length).Replace(Environment.NewLine, " ").TrimEnd(new[] { '!', ':', ';', ',', '.', '?', ' ' }) + "...";
        }

        /// <summary>
        /// Gets a text excerpt.
        /// </summary>
        /// <remarks>
        /// Uses default excerpt length.
        /// </remarks>
        /// <param name="text">The full text.</param>
        /// <returns>
        /// A text excerpt.
        /// </returns>
        public static string TextExcerpt(string text)
        {
            return TextExcerpt(text, Config.ExcerptLength);
        }

        /// <summary>
        /// Combines two JavaScript codes.
        /// </summary>
        /// <param name="code1">The first JavaScript code.</param>
        /// <param name="code2">The second JavaScript code.</param>
        /// <returns>
        /// A combined JavaScript code.
        /// </returns>
        public static string CombineJavaScript(string code1, string code2)
        {
            if(!string.IsNullOrWhiteSpace(code1))
            {
                if(!string.IsNullOrWhiteSpace(code2))
                {
                    code1 = code1.TrimEnd();
                    if(!code1.EndsWith(";", StringComparison.Ordinal))
                        code1 += ';';
                    return code1 + " " + code2;
                }
                return code1;
            }
            return code2;
        }

        /// <summary>
        /// Gets an inner HTML of a tag.
        /// </summary>
        /// <param name="tag">The HTML tag.</param>
        /// <returns>
        /// An inner HTML.
        /// </returns>
        public static string InnerHtml(string tag)
        {
            if(!string.IsNullOrWhiteSpace(tag))
            {
                var first = tag.IndexOf('>');
                var last = tag.IndexOf("/>", StringComparison.Ordinal);
                if(first != -1 && (last == -1 || first < last))
                {
                    last = tag.LastIndexOf("</", StringComparison.Ordinal);
                    if(last != -1)
                        return tag.Substring(first + 1, last - (first + 1));
                }
            }
            return null;
        }

        /// <summary>
        /// Converts a string representation of a boolean to its <see cref="System.Boolean"/> equivalent.
        /// </summary>
        /// <remarks>
        /// "TRUE", "YES", "ON", "T", "Y", "1", "-1" for <c>true</c>. "FALSE", "NO", "OFF", "F", "N", "0" for <c>false</c>.
        /// </remarks>
        /// <param name="value">The case insensitive boolean.</param>
        /// <returns>
        /// A boolean.
        /// </returns>
        public static bool ConvertToBoolean(string value)
        {
            if(!string.IsNullOrWhiteSpace(value))
            {
                value = value.Trim().ToUpperInvariant();
                if(value == "TRUE" || value == "YES" || value == "ON" || value == "T" || value == "Y" || value == "1" || value == "-1")
                    return true;
                if(value == "FALSE" || value == "NO" || value == "OFF" || value == "F" || value == "N" || value == "0")
                    return false;
            }
            throw new ArgumentOutOfRangeException("value", value, @"Argument Out Of Range Exception");
        }

        /// <summary>
        /// Converts a byte array to a hexadecimal Base16 encoded string.
        /// </summary>
        /// <param name="value">The byte array to convert.</param>
        /// <returns>
        /// A hexadecimal Base16 encoded string.
        /// </returns>
        public static string ConvertToBase16String(byte[] value)
        {
            Contract.Requires(value != null);

            return BitConverter.ToString(value).Replace("-", string.Empty);
        }

        /// <summary>
        /// Converts a hexadecimal Base16 encoded string to a byte array.
        /// </summary>
        /// <param name="hex">The hexadecimal Base16 encoded string to convert.</param>
        /// <returns>
        /// A byte array.
        /// </returns>
        public static byte[] ConvertFromBase16String(string hex)
        {
            Contract.Requires(hex != null);

            hex = hex.Trim();
            if(hex.Length % 2 != 0)
                hex = "0" + hex;
            var data = new byte[hex.Length / 2];
            for(int i = 0, j = data.Length; i < j; i++)
                data[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            return data;
        }

        /// <summary>
        /// Gets cached (to avoid reflection) all enum values, including any flags combinations.
        /// </summary>
        /// <param name="type">The enum type.</param>
        /// <returns>
        /// The cached enum values.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2002:DoNotLockOnObjectsWithWeakIdentity")]
        public static IEnumerable<int> EnumGetValues(Type type)
        {
            Contract.Requires(type != null);

            string key = type.ToString();
            MemoryCache cache = MemoryCache.Default;
            var values = cache[key] as int[];
            if(values == null)
            {
                lock(string.Intern("Enum: " + key))
                {
                    values = cache.Get(key) as int[];
                    if(values == null)
                    {
                        values = (int[])type.GetEnumValues();
                        if(type.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0)
                        {
                            int max = 0;
                            foreach(int val in values)
                                max += val;
                            int min = values[0];
                            values = new int[(max - min) + 1];
                            for(int i = min; i <= max; i++)
                                values[i - min] = i;
                        }
                        cache.Add(
                            key,
                            values,
                            new CacheItemPolicy
                            {
                                SlidingExpiration = Config.SlidingExpiration
                            });
                    }
                }
            }
            return values;
        }

        /// <summary>
        /// Pages a filtered and sorted data table.
        /// </summary>
        /// <param name="table">The data table to page.</param>
        /// <param name="filter">The filter expression.</param>
        /// <param name="sort">The sort expression.</param>
        /// <param name="order">The sort order.</param>
        /// <param name="start">The start row index.</param>
        /// <param name="max">The maximum rows number.</param>
        /// <param name="total">[out] The total rows number.</param>
        /// <returns>
        /// A data table page.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static IReadOnlyList<DataRow> PagedDataSource(DataTable table, string filter, string sort, SortOrder order, int start, int max, out int total)
        {
            if(table != null)
            {
                DataRow[] rows = table.Select(filter, order == SortOrder.Ascending ? sort : sort + " DESC");
                total = rows.Length;
                if(start < total)
                {
                    if(start == 0 && max >= total)
                        return rows;
                    var page = new DataRow[Math.Min(max, total - start)];
                    Array.Copy(rows, start, page, 0, page.Length);
                    return page;
                }
            }
            else
                total = -1;
            return null;
        }

        /// <summary>
        /// Gets a cancellation token with a timeout interval.
        /// </summary>
        /// <param name="delay">The delay in milliseconds.</param>
        /// <returns>
        /// A cancellation token.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static CancellationToken CancelToken(int delay)
        {
            return CancellationTokenSource.CreateLinkedTokenSource(RegisteredTask.CancelToken, new CancellationTokenSource(delay).Token).Token;
        }

        /// <summary>
        /// Gets a cancellation token with a timeout interval.
        /// </summary>
        /// <param name="delay">The delay.</param>
        /// <returns>
        /// A cancellation token.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static CancellationToken CancelToken(TimeSpan delay)
        {
            return CancelToken((int)delay.TotalMilliseconds);
        }

        /// <summary>
        /// Gets a cancellation token with a timeout interval.
        /// </summary>
        /// <returns>
        /// A cancellation token.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public static CancellationToken CancelToken()
        {
            return CancelToken(Config.TimeOut);
        }

        /// <summary>
        /// Missing <see cref="System.Threading.Tasks.Task.Run(Action, CancellationToken)"/> overload with an asynchronous state.
        /// </summary>
        /// <param name="act">The delegate.</param>
        /// <param name="state">The asynchronous state.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A started <see cref="System.Threading.Tasks.Task"/>.
        /// </returns>
        public static Task TaskRun(Action<object> act, object state, CancellationToken cancel)
        {
            Contract.Requires(act != null);

            return Task.Factory.StartNew(act, state, cancel, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
        }

        /// <summary>
        /// Missing <see cref="System.Threading.Tasks.Task.Run{TResult}(Func{TResult}, CancellationToken)"/> overload with an asynchronous state.
        /// </summary>
        /// <typeparam name="T">The generic type parameter.</typeparam>
        /// <param name="func">The delegate.</param>
        /// <param name="state">The asynchronous state.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// A started <see cref="System.Threading.Tasks.Task{T}"/>.
        /// </returns>
        public static Task<T> TaskRun<T>(Func<object, T> func, object state, CancellationToken cancel)
        {
            Contract.Requires(func != null);

            return Task<T>.Factory.StartNew(func, state, cancel, TaskCreationOptions.DenyChildAttach, TaskScheduler.Default);
        }

        /// <summary>
        /// Missing <see cref="System.Threading.Tasks.Task.RunSynchronously()"/> overload with a cancellation token.
        /// </summary>
        /// <remarks>
        /// Safely logs and swallows any exceptions.
        /// </remarks>
        /// <param name="task">The task to run synchronously, can already be started.</param>
        /// <param name="cancel">The cancellation token.</param>
        /// <returns>
        /// <c>true</c> if task ran to completion; otherwise <c>false</c>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        public static bool TaskRunSynchronously(Task task, CancellationToken cancel)
        {
            Contract.Requires(task != null);

            try
            {
                if(task.Status == TaskStatus.Created)
                    task.Start();
                task.Wait(cancel);
                return true;
            }
            catch(Exception ex)
            {
                RegisteredTask.Start(Logger.LogExceptionAsync(true, task.Exception ?? ex, Http.CurrentRequest));
                return false;
            }
        }
    } 
}
