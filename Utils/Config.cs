﻿namespace Utils
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Web.Configuration;

    using Utils.Types;

    /// <exclude/>
    #pragma warning disable 1591
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "SA1604:ElementDocumentationMustHaveSummary", Justification = "Reviewed.")]
    public static class Config
    {
        public const string IsoTime = "HH':'mm':'ss";
        public const string IsoDate = "yyyy'-'MM'-'dd";
        public const string IsoDateTime = IsoDate + "' '" + IsoTime;
        public const string IsoDateTimeZone = IsoDate + "'T'" + IsoTime + "'.'fffK";

        public static readonly string RootPath = AppDomain.CurrentDomain.BaseDirectory;
        public static readonly string DataPath = Path.Combine(RootPath, "App_Data");

        public static readonly string AdminEmail = Crypto.Unprotect(WebConfigurationManager.AppSettings["AdminEmail"], "AdminEmail");
        public static readonly string CacheControl = WebConfigurationManager.AppSettings["CacheControl"];
        public static readonly string CdnPublic = Crypto.Unprotect(WebConfigurationManager.AppSettings["CdnPublic"], "CdnPublic");
        public static readonly string CdnSecure = Crypto.Unprotect(WebConfigurationManager.AppSettings["CdnSecure"], "CdnSecure");
        public static readonly string DatabaseName = Crypto.Unprotect(WebConfigurationManager.AppSettings["DatabaseName"], "DatabaseName");
        public static readonly string DatabasePass = Crypto.Unprotect(WebConfigurationManager.AppSettings["DatabasePass"], "DatabasePass");
        public static readonly string DatabaseServer = Crypto.Unprotect(WebConfigurationManager.AppSettings["DatabaseServer"], "DatabaseServer");
        public static readonly string DatabaseUser = Crypto.Unprotect(WebConfigurationManager.AppSettings["DatabaseUser"], "DatabaseUser");
        public static readonly string DomainPublic = Crypto.Unprotect(WebConfigurationManager.AppSettings["DomainPublic"], "DomainPublic");
        public static readonly string DomainSecure = Crypto.Unprotect(WebConfigurationManager.AppSettings["DomainSecure"], "DomainSecure");
        public static readonly string SmtpEmail = Crypto.Unprotect(WebConfigurationManager.AppSettings["SmtpEmail"], "SmtpEmail");
        public static readonly string SmtpPass = Crypto.Unprotect(WebConfigurationManager.AppSettings["SmtpPass"], "SmtpPass");
        public static readonly string SmtpServer = Crypto.Unprotect(WebConfigurationManager.AppSettings["SmtpServer"], "SmtpServer");
        public static readonly string WebsiteName = Crypto.Unprotect(WebConfigurationManager.AppSettings["WebsiteName"], "WebsiteName");

        public static readonly bool SmtpAuth = Helper.ConvertToBoolean(Crypto.Unprotect(WebConfigurationManager.AppSettings["SmtpAuth"], "SmtpAuth") ?? bool.FalseString);
        public static readonly bool SmtpHtml = Helper.ConvertToBoolean(Crypto.Unprotect(WebConfigurationManager.AppSettings["SmtpHtml"], "SmtpHtml") ?? bool.FalseString);
        public static readonly bool SmtpImplicit = Helper.ConvertToBoolean(Crypto.Unprotect(WebConfigurationManager.AppSettings["SmtpImplicit"], "SmtpImplicit") ?? bool.FalseString);
        public static readonly bool SmtpSsl = Helper.ConvertToBoolean(Crypto.Unprotect(WebConfigurationManager.AppSettings["SmtpSsl"], "SmtpSsl") ?? bool.FalseString);

        public static readonly byte PageSize = byte.Parse(Crypto.Unprotect(WebConfigurationManager.AppSettings["PageSize"], "PageSize") ?? "0", CultureInfo.InvariantCulture);

        public static readonly int DatabasePort = int.Parse(Crypto.Unprotect(WebConfigurationManager.AppSettings["DatabasePort"], "DatabasePort") ?? "0", CultureInfo.InvariantCulture);
        public static readonly int ExcerptLength = int.Parse(Crypto.Unprotect(WebConfigurationManager.AppSettings["ExcerptLength"], "ExcerptLength") ?? "0", CultureInfo.InvariantCulture);
        public static readonly int SmtpPort = int.Parse(Crypto.Unprotect(WebConfigurationManager.AppSettings["SmtpPort"], "SmtpPort") ?? "0", CultureInfo.InvariantCulture);

        public static readonly DatabaseType DatabaseType = (DatabaseType)int.Parse(Crypto.Unprotect(WebConfigurationManager.AppSettings["DatabaseType"], "DatabaseType") ?? "0", CultureInfo.InvariantCulture);

        public static readonly TimeSpan TimeOut = ((SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState")).StateNetworkTimeout;
        public static readonly TimeSpan SlidingExpiration = ((SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState")).Timeout;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly TimeZoneInfo ServerTimeZone = TimeZoneInfo.FindSystemTimeZoneById(Crypto.Unprotect(WebConfigurationManager.AppSettings["ServerTimeZone"], "ServerTimeZone") ?? "UTC");

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly CultureInfo ServerCulture = CultureInfo.GetCultureInfo(Crypto.Unprotect(WebConfigurationManager.AppSettings["ServerCulture"], "ServerCulture") ?? string.Empty);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly RegionInfo ServerRegion = new RegionInfo(ServerCulture.LCID);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly Currency ServerCurrency = new Currency(ServerRegion.ISOCurrencySymbol, ServerCulture.NumberFormat.CurrencyDecimalDigits, 1);

        public static readonly string ConnectionString = Database.BuildConnectionString(DatabaseType, WebConfigurationManager.ConnectionStrings["default"].ConnectionString, DatabaseName, DatabaseUser, DatabasePass, DatabaseServer, DatabasePort);
    }
    #pragma warning restore 1591
}
