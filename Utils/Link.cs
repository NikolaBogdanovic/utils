﻿namespace Utils
{
    using System;
    using System.Diagnostics.Contracts;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// Link services.
    /// </summary>
    public static class Link
    {
        private static readonly bool CdnPublic = !string.IsNullOrWhiteSpace(Config.CdnPublic);
        private static readonly bool CdnSecure = !string.IsNullOrWhiteSpace(Config.CdnSecure);
        private static readonly bool DomainSecure = !string.IsNullOrWhiteSpace(Config.DomainSecure);

        private static string Url(string path, bool ssl, bool cdn)
        {
            path = path.Trim();
            string protocol, domain;
            if(ssl && DomainSecure)
            {
                protocol = "https";
                domain = cdn && CdnSecure ? Config.CdnSecure : Config.DomainSecure;
            }
            else
            {
                protocol = "http";
                domain = cdn && CdnPublic ? Config.CdnPublic : Config.DomainPublic;
            }
            return string.Format(CultureInfo.InvariantCulture, "{0}://{1}{2}{3}", protocol, domain, path[0] == '/' ? null : "/", path);
        }

        /// <summary>
        /// Creates an absolute link from a relative one.
        /// </summary>
        /// <remarks>
        /// If a secure domain is not available, falls back to the public one.
        /// </remarks>
        /// <param name="relative">The relative link.</param>
        /// <param name="ssl"><c>true</c> for secure, <c>false</c> for public domain.</param>
        /// <returns>
        /// An absolute link.
        /// </returns>
        public static string Absolute(string relative, bool ssl)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(relative));

            return Url(relative, ssl, false);
        }

        /// <summary>
        /// Creates a Content Delivery Network (CDN) link from a relative one.
        /// </summary>
        /// <remarks>
        /// If a secure domain is not available, falls back to the public one. If a CDN domain is not available, falls back to the absolute one.
        /// </remarks>
        /// <param name="relative">The relative link.</param>
        /// <param name="ssl"><c>true</c> for secure, <c>false</c> for public domain.</param>
        /// <returns>
        /// A CDN link.
        /// </returns>
        public static string Static(string relative, bool ssl)
        {
            Contract.Requires(!string.IsNullOrWhiteSpace(relative));

            return Url(relative, ssl, true);
        }

        /// <summary>
        /// Creates a "tel" link.
        /// </summary>
        /// <param name="dial">The dial code.</param>
        /// <param name="phone">The phone number.</param>
        /// <returns>
        /// A "tel" link.
        /// </returns>
        public static string Tel(short dial, long phone)
        {
            return string.Format(CultureInfo.CurrentCulture, "<a href='tel:+{0}{1}'>+{2} {3}</a>", dial.ToString(CultureInfo.InvariantCulture), phone.ToString(CultureInfo.InvariantCulture), dial, phone);
        }

        /// <summary>
        /// Creates a "tel:" link.
        /// </summary>
        /// <param name="dial">The dial code.</param>
        /// <param name="phone">The phone number.</param>
        /// <returns>
        /// A "tel:" link.
        /// </returns>
        public static string Tel(string dial, string phone)
        {
            if(string.IsNullOrWhiteSpace(phone))
                return null;
            if(phone.StartsWith("+" + dial, StringComparison.Ordinal))
                phone = phone.Substring(dial.Length + 1);
            else if(phone.StartsWith("00" + dial, StringComparison.Ordinal))
                phone = phone.Substring(dial.Length + 2);
            var sb = new StringBuilder();
            foreach(char c in phone)
            {
                if(char.IsDigit(c))
                    sb.Append(c);
            }
            return Tel(short.Parse(dial, CultureInfo.CurrentCulture), long.Parse(sb.ToString(), CultureInfo.CurrentCulture));
        }

        /// <summary>
        /// Creates a "sms:" link.
        /// </summary>
        /// <param name="dial">The dial code.</param>
        /// <param name="phone">The phone number.</param>
        /// <param name="body">The message body.</param>
        /// <returns>
        /// A "sms:" link.
        /// </returns>
        public static string Sms(short dial, long phone, string body)
        {
            return string.Format(CultureInfo.CurrentCulture, "<a href='sms:+{0}{1}?body={4}'>+{2} {3}</a>", dial.ToString(CultureInfo.InvariantCulture), phone.ToString(CultureInfo.InvariantCulture), dial, phone, Uri.EscapeDataString(body));
        }

        /// <summary>
        /// Creates a "mailto:" link.
        /// </summary>
        /// <param name="email">The e-mail message.</param>
        /// <returns>
        /// A "mailto:" link.
        /// </returns>
        public static string MailTo(EmailMessage email)
        {
            return string.Format(CultureInfo.CurrentCulture, "<a href='mailto:{0}?from={1}&amp;cc={2}&amp;bcc={3}&amp;subject={4}&amp;body={5}'>{0}</a>", Uri.EscapeDataString(email.To), Uri.EscapeDataString(email.From), Uri.EscapeDataString(email.Cc), Uri.EscapeDataString(email.Bcc), Uri.EscapeDataString(email.Subject), Uri.EscapeDataString(email.Body));
        }
    } 
}
