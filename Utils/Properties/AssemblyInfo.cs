﻿using System;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: Guid("4667c3d2-45c6-453b-9bf5-d8231961e39d")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyDefaultAlias("Miscellaneous Helper Utilities")]
[assembly: AssemblyTitle("Miscellaneous Helper Utilities")]
[assembly: AssemblyDescription("Miscellaneous Helper Utilities")]
[assembly: AssemblyProduct("Miscellaneous Helper Utilities")]
[assembly: AssemblyCompany("Nikola Bogdanović")]
[assembly: AssemblyCopyright("© 2012-2015 http://rs.linkedin.com/in/NikolaBogdanovic")]
[assembly: AssemblyTrademark("pOcHa")]
[assembly: AssemblyCulture("")]
[assembly: NeutralResourcesLanguage("en-US")]
